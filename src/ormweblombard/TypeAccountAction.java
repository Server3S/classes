/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_account_action", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeAccountAction.findAll", query = "SELECT t FROM TypeAccountAction t"),
    @NamedQuery(name = "TypeAccountAction.findByIdRecord", query = "SELECT t FROM TypeAccountAction t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeAccountAction.findByIdAccountAction", query = "SELECT t FROM TypeAccountAction t WHERE t.idAccountAction = :idAccountAction"),
    @NamedQuery(name = "TypeAccountAction.findByNameAccountAction", query = "SELECT t FROM TypeAccountAction t WHERE t.nameAccountAction = :nameAccountAction"),
    @NamedQuery(name = "TypeAccountAction.findByStatusRow", query = "SELECT t FROM TypeAccountAction t WHERE t.statusRow = :statusRow")})
public class TypeAccountAction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_account_action")
    private Integer idAccountAction;
    @Column(name = "name_account_action", length = 50)
    private String nameAccountAction;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeAccountAction() {
    }

    public TypeAccountAction(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdAccountAction() {
        return idAccountAction;
    }

    public void setIdAccountAction(Integer idAccountAction) {
        this.idAccountAction = idAccountAction;
    }

    public String getNameAccountAction() {
        return nameAccountAction;
    }

    public void setNameAccountAction(String nameAccountAction) {
        this.nameAccountAction = nameAccountAction;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeAccountAction)) {
            return false;
        }
        TypeAccountAction other = (TypeAccountAction) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeAccountAction[ idRecord=" + idRecord + " ]";
    }
    
}
