/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_11", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi11.findAll", query = "SELECT c FROM CbNsi11 c"),
    @NamedQuery(name = "CbNsi11.findByNciIdO", query = "SELECT c FROM CbNsi11 c WHERE c.nciIdO = :nciIdO"),
    @NamedQuery(name = "CbNsi11.findByNciId", query = "SELECT c FROM CbNsi11 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi11.findBySNum", query = "SELECT c FROM CbNsi11 c WHERE c.sNum = :sNum"),
    @NamedQuery(name = "CbNsi11.findBySName", query = "SELECT c FROM CbNsi11 c WHERE c.sName = :sName"),
    @NamedQuery(name = "CbNsi11.findBySLen", query = "SELECT c FROM CbNsi11 c WHERE c.sLen = :sLen"),
    @NamedQuery(name = "CbNsi11.findByPozBeg", query = "SELECT c FROM CbNsi11 c WHERE c.pozBeg = :pozBeg"),
    @NamedQuery(name = "CbNsi11.findByPozEnd", query = "SELECT c FROM CbNsi11 c WHERE c.pozEnd = :pozEnd"),
    @NamedQuery(name = "CbNsi11.findBySTyper", query = "SELECT c FROM CbNsi11 c WHERE c.sTyper = :sTyper"),
    @NamedQuery(name = "CbNsi11.findByIdRecord", query = "SELECT c FROM CbNsi11 c WHERE c.idRecord = :idRecord")})
public class CbNsi11 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id_o", length = 10)
    private String nciIdO;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Column(name = "s_num", length = 10)
    private String sNum;
    @Column(name = "s_name", length = 50)
    private String sName;
    @Column(name = "s_len", length = 10)
    private String sLen;
    @Column(name = "poz_beg", length = 10)
    private String pozBeg;
    @Column(name = "poz_end", length = 10)
    private String pozEnd;
    @Column(name = "s_typer", length = 10)
    private String sTyper;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi11() {
    }

    public CbNsi11(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNciIdO() {
        return nciIdO;
    }

    public void setNciIdO(String nciIdO) {
        this.nciIdO = nciIdO;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getSNum() {
        return sNum;
    }

    public void setSNum(String sNum) {
        this.sNum = sNum;
    }

    public String getSName() {
        return sName;
    }

    public void setSName(String sName) {
        this.sName = sName;
    }

    public String getSLen() {
        return sLen;
    }

    public void setSLen(String sLen) {
        this.sLen = sLen;
    }

    public String getPozBeg() {
        return pozBeg;
    }

    public void setPozBeg(String pozBeg) {
        this.pozBeg = pozBeg;
    }

    public String getPozEnd() {
        return pozEnd;
    }

    public void setPozEnd(String pozEnd) {
        this.pozEnd = pozEnd;
    }

    public String getSTyper() {
        return sTyper;
    }

    public void setSTyper(String sTyper) {
        this.sTyper = sTyper;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi11)) {
            return false;
        }
        CbNsi11 other = (CbNsi11) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi11[ idRecord=" + idRecord + " ]";
    }
    
}
