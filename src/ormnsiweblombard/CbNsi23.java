/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_23", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi23.findAll", query = "SELECT c FROM CbNsi23 c"),
    @NamedQuery(name = "CbNsi23.findByNciId", query = "SELECT c FROM CbNsi23 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi23.findByBranchStat", query = "SELECT c FROM CbNsi23 c WHERE c.branchStat = :branchStat"),
    @NamedQuery(name = "CbNsi23.findByBranchId", query = "SELECT c FROM CbNsi23 c WHERE c.branchId = :branchId"),
    @NamedQuery(name = "CbNsi23.findByOconx", query = "SELECT c FROM CbNsi23 c WHERE c.oconx = :oconx"),
    @NamedQuery(name = "CbNsi23.findByBranchNam", query = "SELECT c FROM CbNsi23 c WHERE c.branchNam = :branchNam"),
    @NamedQuery(name = "CbNsi23.findByDateOpen", query = "SELECT c FROM CbNsi23 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi23.findByDateClose", query = "SELECT c FROM CbNsi23 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi23.findByActive", query = "SELECT c FROM CbNsi23 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi23.findByIdRecord", query = "SELECT c FROM CbNsi23 c WHERE c.idRecord = :idRecord")})
public class CbNsi23 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Column(name = "branch_stat", length = 10)
    private String branchStat;
    @Column(name = "branch_id", length = 10)
    private String branchId;
    @Column(name = "oconx", length = 10)
    private String oconx;
    @Column(name = "branch_nam", length = 150)
    private String branchNam;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Column(name = "date_close", length = 10)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi23() {
    }

    public CbNsi23(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getBranchStat() {
        return branchStat;
    }

    public void setBranchStat(String branchStat) {
        this.branchStat = branchStat;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getOconx() {
        return oconx;
    }

    public void setOconx(String oconx) {
        this.oconx = oconx;
    }

    public String getBranchNam() {
        return branchNam;
    }

    public void setBranchNam(String branchNam) {
        this.branchNam = branchNam;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi23)) {
            return false;
        }
        CbNsi23 other = (CbNsi23) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi23[ idRecord=" + idRecord + " ]";
    }
    
}
