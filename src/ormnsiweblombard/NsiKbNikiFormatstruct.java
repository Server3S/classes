/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "nsi_kb_niki_formatstruct", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NsiKbNikiFormatstruct.findAll", query = "SELECT n FROM NsiKbNikiFormatstruct n"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByIdRecord", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.idRecord = :idRecord"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByCategoryFormat", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.categoryFormat = :categoryFormat"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByNameCategoryFormat", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.nameCategoryFormat = :nameCategoryFormat"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByDescriptCategoryFormat", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.descriptCategoryFormat = :descriptCategoryFormat"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByFormat", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.format = :format"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByAtributFormat", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.atributFormat = :atributFormat"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByAtributType", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.atributType = :atributType"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByAtributSize", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.atributSize = :atributSize"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByAtribDefaultValue", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.atribDefaultValue = :atribDefaultValue"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByRecipientName", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.recipientName = :recipientName"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findBySenderName", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.senderName = :senderName"),
    @NamedQuery(name = "NsiKbNikiFormatstruct.findByAtribDescript", query = "SELECT n FROM NsiKbNikiFormatstruct n WHERE n.atribDescript = :atribDescript")})
public class NsiKbNikiFormatstruct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "category_format", length = 50)
    private String categoryFormat;
    @Column(name = "name_category_format", length = 100)
    private String nameCategoryFormat;
    @Column(name = "descript_category_format", length = 1000)
    private String descriptCategoryFormat;
    @Column(name = "format", length = 100)
    private String format;
    @Column(name = "atribut_format", length = 20)
    private String atributFormat;
    @Column(name = "atribut_type", length = 20)
    private String atributType;
    @Column(name = "atribut_size", length = 5)
    private String atributSize;
    @Column(name = "atrib_default_value", length = 20)
    private String atribDefaultValue;
    @Column(name = "recipient_name", length = 30)
    private String recipientName;
    @Column(name = "sender_name", length = 30)
    private String senderName;
    @Column(name = "atrib_descript", length = 1000)
    private String atribDescript;

    public NsiKbNikiFormatstruct() {
    }

    public NsiKbNikiFormatstruct(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getCategoryFormat() {
        return categoryFormat;
    }

    public void setCategoryFormat(String categoryFormat) {
        this.categoryFormat = categoryFormat;
    }

    public String getNameCategoryFormat() {
        return nameCategoryFormat;
    }

    public void setNameCategoryFormat(String nameCategoryFormat) {
        this.nameCategoryFormat = nameCategoryFormat;
    }

    public String getDescriptCategoryFormat() {
        return descriptCategoryFormat;
    }

    public void setDescriptCategoryFormat(String descriptCategoryFormat) {
        this.descriptCategoryFormat = descriptCategoryFormat;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getAtributFormat() {
        return atributFormat;
    }

    public void setAtributFormat(String atributFormat) {
        this.atributFormat = atributFormat;
    }

    public String getAtributType() {
        return atributType;
    }

    public void setAtributType(String atributType) {
        this.atributType = atributType;
    }

    public String getAtributSize() {
        return atributSize;
    }

    public void setAtributSize(String atributSize) {
        this.atributSize = atributSize;
    }

    public String getAtribDefaultValue() {
        return atribDefaultValue;
    }

    public void setAtribDefaultValue(String atribDefaultValue) {
        this.atribDefaultValue = atribDefaultValue;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getAtribDescript() {
        return atribDescript;
    }

    public void setAtribDescript(String atribDescript) {
        this.atribDescript = atribDescript;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NsiKbNikiFormatstruct)) {
            return false;
        }
        NsiKbNikiFormatstruct other = (NsiKbNikiFormatstruct) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NsiKbNikiFormatstruct[ idRecord=" + idRecord + " ]";
    }
    
}
