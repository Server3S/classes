/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "account_client_history", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountClientHistory.findAll", query = "SELECT a FROM AccountClientHistory a"),
    @NamedQuery(name = "AccountClientHistory.findByIdRecord", query = "SELECT a FROM AccountClientHistory a WHERE a.idRecord = :idRecord"),
    @NamedQuery(name = "AccountClientHistory.findByIdAccountClient", query = "SELECT a FROM AccountClientHistory a WHERE a.idAccountClient = :idAccountClient"),
    @NamedQuery(name = "AccountClientHistory.findByDatetimeOperation", query = "SELECT a FROM AccountClientHistory a WHERE a.datetimeOperation = :datetimeOperation"),
    @NamedQuery(name = "AccountClientHistory.findByUserOperationAccount", query = "SELECT a FROM AccountClientHistory a WHERE a.userOperationAccount = :userOperationAccount"),
    @NamedQuery(name = "AccountClientHistory.findByIncomingSaldoAccount", query = "SELECT a FROM AccountClientHistory a WHERE a.incomingSaldoAccount = :incomingSaldoAccount"),
    @NamedQuery(name = "AccountClientHistory.findBySummaOperationAccount", query = "SELECT a FROM AccountClientHistory a WHERE a.summaOperationAccount = :summaOperationAccount"),
    @NamedQuery(name = "AccountClientHistory.findByOutgoingSaldoAccount", query = "SELECT a FROM AccountClientHistory a WHERE a.outgoingSaldoAccount = :outgoingSaldoAccount"),
    @NamedQuery(name = "AccountClientHistory.findByTypeOperationAccount", query = "SELECT a FROM AccountClientHistory a WHERE a.typeOperationAccount = :typeOperationAccount"),
    @NamedQuery(name = "AccountClientHistory.findByStatusRow", query = "SELECT a FROM AccountClientHistory a WHERE a.statusRow = :statusRow")})
public class AccountClientHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_account_client")
    private Integer idAccountClient;
    @Column(name = "datetime_operation", length = 20)
    private String datetimeOperation;
    @Column(name = "user_operation_account", length = 30)
    private String userOperationAccount;
    @Column(name = "incoming_saldo_account")
    private BigInteger incomingSaldoAccount;
    @Column(name = "summa_operation_account")
    private BigInteger summaOperationAccount;
    @Column(name = "outgoing_saldo_account")
    private BigInteger outgoingSaldoAccount;
    @Column(name = "type_operation_account")
    private Integer typeOperationAccount;
    @Column(name = "status_row")
    private Short statusRow;

    public AccountClientHistory() {
    }

    public AccountClientHistory(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdAccountClient() {
        return idAccountClient;
    }

    public void setIdAccountClient(Integer idAccountClient) {
        this.idAccountClient = idAccountClient;
    }

    public String getDatetimeOperation() {
        return datetimeOperation;
    }

    public void setDatetimeOperation(String datetimeOperation) {
        this.datetimeOperation = datetimeOperation;
    }

    public String getUserOperationAccount() {
        return userOperationAccount;
    }

    public void setUserOperationAccount(String userOperationAccount) {
        this.userOperationAccount = userOperationAccount;
    }

    public BigInteger getIncomingSaldoAccount() {
        return incomingSaldoAccount;
    }

    public void setIncomingSaldoAccount(BigInteger incomingSaldoAccount) {
        this.incomingSaldoAccount = incomingSaldoAccount;
    }

    public BigInteger getSummaOperationAccount() {
        return summaOperationAccount;
    }

    public void setSummaOperationAccount(BigInteger summaOperationAccount) {
        this.summaOperationAccount = summaOperationAccount;
    }

    public BigInteger getOutgoingSaldoAccount() {
        return outgoingSaldoAccount;
    }

    public void setOutgoingSaldoAccount(BigInteger outgoingSaldoAccount) {
        this.outgoingSaldoAccount = outgoingSaldoAccount;
    }

    public Integer getTypeOperationAccount() {
        return typeOperationAccount;
    }

    public void setTypeOperationAccount(Integer typeOperationAccount) {
        this.typeOperationAccount = typeOperationAccount;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountClientHistory)) {
            return false;
        }
        AccountClientHistory other = (AccountClientHistory) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.AccountClientHistory[ idRecord=" + idRecord + " ]";
    }
    
}
