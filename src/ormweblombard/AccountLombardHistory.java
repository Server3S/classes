/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "account_lombard_history", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountLombardHistory.findAll", query = "SELECT a FROM AccountLombardHistory a"),
    @NamedQuery(name = "AccountLombardHistory.findByIdRecord", query = "SELECT a FROM AccountLombardHistory a WHERE a.idRecord = :idRecord"),
    @NamedQuery(name = "AccountLombardHistory.findByIdAccountLombard", query = "SELECT a FROM AccountLombardHistory a WHERE a.idAccountLombard = :idAccountLombard"),
    @NamedQuery(name = "AccountLombardHistory.findByDatetimeOperAcclombard", query = "SELECT a FROM AccountLombardHistory a WHERE a.datetimeOperAcclombard = :datetimeOperAcclombard"),
    @NamedQuery(name = "AccountLombardHistory.findByUserOperAcclombard", query = "SELECT a FROM AccountLombardHistory a WHERE a.userOperAcclombard = :userOperAcclombard"),
    @NamedQuery(name = "AccountLombardHistory.findByIncomingSaldoAcclombard", query = "SELECT a FROM AccountLombardHistory a WHERE a.incomingSaldoAcclombard = :incomingSaldoAcclombard"),
    @NamedQuery(name = "AccountLombardHistory.findBySummaOperAcclombard", query = "SELECT a FROM AccountLombardHistory a WHERE a.summaOperAcclombard = :summaOperAcclombard"),
    @NamedQuery(name = "AccountLombardHistory.findByOutgoingSaldoAcclombard", query = "SELECT a FROM AccountLombardHistory a WHERE a.outgoingSaldoAcclombard = :outgoingSaldoAcclombard"),
    @NamedQuery(name = "AccountLombardHistory.findByStatusRow", query = "SELECT a FROM AccountLombardHistory a WHERE a.statusRow = :statusRow"),
    @NamedQuery(name = "AccountLombardHistory.findByTypeOperAcclombard", query = "SELECT a FROM AccountLombardHistory a WHERE a.typeOperAcclombard = :typeOperAcclombard")})
public class AccountLombardHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_account_lombard")
    private Integer idAccountLombard;
    @Column(name = "datetime_oper_acclombard", length = 20)
    private String datetimeOperAcclombard;
    @Column(name = "user_oper_acclombard", length = 30)
    private String userOperAcclombard;
    @Column(name = "incoming_saldo_acclombard")
    private BigInteger incomingSaldoAcclombard;
    @Column(name = "summa_oper_acclombard")
    private BigInteger summaOperAcclombard;
    @Column(name = "outgoing_saldo_acclombard")
    private BigInteger outgoingSaldoAcclombard;
    @Column(name = "status_row")
    private Short statusRow;
    @Column(name = "type_oper_acclombard")
    private Short typeOperAcclombard;

    public AccountLombardHistory() {
    }

    public AccountLombardHistory(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdAccountLombard() {
        return idAccountLombard;
    }

    public void setIdAccountLombard(Integer idAccountLombard) {
        this.idAccountLombard = idAccountLombard;
    }

    public String getDatetimeOperAcclombard() {
        return datetimeOperAcclombard;
    }

    public void setDatetimeOperAcclombard(String datetimeOperAcclombard) {
        this.datetimeOperAcclombard = datetimeOperAcclombard;
    }

    public String getUserOperAcclombard() {
        return userOperAcclombard;
    }

    public void setUserOperAcclombard(String userOperAcclombard) {
        this.userOperAcclombard = userOperAcclombard;
    }

    public BigInteger getIncomingSaldoAcclombard() {
        return incomingSaldoAcclombard;
    }

    public void setIncomingSaldoAcclombard(BigInteger incomingSaldoAcclombard) {
        this.incomingSaldoAcclombard = incomingSaldoAcclombard;
    }

    public BigInteger getSummaOperAcclombard() {
        return summaOperAcclombard;
    }

    public void setSummaOperAcclombard(BigInteger summaOperAcclombard) {
        this.summaOperAcclombard = summaOperAcclombard;
    }

    public BigInteger getOutgoingSaldoAcclombard() {
        return outgoingSaldoAcclombard;
    }

    public void setOutgoingSaldoAcclombard(BigInteger outgoingSaldoAcclombard) {
        this.outgoingSaldoAcclombard = outgoingSaldoAcclombard;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    public Short getTypeOperAcclombard() {
        return typeOperAcclombard;
    }

    public void setTypeOperAcclombard(Short typeOperAcclombard) {
        this.typeOperAcclombard = typeOperAcclombard;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountLombardHistory)) {
            return false;
        }
        AccountLombardHistory other = (AccountLombardHistory) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.AccountLombardHistory[ idRecord=" + idRecord + " ]";
    }
    
}
