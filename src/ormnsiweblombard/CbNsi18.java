/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_18", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi18.findAll", query = "SELECT c FROM CbNsi18 c"),
    @NamedQuery(name = "CbNsi18.findByNsiId", query = "SELECT c FROM CbNsi18 c WHERE c.nsiId = :nsiId"),
    @NamedQuery(name = "CbNsi18.findByCountryCode", query = "SELECT c FROM CbNsi18 c WHERE c.countryCode = :countryCode"),
    @NamedQuery(name = "CbNsi18.findBySimbolCode", query = "SELECT c FROM CbNsi18 c WHERE c.simbolCode = :simbolCode"),
    @NamedQuery(name = "CbNsi18.findByAlpha3Code", query = "SELECT c FROM CbNsi18 c WHERE c.alpha3Code = :alpha3Code"),
    @NamedQuery(name = "CbNsi18.findByNameCountry", query = "SELECT c FROM CbNsi18 c WHERE c.nameCountry = :nameCountry"),
    @NamedQuery(name = "CbNsi18.findByCurrencyCode", query = "SELECT c FROM CbNsi18 c WHERE c.currencyCode = :currencyCode"),
    @NamedQuery(name = "CbNsi18.findByPr", query = "SELECT c FROM CbNsi18 c WHERE c.pr = :pr"),
    @NamedQuery(name = "CbNsi18.findByDateOpen", query = "SELECT c FROM CbNsi18 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi18.findByDateClose", query = "SELECT c FROM CbNsi18 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi18.findByActive", query = "SELECT c FROM CbNsi18 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi18.findByIdRecord", query = "SELECT c FROM CbNsi18 c WHERE c.idRecord = :idRecord")})
public class CbNsi18 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nsi_id")
    private Integer nsiId;
    @Column(name = "country_code", length = 3)
    private String countryCode;
    @Column(name = "simbol_code", length = 2)
    private String simbolCode;
    @Column(name = "alpha3_code", length = 3)
    private String alpha3Code;
    @Column(name = "name_country", length = 200)
    private String nameCountry;
    @Column(name = "currency_code", length = 3)
    private String currencyCode;
    @Column(name = "pr", length = 1)
    private String pr;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Column(name = "date_close", length = 10)
    private String dateClose;
    @Column(name = "active", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi18() {
    }

    public CbNsi18(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getNsiId() {
        return nsiId;
    }

    public void setNsiId(Integer nsiId) {
        this.nsiId = nsiId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getSimbolCode() {
        return simbolCode;
    }

    public void setSimbolCode(String simbolCode) {
        this.simbolCode = simbolCode;
    }

    public String getAlpha3Code() {
        return alpha3Code;
    }

    public void setAlpha3Code(String alpha3Code) {
        this.alpha3Code = alpha3Code;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getPr() {
        return pr;
    }

    public void setPr(String pr) {
        this.pr = pr;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi18)) {
            return false;
        }
        CbNsi18 other = (CbNsi18) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi18[ idRecord=" + idRecord + " ]";
    }
    
}
