/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "doc", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Doc.findAll", query = "SELECT d FROM Doc d"),
    @NamedQuery(name = "Doc.findByIdRecord", query = "SELECT d FROM Doc d WHERE d.idRecord = :idRecord"),
    @NamedQuery(name = "Doc.findByIdTypedoc", query = "SELECT d FROM Doc d WHERE d.idTypedoc = :idTypedoc"),
    @NamedQuery(name = "Doc.findByIdOperation", query = "SELECT d FROM Doc d WHERE d.idOperation = :idOperation"),
    @NamedQuery(name = "Doc.findByIdDoc", query = "SELECT d FROM Doc d WHERE d.idDoc = :idDoc"),
    @NamedQuery(name = "Doc.findByIdStatusDoc", query = "SELECT d FROM Doc d WHERE d.idStatusDoc = :idStatusDoc"),
    @NamedQuery(name = "Doc.findByNameDoc", query = "SELECT d FROM Doc d WHERE d.nameDoc = :nameDoc"),
    @NamedQuery(name = "Doc.findByIdTemplate", query = "SELECT d FROM Doc d WHERE d.idTemplate = :idTemplate"),
    @NamedQuery(name = "Doc.findByIdParrentDoc", query = "SELECT d FROM Doc d WHERE d.idParrentDoc = :idParrentDoc"),
    @NamedQuery(name = "Doc.findByDatetimeCreateDoc", query = "SELECT d FROM Doc d WHERE d.datetimeCreateDoc = :datetimeCreateDoc"),
    @NamedQuery(name = "Doc.findByDatetimeLastEditdoc", query = "SELECT d FROM Doc d WHERE d.datetimeLastEditdoc = :datetimeLastEditdoc"),
    @NamedQuery(name = "Doc.findByUserCreateDoc", query = "SELECT d FROM Doc d WHERE d.userCreateDoc = :userCreateDoc"),
    @NamedQuery(name = "Doc.findByUserLastEditdoc", query = "SELECT d FROM Doc d WHERE d.userLastEditdoc = :userLastEditdoc"),
    @NamedQuery(name = "Doc.findByPathTemplateDoc", query = "SELECT d FROM Doc d WHERE d.pathTemplateDoc = :pathTemplateDoc"),
    @NamedQuery(name = "Doc.findByStatusRow", query = "SELECT d FROM Doc d WHERE d.statusRow = :statusRow")})
public class Doc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_typedoc")
    private Integer idTypedoc;
    @Column(name = "id_operation")
    private Integer idOperation;
    @Column(name = "id_doc")
    private Integer idDoc;
    @Column(name = "id_status_doc")
    private Integer idStatusDoc;
    @Column(name = "name_doc", length = 30)
    private String nameDoc;
    @Column(name = "id_template")
    private Integer idTemplate;
    @Column(name = "id_parrent_doc")
    private Integer idParrentDoc;
    @Column(name = "datetime_create_doc", length = 20)
    private String datetimeCreateDoc;
    @Column(name = "datetime_last_editdoc", length = 20)
    private String datetimeLastEditdoc;
    @Column(name = "user_create_doc", length = 30)
    private String userCreateDoc;
    @Column(name = "user_last_editdoc", length = 30)
    private String userLastEditdoc;
    @Column(name = "path_template_doc", length = 1000)
    private String pathTemplateDoc;
    @Column(name = "status_row")
    private Short statusRow;

    public Doc() {
    }

    public Doc(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdTypedoc() {
        return idTypedoc;
    }

    public void setIdTypedoc(Integer idTypedoc) {
        this.idTypedoc = idTypedoc;
    }

    public Integer getIdOperation() {
        return idOperation;
    }

    public void setIdOperation(Integer idOperation) {
        this.idOperation = idOperation;
    }

    public Integer getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(Integer idDoc) {
        this.idDoc = idDoc;
    }

    public Integer getIdStatusDoc() {
        return idStatusDoc;
    }

    public void setIdStatusDoc(Integer idStatusDoc) {
        this.idStatusDoc = idStatusDoc;
    }

    public String getNameDoc() {
        return nameDoc;
    }

    public void setNameDoc(String nameDoc) {
        this.nameDoc = nameDoc;
    }

    public Integer getIdTemplate() {
        return idTemplate;
    }

    public void setIdTemplate(Integer idTemplate) {
        this.idTemplate = idTemplate;
    }

    public Integer getIdParrentDoc() {
        return idParrentDoc;
    }

    public void setIdParrentDoc(Integer idParrentDoc) {
        this.idParrentDoc = idParrentDoc;
    }

    public String getDatetimeCreateDoc() {
        return datetimeCreateDoc;
    }

    public void setDatetimeCreateDoc(String datetimeCreateDoc) {
        this.datetimeCreateDoc = datetimeCreateDoc;
    }

    public String getDatetimeLastEditdoc() {
        return datetimeLastEditdoc;
    }

    public void setDatetimeLastEditdoc(String datetimeLastEditdoc) {
        this.datetimeLastEditdoc = datetimeLastEditdoc;
    }

    public String getUserCreateDoc() {
        return userCreateDoc;
    }

    public void setUserCreateDoc(String userCreateDoc) {
        this.userCreateDoc = userCreateDoc;
    }

    public String getUserLastEditdoc() {
        return userLastEditdoc;
    }

    public void setUserLastEditdoc(String userLastEditdoc) {
        this.userLastEditdoc = userLastEditdoc;
    }

    public String getPathTemplateDoc() {
        return pathTemplateDoc;
    }

    public void setPathTemplateDoc(String pathTemplateDoc) {
        this.pathTemplateDoc = pathTemplateDoc;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Doc)) {
            return false;
        }
        Doc other = (Doc) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.Doc[ idRecord=" + idRecord + " ]";
    }
    
}
