/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "firma", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Firma.findAll", query = "SELECT f FROM Firma f"),
    @NamedQuery(name = "Firma.findByIdRecord", query = "SELECT f FROM Firma f WHERE f.idRecord = :idRecord"),
    @NamedQuery(name = "Firma.findByLocalIdLombard", query = "SELECT f FROM Firma f WHERE f.localIdLombard = :localIdLombard"),
    @NamedQuery(name = "Firma.findByNameLombard", query = "SELECT f FROM Firma f WHERE f.nameLombard = :nameLombard"),
    @NamedQuery(name = "Firma.findByAdresLombard", query = "SELECT f FROM Firma f WHERE f.adresLombard = :adresLombard"),
    @NamedQuery(name = "Firma.findByShefLombard", query = "SELECT f FROM Firma f WHERE f.shefLombard = :shefLombard"),
    @NamedQuery(name = "Firma.findByNameBank", query = "SELECT f FROM Firma f WHERE f.nameBank = :nameBank"),
    @NamedQuery(name = "Firma.findByMfoBank", query = "SELECT f FROM Firma f WHERE f.mfoBank = :mfoBank"),
    @NamedQuery(name = "Firma.findByAccountNumberLombard", query = "SELECT f FROM Firma f WHERE f.accountNumberLombard = :accountNumberLombard"),
    @NamedQuery(name = "Firma.findByInnLombard", query = "SELECT f FROM Firma f WHERE f.innLombard = :innLombard"),
    @NamedQuery(name = "Firma.findByOkonxLombard", query = "SELECT f FROM Firma f WHERE f.okonxLombard = :okonxLombard"),
    @NamedQuery(name = "Firma.findByContactLombard", query = "SELECT f FROM Firma f WHERE f.contactLombard = :contactLombard"),
    @NamedQuery(name = "Firma.findByTypeOrg", query = "SELECT f FROM Firma f WHERE f.typeOrg = :typeOrg"),
    @NamedQuery(name = "Firma.findByCodeLombard", query = "SELECT f FROM Firma f WHERE f.codeLombard = :codeLombard"),
    @NamedQuery(name = "Firma.findByLicenseLombard", query = "SELECT f FROM Firma f WHERE f.licenseLombard = :licenseLombard"),
    @NamedQuery(name = "Firma.findByDateLicense", query = "SELECT f FROM Firma f WHERE f.dateLicense = :dateLicense"),
    @NamedQuery(name = "Firma.findByRegionLombard", query = "SELECT f FROM Firma f WHERE f.regionLombard = :regionLombard"),
    @NamedQuery(name = "Firma.findByDistrictLombard", query = "SELECT f FROM Firma f WHERE f.districtLombard = :districtLombard"),
    @NamedQuery(name = "Firma.findByStatusRow", query = "SELECT f FROM Firma f WHERE f.statusRow = :statusRow")})
public class Firma implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "local_id_lombard")
    private Integer localIdLombard;
    @Column(name = "name_lombard", length = 200)
    private String nameLombard;
    @Column(name = "adres_lombard", length = 200)
    private String adresLombard;
    @Column(name = "shef_lombard", length = 150)
    private String shefLombard;
    @Column(name = "name_bank", length = 150)
    private String nameBank;
    @Column(name = "mfo_bank", length = 6)
    private String mfoBank;
    @Column(name = "account_number_lombard", length = 25)
    private String accountNumberLombard;
    @Column(name = "inn_lombard", length = 10)
    private String innLombard;
    @Column(name = "okonx_lombard", length = 6)
    private String okonxLombard;
    @Column(name = "contact_lombard", length = 200)
    private String contactLombard;
    @Column(name = "type_org", length = 1)
    private String typeOrg;
    @Column(name = "code_lombard", length = 5)
    private String codeLombard;
    @Column(name = "license_lombard", length = 3)
    private String licenseLombard;
    @Column(name = "date_license", length = 10)
    private String dateLicense;
    @Column(name = "region_lombard", length = 2)
    private String regionLombard;
    @Column(name = "district_lombard", length = 3)
    private String districtLombard;
    @Column(name = "status_row")
    private Short statusRow;

    public Firma() {
    }

    public Firma(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getLocalIdLombard() {
        return localIdLombard;
    }

    public void setLocalIdLombard(Integer localIdLombard) {
        this.localIdLombard = localIdLombard;
    }

    public String getNameLombard() {
        return nameLombard;
    }

    public void setNameLombard(String nameLombard) {
        this.nameLombard = nameLombard;
    }

    public String getAdresLombard() {
        return adresLombard;
    }

    public void setAdresLombard(String adresLombard) {
        this.adresLombard = adresLombard;
    }

    public String getShefLombard() {
        return shefLombard;
    }

    public void setShefLombard(String shefLombard) {
        this.shefLombard = shefLombard;
    }

    public String getNameBank() {
        return nameBank;
    }

    public void setNameBank(String nameBank) {
        this.nameBank = nameBank;
    }

    public String getMfoBank() {
        return mfoBank;
    }

    public void setMfoBank(String mfoBank) {
        this.mfoBank = mfoBank;
    }

    public String getAccountNumberLombard() {
        return accountNumberLombard;
    }

    public void setAccountNumberLombard(String accountNumberLombard) {
        this.accountNumberLombard = accountNumberLombard;
    }

    public String getInnLombard() {
        return innLombard;
    }

    public void setInnLombard(String innLombard) {
        this.innLombard = innLombard;
    }

    public String getOkonxLombard() {
        return okonxLombard;
    }

    public void setOkonxLombard(String okonxLombard) {
        this.okonxLombard = okonxLombard;
    }

    public String getContactLombard() {
        return contactLombard;
    }

    public void setContactLombard(String contactLombard) {
        this.contactLombard = contactLombard;
    }

    public String getTypeOrg() {
        return typeOrg;
    }

    public void setTypeOrg(String typeOrg) {
        this.typeOrg = typeOrg;
    }

    public String getCodeLombard() {
        return codeLombard;
    }

    public void setCodeLombard(String codeLombard) {
        this.codeLombard = codeLombard;
    }

    public String getLicenseLombard() {
        return licenseLombard;
    }

    public void setLicenseLombard(String licenseLombard) {
        this.licenseLombard = licenseLombard;
    }

    public String getDateLicense() {
        return dateLicense;
    }

    public void setDateLicense(String dateLicense) {
        this.dateLicense = dateLicense;
    }

    public String getRegionLombard() {
        return regionLombard;
    }

    public void setRegionLombard(String regionLombard) {
        this.regionLombard = regionLombard;
    }

    public String getDistrictLombard() {
        return districtLombard;
    }

    public void setDistrictLombard(String districtLombard) {
        this.districtLombard = districtLombard;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Firma)) {
            return false;
        }
        Firma other = (Firma) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.Firma[ idRecord=" + idRecord + " ]";
    }
    
}
