/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_34", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi34.findAll", query = "SELECT c FROM CbNsi34 c"),
    @NamedQuery(name = "CbNsi34.findByNciId", query = "SELECT c FROM CbNsi34 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi34.findByKodGr", query = "SELECT c FROM CbNsi34 c WHERE c.kodGr = :kodGr"),
    @NamedQuery(name = "CbNsi34.findByKodPgr", query = "SELECT c FROM CbNsi34 c WHERE c.kodPgr = :kodPgr"),
    @NamedQuery(name = "CbNsi34.findByShifrId", query = "SELECT c FROM CbNsi34 c WHERE c.shifrId = :shifrId"),
    @NamedQuery(name = "CbNsi34.findByShifrName", query = "SELECT c FROM CbNsi34 c WHERE c.shifrName = :shifrName"),
    @NamedQuery(name = "CbNsi34.findByDateOpen", query = "SELECT c FROM CbNsi34 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi34.findByDateClose", query = "SELECT c FROM CbNsi34 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi34.findByActive", query = "SELECT c FROM CbNsi34 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi34.findByIdRecord", query = "SELECT c FROM CbNsi34 c WHERE c.idRecord = :idRecord")})
public class CbNsi34 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Column(name = "kod_gr", length = 10)
    private String kodGr;
    @Column(name = "kod_pgr", length = 10)
    private String kodPgr;
    @Basic(optional = false)
    @Column(name = "shifr_id", nullable = false, length = 2)
    private String shifrId;
    @Column(name = "shifr_name", length = 200)
    private String shifrName;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Column(name = "date_close", length = 10)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi34() {
    }

    public CbNsi34(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public CbNsi34(Integer idRecord, String shifrId) {
        this.idRecord = idRecord;
        this.shifrId = shifrId;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getKodGr() {
        return kodGr;
    }

    public void setKodGr(String kodGr) {
        this.kodGr = kodGr;
    }

    public String getKodPgr() {
        return kodPgr;
    }

    public void setKodPgr(String kodPgr) {
        this.kodPgr = kodPgr;
    }

    public String getShifrId() {
        return shifrId;
    }

    public void setShifrId(String shifrId) {
        this.shifrId = shifrId;
    }

    public String getShifrName() {
        return shifrName;
    }

    public void setShifrName(String shifrName) {
        this.shifrName = shifrName;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi34)) {
            return false;
        }
        CbNsi34 other = (CbNsi34) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi34[ idRecord=" + idRecord + " ]";
    }
    
}
