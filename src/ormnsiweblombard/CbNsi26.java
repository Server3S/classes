/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_26", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi26.findAll", query = "SELECT c FROM CbNsi26 c"),
    @NamedQuery(name = "CbNsi26.findByNciId", query = "SELECT c FROM CbNsi26 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi26.findByKodDoc", query = "SELECT c FROM CbNsi26 c WHERE c.kodDoc = :kodDoc"),
    @NamedQuery(name = "CbNsi26.findByNaimDoc", query = "SELECT c FROM CbNsi26 c WHERE c.naimDoc = :naimDoc"),
    @NamedQuery(name = "CbNsi26.findByDateOpen", query = "SELECT c FROM CbNsi26 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi26.findByDateClose", query = "SELECT c FROM CbNsi26 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi26.findByActive", query = "SELECT c FROM CbNsi26 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi26.findByIdRecord", query = "SELECT c FROM CbNsi26 c WHERE c.idRecord = :idRecord")})
public class CbNsi26 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Column(name = "kod_doc", length = 10)
    private String kodDoc;
    @Column(name = "naim_doc", length = 30)
    private String naimDoc;
    @Column(name = "date_open", length = 8)
    private String dateOpen;
    @Column(name = "date_close", length = 8)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi26() {
    }

    public CbNsi26(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getKodDoc() {
        return kodDoc;
    }

    public void setKodDoc(String kodDoc) {
        this.kodDoc = kodDoc;
    }

    public String getNaimDoc() {
        return naimDoc;
    }

    public void setNaimDoc(String naimDoc) {
        this.naimDoc = naimDoc;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi26)) {
            return false;
        }
        CbNsi26 other = (CbNsi26) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi26[ idRecord=" + idRecord + " ]";
    }
    
}
