/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "nsi_translit", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NsiTranslit.findAll", query = "SELECT n FROM NsiTranslit n"),
    @NamedQuery(name = "NsiTranslit.findByCyrillic", query = "SELECT n FROM NsiTranslit n WHERE n.cyrillic = :cyrillic"),
    @NamedQuery(name = "NsiTranslit.findByDecCyr", query = "SELECT n FROM NsiTranslit n WHERE n.decCyr = :decCyr"),
    @NamedQuery(name = "NsiTranslit.findByHexCyr", query = "SELECT n FROM NsiTranslit n WHERE n.hexCyr = :hexCyr"),
    @NamedQuery(name = "NsiTranslit.findByToLatin", query = "SELECT n FROM NsiTranslit n WHERE n.toLatin = :toLatin"),
    @NamedQuery(name = "NsiTranslit.findByAsciiHexLatin", query = "SELECT n FROM NsiTranslit n WHERE n.asciiHexLatin = :asciiHexLatin"),
    @NamedQuery(name = "NsiTranslit.findByForSearch", query = "SELECT n FROM NsiTranslit n WHERE n.forSearch = :forSearch"),
    @NamedQuery(name = "NsiTranslit.findByAsciiDecLatin", query = "SELECT n FROM NsiTranslit n WHERE n.asciiDecLatin = :asciiDecLatin"),
    @NamedQuery(name = "NsiTranslit.findByIdRecord", query = "SELECT n FROM NsiTranslit n WHERE n.idRecord = :idRecord")})
public class NsiTranslit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "cyrillic", length = 1)
    private String cyrillic;
    @Column(name = "dec_cyr")
    private Integer decCyr;
    @Column(name = "hex_cyr", length = 5)
    private String hexCyr;
    @Lob
    @Column(name = "position_cyr")
    private Object positionCyr;
    @Lob
    @Column(name = "after_cyr")
    private Object afterCyr;
    @Lob
    @Column(name = "hard_cyr")
    private Object hardCyr;
    @Lob
    @Column(name = "voiced_cyr")
    private Object voicedCyr;
    @Lob
    @Column(name = "glas_cyr")
    private Object glasCyr;
    @Column(name = "to_latin", length = 3)
    private String toLatin;
    @Column(name = "ascii_hex_latin", length = 5)
    private String asciiHexLatin;
    @Column(name = "for_search", length = 15)
    private String forSearch;
    @Column(name = "ascii_dec_latin", length = 10)
    private String asciiDecLatin;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NsiTranslit() {
    }

    public NsiTranslit(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getCyrillic() {
        return cyrillic;
    }

    public void setCyrillic(String cyrillic) {
        this.cyrillic = cyrillic;
    }

    public Integer getDecCyr() {
        return decCyr;
    }

    public void setDecCyr(Integer decCyr) {
        this.decCyr = decCyr;
    }

    public String getHexCyr() {
        return hexCyr;
    }

    public void setHexCyr(String hexCyr) {
        this.hexCyr = hexCyr;
    }

    public Object getPositionCyr() {
        return positionCyr;
    }

    public void setPositionCyr(Object positionCyr) {
        this.positionCyr = positionCyr;
    }

    public Object getAfterCyr() {
        return afterCyr;
    }

    public void setAfterCyr(Object afterCyr) {
        this.afterCyr = afterCyr;
    }

    public Object getHardCyr() {
        return hardCyr;
    }

    public void setHardCyr(Object hardCyr) {
        this.hardCyr = hardCyr;
    }

    public Object getVoicedCyr() {
        return voicedCyr;
    }

    public void setVoicedCyr(Object voicedCyr) {
        this.voicedCyr = voicedCyr;
    }

    public Object getGlasCyr() {
        return glasCyr;
    }

    public void setGlasCyr(Object glasCyr) {
        this.glasCyr = glasCyr;
    }

    public String getToLatin() {
        return toLatin;
    }

    public void setToLatin(String toLatin) {
        this.toLatin = toLatin;
    }

    public String getAsciiHexLatin() {
        return asciiHexLatin;
    }

    public void setAsciiHexLatin(String asciiHexLatin) {
        this.asciiHexLatin = asciiHexLatin;
    }

    public String getForSearch() {
        return forSearch;
    }

    public void setForSearch(String forSearch) {
        this.forSearch = forSearch;
    }

    public String getAsciiDecLatin() {
        return asciiDecLatin;
    }

    public void setAsciiDecLatin(String asciiDecLatin) {
        this.asciiDecLatin = asciiDecLatin;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NsiTranslit)) {
            return false;
        }
        NsiTranslit other = (NsiTranslit) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NsiTranslit[ idRecord=" + idRecord + " ]";
    }
    
}
