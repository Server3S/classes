/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "category_lient", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoryLient.findAll", query = "SELECT c FROM CategoryLient c"),
    @NamedQuery(name = "CategoryLient.findByIdRecord", query = "SELECT c FROM CategoryLient c WHERE c.idRecord = :idRecord"),
    @NamedQuery(name = "CategoryLient.findByIdCategoryLien", query = "SELECT c FROM CategoryLient c WHERE c.idCategoryLien = :idCategoryLien"),
    @NamedQuery(name = "CategoryLient.findByNameCategoryLien", query = "SELECT c FROM CategoryLient c WHERE c.nameCategoryLien = :nameCategoryLien"),
    @NamedQuery(name = "CategoryLient.findByStatusRow", query = "SELECT c FROM CategoryLient c WHERE c.statusRow = :statusRow")})
public class CategoryLient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_category_lien")
    private Integer idCategoryLien;
    @Column(name = "name_category_lien", length = 100)
    private String nameCategoryLien;
    @Column(name = "status_row")
    private Short statusRow;

    public CategoryLient() {
    }

    public CategoryLient(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdCategoryLien() {
        return idCategoryLien;
    }

    public void setIdCategoryLien(Integer idCategoryLien) {
        this.idCategoryLien = idCategoryLien;
    }

    public String getNameCategoryLien() {
        return nameCategoryLien;
    }

    public void setNameCategoryLien(String nameCategoryLien) {
        this.nameCategoryLien = nameCategoryLien;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoryLient)) {
            return false;
        }
        CategoryLient other = (CategoryLient) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.CategoryLient[ idRecord=" + idRecord + " ]";
    }
    
}
