/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_52", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi52.findAll", query = "SELECT c FROM CbNsi52 c"),
    @NamedQuery(name = "CbNsi52.findByNciId", query = "SELECT c FROM CbNsi52 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi52.findByDistr", query = "SELECT c FROM CbNsi52 c WHERE c.distr = :distr"),
    @NamedQuery(name = "CbNsi52.findByDistrName", query = "SELECT c FROM CbNsi52 c WHERE c.distrName = :distrName"),
    @NamedQuery(name = "CbNsi52.findByRegionId", query = "SELECT c FROM CbNsi52 c WHERE c.regionId = :regionId"),
    @NamedQuery(name = "CbNsi52.findByDateOpen", query = "SELECT c FROM CbNsi52 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi52.findByDateClose", query = "SELECT c FROM CbNsi52 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi52.findByActive", query = "SELECT c FROM CbNsi52 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi52.findByIdRecord", query = "SELECT c FROM CbNsi52 c WHERE c.idRecord = :idRecord")})
public class CbNsi52 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Basic(optional = false)
    @Column(name = "distr", nullable = false, length = 10)
    private String distr;
    @Column(name = "distr_name", length = 30)
    private String distrName;
    @Column(name = "region_id", length = 10)
    private String regionId;
    @Column(name = "date_open", length = 8)
    private String dateOpen;
    @Column(name = "date_close", length = 8)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi52() {
    }

    public CbNsi52(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public CbNsi52(Integer idRecord, String distr) {
        this.idRecord = idRecord;
        this.distr = distr;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getDistr() {
        return distr;
    }

    public void setDistr(String distr) {
        this.distr = distr;
    }

    public String getDistrName() {
        return distrName;
    }

    public void setDistrName(String distrName) {
        this.distrName = distrName;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi52)) {
            return false;
        }
        CbNsi52 other = (CbNsi52) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi52[ idRecord=" + idRecord + " ]";
    }
    
}
