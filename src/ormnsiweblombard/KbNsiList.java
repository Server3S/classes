/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "kb_nsi_list", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KbNsiList.findAll", query = "SELECT k FROM KbNsiList k"),
    @NamedQuery(name = "KbNsiList.findByKod", query = "SELECT k FROM KbNsiList k WHERE k.kod = :kod"),
    @NamedQuery(name = "KbNsiList.findByNaim", query = "SELECT k FROM KbNsiList k WHERE k.naim = :naim"),
    @NamedQuery(name = "KbNsiList.findByPrimech", query = "SELECT k FROM KbNsiList k WHERE k.primech = :primech"),
    @NamedQuery(name = "KbNsiList.findByIdRecord", query = "SELECT k FROM KbNsiList k WHERE k.idRecord = :idRecord")})
public class KbNsiList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "kod", length = 3)
    private String kod;
    @Column(name = "naim", length = 100)
    private String naim;
    @Column(name = "primech", length = 100)
    private String primech;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public KbNsiList() {
    }

    public KbNsiList(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getNaim() {
        return naim;
    }

    public void setNaim(String naim) {
        this.naim = naim;
    }

    public String getPrimech() {
        return primech;
    }

    public void setPrimech(String primech) {
        this.primech = primech;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KbNsiList)) {
            return false;
        }
        KbNsiList other = (KbNsiList) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.KbNsiList[ idRecord=" + idRecord + " ]";
    }
    
}
