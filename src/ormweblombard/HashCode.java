/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "hash_code", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HashCode.findAll", query = "SELECT h FROM HashCode h"),
    @NamedQuery(name = "HashCode.findByIdUser", query = "SELECT h FROM HashCode h WHERE h.idUser = :idUser"),
    @NamedQuery(name = "HashCode.findByHashCode", query = "SELECT h FROM HashCode h WHERE h.hashCode = :hashCode"),
    @NamedQuery(name = "HashCode.findByDatetimeChangeHashcode", query = "SELECT h FROM HashCode h WHERE h.datetimeChangeHashcode = :datetimeChangeHashcode"),
    @NamedQuery(name = "HashCode.findByExpireDatePasw", query = "SELECT h FROM HashCode h WHERE h.expireDatePasw = :expireDatePasw"),
    @NamedQuery(name = "HashCode.findByLastHashPasw", query = "SELECT h FROM HashCode h WHERE h.lastHashPasw = :lastHashPasw"),
    @NamedQuery(name = "HashCode.findByIdRecord", query = "SELECT h FROM HashCode h WHERE h.idRecord = :idRecord"),
    @NamedQuery(name = "HashCode.findByNotChangePasw", query = "SELECT h FROM HashCode h WHERE h.notChangePasw = :notChangePasw"),
    @NamedQuery(name = "HashCode.findByStatusRow", query = "SELECT h FROM HashCode h WHERE h.statusRow = :statusRow")})
public class HashCode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "id_user")
    private Integer idUser;
    @Column(name = "hash_code", length = 100)
    private String hashCode;
    @Column(name = "datetime_change_hashcode", length = 20)
    private String datetimeChangeHashcode;
    @Column(name = "expire_date_pasw", length = 10)
    private String expireDatePasw;
    @Column(name = "last_hash_pasw", length = 100)
    private String lastHashPasw;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "not_change_pasw")
    private Short notChangePasw;
    @Column(name = "status_row")
    private Short statusRow;

    public HashCode() {
    }

    public HashCode(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getHashCode() {
        return hashCode;
    }

    public void setHashCode(String hashCode) {
        this.hashCode = hashCode;
    }

    public String getDatetimeChangeHashcode() {
        return datetimeChangeHashcode;
    }

    public void setDatetimeChangeHashcode(String datetimeChangeHashcode) {
        this.datetimeChangeHashcode = datetimeChangeHashcode;
    }

    public String getExpireDatePasw() {
        return expireDatePasw;
    }

    public void setExpireDatePasw(String expireDatePasw) {
        this.expireDatePasw = expireDatePasw;
    }

    public String getLastHashPasw() {
        return lastHashPasw;
    }

    public void setLastHashPasw(String lastHashPasw) {
        this.lastHashPasw = lastHashPasw;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Short getNotChangePasw() {
        return notChangePasw;
    }

    public void setNotChangePasw(Short notChangePasw) {
        this.notChangePasw = notChangePasw;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HashCode)) {
            return false;
        }
        HashCode other = (HashCode) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.HashCode[ idRecord=" + idRecord + " ]";
    }
    
}
