/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "account_posting", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountPosting.findAll", query = "SELECT a FROM AccountPosting a"),
    @NamedQuery(name = "AccountPosting.findByIdRecord", query = "SELECT a FROM AccountPosting a WHERE a.idRecord = :idRecord"),
    @NamedQuery(name = "AccountPosting.findByIdDocPosting", query = "SELECT a FROM AccountPosting a WHERE a.idDocPosting = :idDocPosting"),
    @NamedQuery(name = "AccountPosting.findByCodeOperationPosting", query = "SELECT a FROM AccountPosting a WHERE a.codeOperationPosting = :codeOperationPosting"),
    @NamedQuery(name = "AccountPosting.findByDatetimePosting", query = "SELECT a FROM AccountPosting a WHERE a.datetimePosting = :datetimePosting"),
    @NamedQuery(name = "AccountPosting.findByIdPaydoc", query = "SELECT a FROM AccountPosting a WHERE a.idPaydoc = :idPaydoc"),
    @NamedQuery(name = "AccountPosting.findByIdOperday", query = "SELECT a FROM AccountPosting a WHERE a.idOperday = :idOperday"),
    @NamedQuery(name = "AccountPosting.findByIdUserManualPosting", query = "SELECT a FROM AccountPosting a WHERE a.idUserManualPosting = :idUserManualPosting"),
    @NamedQuery(name = "AccountPosting.findBySumPosting", query = "SELECT a FROM AccountPosting a WHERE a.sumPosting = :sumPosting"),
    @NamedQuery(name = "AccountPosting.findByDebitAccount", query = "SELECT a FROM AccountPosting a WHERE a.debitAccount = :debitAccount"),
    @NamedQuery(name = "AccountPosting.findByCreditAccount", query = "SELECT a FROM AccountPosting a WHERE a.creditAccount = :creditAccount"),
    @NamedQuery(name = "AccountPosting.findByDatetimeCancelPosting", query = "SELECT a FROM AccountPosting a WHERE a.datetimeCancelPosting = :datetimeCancelPosting"),
    @NamedQuery(name = "AccountPosting.findByUserCancelPosting", query = "SELECT a FROM AccountPosting a WHERE a.userCancelPosting = :userCancelPosting"),
    @NamedQuery(name = "AccountPosting.findByDescriptCanselPosting", query = "SELECT a FROM AccountPosting a WHERE a.descriptCanselPosting = :descriptCanselPosting"),
    @NamedQuery(name = "AccountPosting.findByIdTemplatePosting", query = "SELECT a FROM AccountPosting a WHERE a.idTemplatePosting = :idTemplatePosting"),
    @NamedQuery(name = "AccountPosting.findByStatusRow", query = "SELECT a FROM AccountPosting a WHERE a.statusRow = :statusRow"),
    @NamedQuery(name = "AccountPosting.findByAtribCancelPosting", query = "SELECT a FROM AccountPosting a WHERE a.atribCancelPosting = :atribCancelPosting")})
public class AccountPosting implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_doc_posting")
    private Integer idDocPosting;
    @Column(name = "code_operation_posting")
    private Integer codeOperationPosting;
    @Column(name = "datetime_posting", length = 20)
    private String datetimePosting;
    @Column(name = "id_paydoc")
    private Integer idPaydoc;
    @Column(name = "id_operday")
    private Integer idOperday;
    @Column(name = "id_user_manual_posting")
    private Integer idUserManualPosting;
    @Column(name = "sum_posting")
    private BigInteger sumPosting;
    @Column(name = "debit_account")
    private Integer debitAccount;
    @Column(name = "credit_account")
    private Integer creditAccount;
    @Column(name = "datetime_cancel_posting", length = 20)
    private String datetimeCancelPosting;
    @Column(name = "user_cancel_posting", length = 30)
    private String userCancelPosting;
    @Column(name = "descript_cansel_posting", length = 100)
    private String descriptCanselPosting;
    @Column(name = "id_template_posting")
    private Integer idTemplatePosting;
    @Column(name = "status_row")
    private Short statusRow;
    @Column(name = "atrib_cancel_posting")
    private Short atribCancelPosting;

    public AccountPosting() {
    }

    public AccountPosting(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdDocPosting() {
        return idDocPosting;
    }

    public void setIdDocPosting(Integer idDocPosting) {
        this.idDocPosting = idDocPosting;
    }

    public Integer getCodeOperationPosting() {
        return codeOperationPosting;
    }

    public void setCodeOperationPosting(Integer codeOperationPosting) {
        this.codeOperationPosting = codeOperationPosting;
    }

    public String getDatetimePosting() {
        return datetimePosting;
    }

    public void setDatetimePosting(String datetimePosting) {
        this.datetimePosting = datetimePosting;
    }

    public Integer getIdPaydoc() {
        return idPaydoc;
    }

    public void setIdPaydoc(Integer idPaydoc) {
        this.idPaydoc = idPaydoc;
    }

    public Integer getIdOperday() {
        return idOperday;
    }

    public void setIdOperday(Integer idOperday) {
        this.idOperday = idOperday;
    }

    public Integer getIdUserManualPosting() {
        return idUserManualPosting;
    }

    public void setIdUserManualPosting(Integer idUserManualPosting) {
        this.idUserManualPosting = idUserManualPosting;
    }

    public BigInteger getSumPosting() {
        return sumPosting;
    }

    public void setSumPosting(BigInteger sumPosting) {
        this.sumPosting = sumPosting;
    }

    public Integer getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(Integer debitAccount) {
        this.debitAccount = debitAccount;
    }

    public Integer getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(Integer creditAccount) {
        this.creditAccount = creditAccount;
    }

    public String getDatetimeCancelPosting() {
        return datetimeCancelPosting;
    }

    public void setDatetimeCancelPosting(String datetimeCancelPosting) {
        this.datetimeCancelPosting = datetimeCancelPosting;
    }

    public String getUserCancelPosting() {
        return userCancelPosting;
    }

    public void setUserCancelPosting(String userCancelPosting) {
        this.userCancelPosting = userCancelPosting;
    }

    public String getDescriptCanselPosting() {
        return descriptCanselPosting;
    }

    public void setDescriptCanselPosting(String descriptCanselPosting) {
        this.descriptCanselPosting = descriptCanselPosting;
    }

    public Integer getIdTemplatePosting() {
        return idTemplatePosting;
    }

    public void setIdTemplatePosting(Integer idTemplatePosting) {
        this.idTemplatePosting = idTemplatePosting;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    public Short getAtribCancelPosting() {
        return atribCancelPosting;
    }

    public void setAtribCancelPosting(Short atribCancelPosting) {
        this.atribCancelPosting = atribCancelPosting;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountPosting)) {
            return false;
        }
        AccountPosting other = (AccountPosting) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.AccountPosting[ idRecord=" + idRecord + " ]";
    }
    
}
