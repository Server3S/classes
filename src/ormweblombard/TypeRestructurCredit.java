/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_restructur_credit", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeRestructurCredit.findAll", query = "SELECT t FROM TypeRestructurCredit t"),
    @NamedQuery(name = "TypeRestructurCredit.findByIdRecord", query = "SELECT t FROM TypeRestructurCredit t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeRestructurCredit.findByIdRestructur", query = "SELECT t FROM TypeRestructurCredit t WHERE t.idRestructur = :idRestructur"),
    @NamedQuery(name = "TypeRestructurCredit.findByNameRestructur", query = "SELECT t FROM TypeRestructurCredit t WHERE t.nameRestructur = :nameRestructur"),
    @NamedQuery(name = "TypeRestructurCredit.findByActionRestructur", query = "SELECT t FROM TypeRestructurCredit t WHERE t.actionRestructur = :actionRestructur"),
    @NamedQuery(name = "TypeRestructurCredit.findByDescriptRestructur", query = "SELECT t FROM TypeRestructurCredit t WHERE t.descriptRestructur = :descriptRestructur"),
    @NamedQuery(name = "TypeRestructurCredit.findByStatusRow", query = "SELECT t FROM TypeRestructurCredit t WHERE t.statusRow = :statusRow")})
public class TypeRestructurCredit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_restructur")
    private Integer idRestructur;
    @Column(name = "name_restructur", length = 100)
    private String nameRestructur;
    @Column(name = "action_restructur", length = 100)
    private String actionRestructur;
    @Column(name = "descript_restructur", length = 500)
    private String descriptRestructur;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeRestructurCredit() {
    }

    public TypeRestructurCredit(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRestructur() {
        return idRestructur;
    }

    public void setIdRestructur(Integer idRestructur) {
        this.idRestructur = idRestructur;
    }

    public String getNameRestructur() {
        return nameRestructur;
    }

    public void setNameRestructur(String nameRestructur) {
        this.nameRestructur = nameRestructur;
    }

    public String getActionRestructur() {
        return actionRestructur;
    }

    public void setActionRestructur(String actionRestructur) {
        this.actionRestructur = actionRestructur;
    }

    public String getDescriptRestructur() {
        return descriptRestructur;
    }

    public void setDescriptRestructur(String descriptRestructur) {
        this.descriptRestructur = descriptRestructur;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeRestructurCredit)) {
            return false;
        }
        TypeRestructurCredit other = (TypeRestructurCredit) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeRestructurCredit[ idRecord=" + idRecord + " ]";
    }
    
}
