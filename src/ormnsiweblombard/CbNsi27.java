/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_27", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi27.findAll", query = "SELECT c FROM CbNsi27 c"),
    @NamedQuery(name = "CbNsi27.findByNciId", query = "SELECT c FROM CbNsi27 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi27.findByKodRez", query = "SELECT c FROM CbNsi27 c WHERE c.kodRez = :kodRez"),
    @NamedQuery(name = "CbNsi27.findByTypeRez", query = "SELECT c FROM CbNsi27 c WHERE c.typeRez = :typeRez"),
    @NamedQuery(name = "CbNsi27.findByDateOpen", query = "SELECT c FROM CbNsi27 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi27.findByDateClose", query = "SELECT c FROM CbNsi27 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi27.findByActive", query = "SELECT c FROM CbNsi27 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi27.findByIdRecord", query = "SELECT c FROM CbNsi27 c WHERE c.idRecord = :idRecord")})
public class CbNsi27 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Basic(optional = false)
    @Column(name = "kod_rez", nullable = false, length = 10)
    private String kodRez;
    @Column(name = "type_rez", length = 15)
    private String typeRez;
    @Column(name = "date_open", length = 8)
    private String dateOpen;
    @Column(name = "date_close", length = 8)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi27() {
    }

    public CbNsi27(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public CbNsi27(Integer idRecord, String kodRez) {
        this.idRecord = idRecord;
        this.kodRez = kodRez;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getKodRez() {
        return kodRez;
    }

    public void setKodRez(String kodRez) {
        this.kodRez = kodRez;
    }

    public String getTypeRez() {
        return typeRez;
    }

    public void setTypeRez(String typeRez) {
        this.typeRez = typeRez;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi27)) {
            return false;
        }
        CbNsi27 other = (CbNsi27) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi27[ idRecord=" + idRecord + " ]";
    }
    
}
