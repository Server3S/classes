/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_36", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi36.findAll", query = "SELECT c FROM CbNsi36 c"),
    @NamedQuery(name = "CbNsi36.findByNciId", query = "SELECT c FROM CbNsi36 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi36.findByKodKlassa", query = "SELECT c FROM CbNsi36 c WHERE c.kodKlassa = :kodKlassa"),
    @NamedQuery(name = "CbNsi36.findByNameKlassa", query = "SELECT c FROM CbNsi36 c WHERE c.nameKlassa = :nameKlassa"),
    @NamedQuery(name = "CbNsi36.findByProc", query = "SELECT c FROM CbNsi36 c WHERE c.proc = :proc"),
    @NamedQuery(name = "CbNsi36.findByDataOpen", query = "SELECT c FROM CbNsi36 c WHERE c.dataOpen = :dataOpen"),
    @NamedQuery(name = "CbNsi36.findByDataClose", query = "SELECT c FROM CbNsi36 c WHERE c.dataClose = :dataClose"),
    @NamedQuery(name = "CbNsi36.findByActive", query = "SELECT c FROM CbNsi36 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi36.findByIdRecord", query = "SELECT c FROM CbNsi36 c WHERE c.idRecord = :idRecord")})
public class CbNsi36 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Column(name = "kod_klassa", length = 10)
    private String kodKlassa;
    @Column(name = "name_klassa", length = 50)
    private String nameKlassa;
    @Column(name = "proc", length = 3)
    private String proc;
    @Column(name = "data_open", length = 8)
    private String dataOpen;
    @Column(name = "data_close", length = 8)
    private String dataClose;
    @Column(name = "active", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi36() {
    }

    public CbNsi36(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getKodKlassa() {
        return kodKlassa;
    }

    public void setKodKlassa(String kodKlassa) {
        this.kodKlassa = kodKlassa;
    }

    public String getNameKlassa() {
        return nameKlassa;
    }

    public void setNameKlassa(String nameKlassa) {
        this.nameKlassa = nameKlassa;
    }

    public String getProc() {
        return proc;
    }

    public void setProc(String proc) {
        this.proc = proc;
    }

    public String getDataOpen() {
        return dataOpen;
    }

    public void setDataOpen(String dataOpen) {
        this.dataOpen = dataOpen;
    }

    public String getDataClose() {
        return dataClose;
    }

    public void setDataClose(String dataClose) {
        this.dataClose = dataClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi36)) {
            return false;
        }
        CbNsi36 other = (CbNsi36) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi36[ idRecord=" + idRecord + " ]";
    }
    
}
