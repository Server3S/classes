/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "kb_typeinfo_toasoki", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KbTypeinfoToasoki.findAll", query = "SELECT k FROM KbTypeinfoToasoki k"),
    @NamedQuery(name = "KbTypeinfoToasoki.findByIdRecord", query = "SELECT k FROM KbTypeinfoToasoki k WHERE k.idRecord = :idRecord"),
    @NamedQuery(name = "KbTypeinfoToasoki.findByCodeGroupReport", query = "SELECT k FROM KbTypeinfoToasoki k WHERE k.codeGroupReport = :codeGroupReport"),
    @NamedQuery(name = "KbTypeinfoToasoki.findByNumberReport", query = "SELECT k FROM KbTypeinfoToasoki k WHERE k.numberReport = :numberReport"),
    @NamedQuery(name = "KbTypeinfoToasoki.findByNameReport", query = "SELECT k FROM KbTypeinfoToasoki k WHERE k.nameReport = :nameReport"),
    @NamedQuery(name = "KbTypeinfoToasoki.findByPeriodReport", query = "SELECT k FROM KbTypeinfoToasoki k WHERE k.periodReport = :periodReport"),
    @NamedQuery(name = "KbTypeinfoToasoki.findByNameOldTable", query = "SELECT k FROM KbTypeinfoToasoki k WHERE k.nameOldTable = :nameOldTable")})
public class KbTypeinfoToasoki implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "code_group_report", length = 3)
    private String codeGroupReport;
    @Column(name = "number_report", length = 3)
    private String numberReport;
    @Column(name = "name_report", length = 100)
    private String nameReport;
    @Column(name = "period_report", length = 25)
    private String periodReport;
    @Column(name = "name_old_table", length = 25)
    private String nameOldTable;
    @Lob
    @Column(name = "status_report")
    private Object statusReport;

    public KbTypeinfoToasoki() {
    }

    public KbTypeinfoToasoki(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getCodeGroupReport() {
        return codeGroupReport;
    }

    public void setCodeGroupReport(String codeGroupReport) {
        this.codeGroupReport = codeGroupReport;
    }

    public String getNumberReport() {
        return numberReport;
    }

    public void setNumberReport(String numberReport) {
        this.numberReport = numberReport;
    }

    public String getNameReport() {
        return nameReport;
    }

    public void setNameReport(String nameReport) {
        this.nameReport = nameReport;
    }

    public String getPeriodReport() {
        return periodReport;
    }

    public void setPeriodReport(String periodReport) {
        this.periodReport = periodReport;
    }

    public String getNameOldTable() {
        return nameOldTable;
    }

    public void setNameOldTable(String nameOldTable) {
        this.nameOldTable = nameOldTable;
    }

    public Object getStatusReport() {
        return statusReport;
    }

    public void setStatusReport(Object statusReport) {
        this.statusReport = statusReport;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KbTypeinfoToasoki)) {
            return false;
        }
        KbTypeinfoToasoki other = (KbTypeinfoToasoki) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.KbTypeinfoToasoki[ idRecord=" + idRecord + " ]";
    }
    
}
