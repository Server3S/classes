/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "functions", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Functions.findAll", query = "SELECT f FROM Functions f"),
    @NamedQuery(name = "Functions.findByIdRecord", query = "SELECT f FROM Functions f WHERE f.idRecord = :idRecord"),
    @NamedQuery(name = "Functions.findByIdFunction", query = "SELECT f FROM Functions f WHERE f.idFunction = :idFunction"),
    @NamedQuery(name = "Functions.findByNameFunction", query = "SELECT f FROM Functions f WHERE f.nameFunction = :nameFunction"),
    @NamedQuery(name = "Functions.findByDescriptFunction", query = "SELECT f FROM Functions f WHERE f.descriptFunction = :descriptFunction"),
    @NamedQuery(name = "Functions.findByUserCreateFunction", query = "SELECT f FROM Functions f WHERE f.userCreateFunction = :userCreateFunction"),
    @NamedQuery(name = "Functions.findByDatetimeCreateFunction", query = "SELECT f FROM Functions f WHERE f.datetimeCreateFunction = :datetimeCreateFunction"),
    @NamedQuery(name = "Functions.findByAtribPublicFunction", query = "SELECT f FROM Functions f WHERE f.atribPublicFunction = :atribPublicFunction"),
    @NamedQuery(name = "Functions.findByStatusRow", query = "SELECT f FROM Functions f WHERE f.statusRow = :statusRow")})
public class Functions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_function")
    private Integer idFunction;
    @Column(name = "name_function", length = 50)
    private String nameFunction;
    @Column(name = "descript_function", length = 200)
    private String descriptFunction;
    @Column(name = "user_create_function", length = 30)
    private String userCreateFunction;
    @Column(name = "datetime_create_function", length = 20)
    private String datetimeCreateFunction;
    @Column(name = "atrib_public_function")
    private Short atribPublicFunction;
    @Column(name = "status_row")
    private Short statusRow;

    public Functions() {
    }

    public Functions(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdFunction() {
        return idFunction;
    }

    public void setIdFunction(Integer idFunction) {
        this.idFunction = idFunction;
    }

    public String getNameFunction() {
        return nameFunction;
    }

    public void setNameFunction(String nameFunction) {
        this.nameFunction = nameFunction;
    }

    public String getDescriptFunction() {
        return descriptFunction;
    }

    public void setDescriptFunction(String descriptFunction) {
        this.descriptFunction = descriptFunction;
    }

    public String getUserCreateFunction() {
        return userCreateFunction;
    }

    public void setUserCreateFunction(String userCreateFunction) {
        this.userCreateFunction = userCreateFunction;
    }

    public String getDatetimeCreateFunction() {
        return datetimeCreateFunction;
    }

    public void setDatetimeCreateFunction(String datetimeCreateFunction) {
        this.datetimeCreateFunction = datetimeCreateFunction;
    }

    public Short getAtribPublicFunction() {
        return atribPublicFunction;
    }

    public void setAtribPublicFunction(Short atribPublicFunction) {
        this.atribPublicFunction = atribPublicFunction;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Functions)) {
            return false;
        }
        Functions other = (Functions) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.Functions[ idRecord=" + idRecord + " ]";
    }
    
}
