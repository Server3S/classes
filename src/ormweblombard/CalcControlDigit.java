/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "calc_control_digit", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CalcControlDigit.findAll", query = "SELECT c FROM CalcControlDigit c"),
    @NamedQuery(name = "CalcControlDigit.findByIdRecord", query = "SELECT c FROM CalcControlDigit c WHERE c.idRecord = :idRecord"),
    @NamedQuery(name = "CalcControlDigit.findByNameVariable", query = "SELECT c FROM CalcControlDigit c WHERE c.nameVariable = :nameVariable"),
    @NamedQuery(name = "CalcControlDigit.findByDefaultValueVariable", query = "SELECT c FROM CalcControlDigit c WHERE c.defaultValueVariable = :defaultValueVariable"),
    @NamedQuery(name = "CalcControlDigit.findByDescriptActionStep", query = "SELECT c FROM CalcControlDigit c WHERE c.descriptActionStep = :descriptActionStep"),
    @NamedQuery(name = "CalcControlDigit.findByVariable", query = "SELECT c FROM CalcControlDigit c WHERE c.variable = :variable"),
    @NamedQuery(name = "CalcControlDigit.findByStatusRow", query = "SELECT c FROM CalcControlDigit c WHERE c.statusRow = :statusRow")})
public class CalcControlDigit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "name_variable", length = 200)
    private String nameVariable;
    @Column(name = "default_value_variable", length = 20)
    private String defaultValueVariable;
    @Column(name = "descript_action_step", length = 450)
    private String descriptActionStep;
    @Column(name = "variable", length = 50)
    private String variable;
    @Column(name = "status_row")
    private Short statusRow;

    public CalcControlDigit() {
    }

    public CalcControlDigit(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNameVariable() {
        return nameVariable;
    }

    public void setNameVariable(String nameVariable) {
        this.nameVariable = nameVariable;
    }

    public String getDefaultValueVariable() {
        return defaultValueVariable;
    }

    public void setDefaultValueVariable(String defaultValueVariable) {
        this.defaultValueVariable = defaultValueVariable;
    }

    public String getDescriptActionStep() {
        return descriptActionStep;
    }

    public void setDescriptActionStep(String descriptActionStep) {
        this.descriptActionStep = descriptActionStep;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalcControlDigit)) {
            return false;
        }
        CalcControlDigit other = (CalcControlDigit) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.CalcControlDigit[ idRecord=" + idRecord + " ]";
    }
    
}
