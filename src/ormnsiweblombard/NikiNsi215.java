/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_nsi_215", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiNsi215.findAll", query = "SELECT n FROM NikiNsi215 n"),
    @NamedQuery(name = "NikiNsi215.findByKodTypeorganiz", query = "SELECT n FROM NikiNsi215 n WHERE n.kodTypeorganiz = :kodTypeorganiz"),
    @NamedQuery(name = "NikiNsi215.findByNameTypeorganiz", query = "SELECT n FROM NikiNsi215 n WHERE n.nameTypeorganiz = :nameTypeorganiz"),
    @NamedQuery(name = "NikiNsi215.findByKodCreditreport", query = "SELECT n FROM NikiNsi215 n WHERE n.kodCreditreport = :kodCreditreport"),
    @NamedQuery(name = "NikiNsi215.findByNameCreditreport", query = "SELECT n FROM NikiNsi215 n WHERE n.nameCreditreport = :nameCreditreport"),
    @NamedQuery(name = "NikiNsi215.findByActivReport", query = "SELECT n FROM NikiNsi215 n WHERE n.activReport = :activReport"),
    @NamedQuery(name = "NikiNsi215.findByIdRecord", query = "SELECT n FROM NikiNsi215 n WHERE n.idRecord = :idRecord")})
public class NikiNsi215 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "kod_typeorganiz", length = 3)
    private String kodTypeorganiz;
    @Column(name = "name_typeorganiz", length = 50)
    private String nameTypeorganiz;
    @Column(name = "kod_creditreport", length = 6)
    private String kodCreditreport;
    @Column(name = "name_creditreport", length = 100)
    private String nameCreditreport;
    @Column(name = "activ_report", length = 3)
    private String activReport;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiNsi215() {
    }

    public NikiNsi215(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getKodTypeorganiz() {
        return kodTypeorganiz;
    }

    public void setKodTypeorganiz(String kodTypeorganiz) {
        this.kodTypeorganiz = kodTypeorganiz;
    }

    public String getNameTypeorganiz() {
        return nameTypeorganiz;
    }

    public void setNameTypeorganiz(String nameTypeorganiz) {
        this.nameTypeorganiz = nameTypeorganiz;
    }

    public String getKodCreditreport() {
        return kodCreditreport;
    }

    public void setKodCreditreport(String kodCreditreport) {
        this.kodCreditreport = kodCreditreport;
    }

    public String getNameCreditreport() {
        return nameCreditreport;
    }

    public void setNameCreditreport(String nameCreditreport) {
        this.nameCreditreport = nameCreditreport;
    }

    public String getActivReport() {
        return activReport;
    }

    public void setActivReport(String activReport) {
        this.activReport = activReport;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiNsi215)) {
            return false;
        }
        NikiNsi215 other = (NikiNsi215) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiNsi215[ idRecord=" + idRecord + " ]";
    }
    
}
