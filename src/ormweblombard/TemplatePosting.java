/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "template_posting", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TemplatePosting.findAll", query = "SELECT t FROM TemplatePosting t"),
    @NamedQuery(name = "TemplatePosting.findByIdRecord", query = "SELECT t FROM TemplatePosting t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TemplatePosting.findByIdTemplatePosting", query = "SELECT t FROM TemplatePosting t WHERE t.idTemplatePosting = :idTemplatePosting"),
    @NamedQuery(name = "TemplatePosting.findByIdFormaPay", query = "SELECT t FROM TemplatePosting t WHERE t.idFormaPay = :idFormaPay"),
    @NamedQuery(name = "TemplatePosting.findByVariableDebitAccount", query = "SELECT t FROM TemplatePosting t WHERE t.variableDebitAccount = :variableDebitAccount"),
    @NamedQuery(name = "TemplatePosting.findByVariableCreditAccount", query = "SELECT t FROM TemplatePosting t WHERE t.variableCreditAccount = :variableCreditAccount"),
    @NamedQuery(name = "TemplatePosting.findByDatetimeCreateTemplate", query = "SELECT t FROM TemplatePosting t WHERE t.datetimeCreateTemplate = :datetimeCreateTemplate"),
    @NamedQuery(name = "TemplatePosting.findByUserCreateTemplate", query = "SELECT t FROM TemplatePosting t WHERE t.userCreateTemplate = :userCreateTemplate"),
    @NamedQuery(name = "TemplatePosting.findByDatetimeLastEditTemplate", query = "SELECT t FROM TemplatePosting t WHERE t.datetimeLastEditTemplate = :datetimeLastEditTemplate"),
    @NamedQuery(name = "TemplatePosting.findByUserLastEditTemplate", query = "SELECT t FROM TemplatePosting t WHERE t.userLastEditTemplate = :userLastEditTemplate"),
    @NamedQuery(name = "TemplatePosting.findByStatusRow", query = "SELECT t FROM TemplatePosting t WHERE t.statusRow = :statusRow")})
public class TemplatePosting implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_template_posting")
    private Integer idTemplatePosting;
    @Column(name = "id_forma_pay")
    private Integer idFormaPay;
    @Column(name = "variable_debit_account", length = 30)
    private String variableDebitAccount;
    @Column(name = "variable_credit_account", length = 30)
    private String variableCreditAccount;
    @Column(name = "datetime_create_template", length = 20)
    private String datetimeCreateTemplate;
    @Column(name = "user_create_template", length = 30)
    private String userCreateTemplate;
    @Column(name = "datetime_last_edit_template", length = 20)
    private String datetimeLastEditTemplate;
    @Column(name = "user_last_edit_template", length = 30)
    private String userLastEditTemplate;
    @Column(name = "status_row")
    private Short statusRow;

    public TemplatePosting() {
    }

    public TemplatePosting(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdTemplatePosting() {
        return idTemplatePosting;
    }

    public void setIdTemplatePosting(Integer idTemplatePosting) {
        this.idTemplatePosting = idTemplatePosting;
    }

    public Integer getIdFormaPay() {
        return idFormaPay;
    }

    public void setIdFormaPay(Integer idFormaPay) {
        this.idFormaPay = idFormaPay;
    }

    public String getVariableDebitAccount() {
        return variableDebitAccount;
    }

    public void setVariableDebitAccount(String variableDebitAccount) {
        this.variableDebitAccount = variableDebitAccount;
    }

    public String getVariableCreditAccount() {
        return variableCreditAccount;
    }

    public void setVariableCreditAccount(String variableCreditAccount) {
        this.variableCreditAccount = variableCreditAccount;
    }

    public String getDatetimeCreateTemplate() {
        return datetimeCreateTemplate;
    }

    public void setDatetimeCreateTemplate(String datetimeCreateTemplate) {
        this.datetimeCreateTemplate = datetimeCreateTemplate;
    }

    public String getUserCreateTemplate() {
        return userCreateTemplate;
    }

    public void setUserCreateTemplate(String userCreateTemplate) {
        this.userCreateTemplate = userCreateTemplate;
    }

    public String getDatetimeLastEditTemplate() {
        return datetimeLastEditTemplate;
    }

    public void setDatetimeLastEditTemplate(String datetimeLastEditTemplate) {
        this.datetimeLastEditTemplate = datetimeLastEditTemplate;
    }

    public String getUserLastEditTemplate() {
        return userLastEditTemplate;
    }

    public void setUserLastEditTemplate(String userLastEditTemplate) {
        this.userLastEditTemplate = userLastEditTemplate;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TemplatePosting)) {
            return false;
        }
        TemplatePosting other = (TemplatePosting) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TemplatePosting[ idRecord=" + idRecord + " ]";
    }
    
}
