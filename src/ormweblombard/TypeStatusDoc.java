/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_status_doc", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeStatusDoc.findAll", query = "SELECT t FROM TypeStatusDoc t"),
    @NamedQuery(name = "TypeStatusDoc.findByIdRecord", query = "SELECT t FROM TypeStatusDoc t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeStatusDoc.findByIdStatusDoc", query = "SELECT t FROM TypeStatusDoc t WHERE t.idStatusDoc = :idStatusDoc"),
    @NamedQuery(name = "TypeStatusDoc.findByNameStatusDoc", query = "SELECT t FROM TypeStatusDoc t WHERE t.nameStatusDoc = :nameStatusDoc"),
    @NamedQuery(name = "TypeStatusDoc.findByDescriptStatusDoc", query = "SELECT t FROM TypeStatusDoc t WHERE t.descriptStatusDoc = :descriptStatusDoc"),
    @NamedQuery(name = "TypeStatusDoc.findByStatusRow", query = "SELECT t FROM TypeStatusDoc t WHERE t.statusRow = :statusRow")})
public class TypeStatusDoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_status_doc")
    private Integer idStatusDoc;
    @Column(name = "name_status_doc", length = 30)
    private String nameStatusDoc;
    @Column(name = "descript_status_doc", length = 2147483647)
    private String descriptStatusDoc;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeStatusDoc() {
    }

    public TypeStatusDoc(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdStatusDoc() {
        return idStatusDoc;
    }

    public void setIdStatusDoc(Integer idStatusDoc) {
        this.idStatusDoc = idStatusDoc;
    }

    public String getNameStatusDoc() {
        return nameStatusDoc;
    }

    public void setNameStatusDoc(String nameStatusDoc) {
        this.nameStatusDoc = nameStatusDoc;
    }

    public String getDescriptStatusDoc() {
        return descriptStatusDoc;
    }

    public void setDescriptStatusDoc(String descriptStatusDoc) {
        this.descriptStatusDoc = descriptStatusDoc;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeStatusDoc)) {
            return false;
        }
        TypeStatusDoc other = (TypeStatusDoc) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeStatusDoc[ idRecord=" + idRecord + " ]";
    }
    
}
