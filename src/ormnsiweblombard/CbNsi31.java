/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_31", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi31.findAll", query = "SELECT c FROM CbNsi31 c"),
    @NamedQuery(name = "CbNsi31.findByNciId", query = "SELECT c FROM CbNsi31 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi31.findByKredId", query = "SELECT c FROM CbNsi31 c WHERE c.kredId = :kredId"),
    @NamedQuery(name = "CbNsi31.findByKredName", query = "SELECT c FROM CbNsi31 c WHERE c.kredName = :kredName"),
    @NamedQuery(name = "CbNsi31.findByKredGr", query = "SELECT c FROM CbNsi31 c WHERE c.kredGr = :kredGr"),
    @NamedQuery(name = "CbNsi31.findByDateOpen", query = "SELECT c FROM CbNsi31 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi31.findByDateClose", query = "SELECT c FROM CbNsi31 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi31.findByActive", query = "SELECT c FROM CbNsi31 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi31.findByIdRecord", query = "SELECT c FROM CbNsi31 c WHERE c.idRecord = :idRecord")})
public class CbNsi31 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Basic(optional = false)
    @Column(name = "kred_id", nullable = false, length = 10)
    private String kredId;
    @Column(name = "kred_name", length = 60)
    private String kredName;
    @Column(name = "kred_gr", length = 10)
    private String kredGr;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Column(name = "date_close", length = 10)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi31() {
    }

    public CbNsi31(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public CbNsi31(Integer idRecord, String kredId) {
        this.idRecord = idRecord;
        this.kredId = kredId;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getKredId() {
        return kredId;
    }

    public void setKredId(String kredId) {
        this.kredId = kredId;
    }

    public String getKredName() {
        return kredName;
    }

    public void setKredName(String kredName) {
        this.kredName = kredName;
    }

    public String getKredGr() {
        return kredGr;
    }

    public void setKredGr(String kredGr) {
        this.kredGr = kredGr;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi31)) {
            return false;
        }
        CbNsi31 other = (CbNsi31) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi31[ idRecord=" + idRecord + " ]";
    }
    
}
