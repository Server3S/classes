/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_71", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi71.findAll", query = "SELECT c FROM CbNsi71 c"),
    @NamedQuery(name = "CbNsi71.findByNciId", query = "SELECT c FROM CbNsi71 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi71.findBySooguSt", query = "SELECT c FROM CbNsi71 c WHERE c.sooguSt = :sooguSt"),
    @NamedQuery(name = "CbNsi71.findBySooguCb", query = "SELECT c FROM CbNsi71 c WHERE c.sooguCb = :sooguCb"),
    @NamedQuery(name = "CbNsi71.findBySoogu1", query = "SELECT c FROM CbNsi71 c WHERE c.soogu1 = :soogu1"),
    @NamedQuery(name = "CbNsi71.findBySoogu2", query = "SELECT c FROM CbNsi71 c WHERE c.soogu2 = :soogu2"),
    @NamedQuery(name = "CbNsi71.findByBranchId", query = "SELECT c FROM CbNsi71 c WHERE c.branchId = :branchId"),
    @NamedQuery(name = "CbNsi71.findByKodR", query = "SELECT c FROM CbNsi71 c WHERE c.kodR = :kodR"),
    @NamedQuery(name = "CbNsi71.findByKodPr", query = "SELECT c FROM CbNsi71 c WHERE c.kodPr = :kodPr"),
    @NamedQuery(name = "CbNsi71.findByKodK", query = "SELECT c FROM CbNsi71 c WHERE c.kodK = :kodK"),
    @NamedQuery(name = "CbNsi71.findByDateOpen", query = "SELECT c FROM CbNsi71 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi71.findByDateClose", query = "SELECT c FROM CbNsi71 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi71.findByActive", query = "SELECT c FROM CbNsi71 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi71.findByIdRecord", query = "SELECT c FROM CbNsi71 c WHERE c.idRecord = :idRecord")})
public class CbNsi71 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Column(name = "soogu_st", length = 10)
    private String sooguSt;
    @Column(name = "soogu_cb", length = 10)
    private String sooguCb;
    @Column(name = "soogu1", length = 200)
    private String soogu1;
    @Column(name = "soogu2", length = 100)
    private String soogu2;
    @Column(name = "branch_id", length = 10)
    private String branchId;
    @Column(name = "kod_r", length = 10)
    private String kodR;
    @Column(name = "kod_pr", length = 10)
    private String kodPr;
    @Column(name = "kod_k", length = 10)
    private String kodK;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Column(name = "date_close", length = 10)
    private String dateClose;
    @Column(name = "active", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi71() {
    }

    public CbNsi71(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getSooguSt() {
        return sooguSt;
    }

    public void setSooguSt(String sooguSt) {
        this.sooguSt = sooguSt;
    }

    public String getSooguCb() {
        return sooguCb;
    }

    public void setSooguCb(String sooguCb) {
        this.sooguCb = sooguCb;
    }

    public String getSoogu1() {
        return soogu1;
    }

    public void setSoogu1(String soogu1) {
        this.soogu1 = soogu1;
    }

    public String getSoogu2() {
        return soogu2;
    }

    public void setSoogu2(String soogu2) {
        this.soogu2 = soogu2;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getKodR() {
        return kodR;
    }

    public void setKodR(String kodR) {
        this.kodR = kodR;
    }

    public String getKodPr() {
        return kodPr;
    }

    public void setKodPr(String kodPr) {
        this.kodPr = kodPr;
    }

    public String getKodK() {
        return kodK;
    }

    public void setKodK(String kodK) {
        this.kodK = kodK;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi71)) {
            return false;
        }
        CbNsi71 other = (CbNsi71) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi71[ idRecord=" + idRecord + " ]";
    }
    
}
