/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_nsi_233", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiNsi233.findAll", query = "SELECT n FROM NikiNsi233 n"),
    @NamedQuery(name = "NikiNsi233.findByKodCategTypeobiectcredit", query = "SELECT n FROM NikiNsi233 n WHERE n.kodCategTypeobiectcredit = :kodCategTypeobiectcredit"),
    @NamedQuery(name = "NikiNsi233.findByNameCategory", query = "SELECT n FROM NikiNsi233 n WHERE n.nameCategory = :nameCategory"),
    @NamedQuery(name = "NikiNsi233.findByKodTypeclient", query = "SELECT n FROM NikiNsi233 n WHERE n.kodTypeclient = :kodTypeclient"),
    @NamedQuery(name = "NikiNsi233.findByNameTypeclient", query = "SELECT n FROM NikiNsi233 n WHERE n.nameTypeclient = :nameTypeclient"),
    @NamedQuery(name = "NikiNsi233.findByIdRecord", query = "SELECT n FROM NikiNsi233 n WHERE n.idRecord = :idRecord")})
public class NikiNsi233 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "kod_categ_typeobiectcredit", length = 3)
    private String kodCategTypeobiectcredit;
    @Column(name = "name_category", length = 100)
    private String nameCategory;
    @Column(name = "kod_typeclient", length = 3)
    private String kodTypeclient;
    @Column(name = "name_typeclient", length = 100)
    private String nameTypeclient;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiNsi233() {
    }

    public NikiNsi233(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getKodCategTypeobiectcredit() {
        return kodCategTypeobiectcredit;
    }

    public void setKodCategTypeobiectcredit(String kodCategTypeobiectcredit) {
        this.kodCategTypeobiectcredit = kodCategTypeobiectcredit;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public String getKodTypeclient() {
        return kodTypeclient;
    }

    public void setKodTypeclient(String kodTypeclient) {
        this.kodTypeclient = kodTypeclient;
    }

    public String getNameTypeclient() {
        return nameTypeclient;
    }

    public void setNameTypeclient(String nameTypeclient) {
        this.nameTypeclient = nameTypeclient;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiNsi233)) {
            return false;
        }
        NikiNsi233 other = (NikiNsi233) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiNsi233[ idRecord=" + idRecord + " ]";
    }
    
}
