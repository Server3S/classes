/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_06", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi06.findAll", query = "SELECT c FROM CbNsi06 c"),
    @NamedQuery(name = "CbNsi06.findByNsiId", query = "SELECT c FROM CbNsi06 c WHERE c.nsiId = :nsiId"),
    @NamedQuery(name = "CbNsi06.findBySubId", query = "SELECT c FROM CbNsi06 c WHERE c.subId = :subId"),
    @NamedQuery(name = "CbNsi06.findByNameSub", query = "SELECT c FROM CbNsi06 c WHERE c.nameSub = :nameSub"),
    @NamedQuery(name = "CbNsi06.findByDateOpen", query = "SELECT c FROM CbNsi06 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi06.findByDateClose", query = "SELECT c FROM CbNsi06 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi06.findByActive", query = "SELECT c FROM CbNsi06 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi06.findByIdRecord", query = "SELECT c FROM CbNsi06 c WHERE c.idRecord = :idRecord")})
public class CbNsi06 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nsi_id", length = 10)
    private String nsiId;
    @Column(name = "sub_id", length = 10)
    private String subId;
    @Column(name = "name_sub", length = 30)
    private String nameSub;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Column(name = "date_close", length = 10)
    private String dateClose;
    @Column(name = "active", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi06() {
    }

    public CbNsi06(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNsiId() {
        return nsiId;
    }

    public void setNsiId(String nsiId) {
        this.nsiId = nsiId;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getNameSub() {
        return nameSub;
    }

    public void setNameSub(String nameSub) {
        this.nameSub = nameSub;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi06)) {
            return false;
        }
        CbNsi06 other = (CbNsi06) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi06[ idRecord=" + idRecord + " ]";
    }
    
}
