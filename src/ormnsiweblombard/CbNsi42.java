/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_42", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi42.findAll", query = "SELECT c FROM CbNsi42 c"),
    @NamedQuery(name = "CbNsi42.findByNciId", query = "SELECT c FROM CbNsi42 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi42.findByKodSt", query = "SELECT c FROM CbNsi42 c WHERE c.kodSt = :kodSt"),
    @NamedQuery(name = "CbNsi42.findByNameSt", query = "SELECT c FROM CbNsi42 c WHERE c.nameSt = :nameSt"),
    @NamedQuery(name = "CbNsi42.findByDateOpen", query = "SELECT c FROM CbNsi42 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi42.findByDateClose", query = "SELECT c FROM CbNsi42 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi42.findByActive", query = "SELECT c FROM CbNsi42 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi42.findByIdRecord", query = "SELECT c FROM CbNsi42 c WHERE c.idRecord = :idRecord")})
public class CbNsi42 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "nci_id", nullable = false)
    private int nciId;
    @Basic(optional = false)
    @Column(name = "kod_st", nullable = false, length = 1)
    private String kodSt;
    @Column(name = "name_st", length = 30)
    private String nameSt;
    @Column(name = "date_open", length = 8)
    private String dateOpen;
    @Column(name = "date_close", length = 8)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi42() {
    }

    public CbNsi42(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public CbNsi42(Integer idRecord, int nciId, String kodSt) {
        this.idRecord = idRecord;
        this.nciId = nciId;
        this.kodSt = kodSt;
    }

    public int getNciId() {
        return nciId;
    }

    public void setNciId(int nciId) {
        this.nciId = nciId;
    }

    public String getKodSt() {
        return kodSt;
    }

    public void setKodSt(String kodSt) {
        this.kodSt = kodSt;
    }

    public String getNameSt() {
        return nameSt;
    }

    public void setNameSt(String nameSt) {
        this.nameSt = nameSt;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi42)) {
            return false;
        }
        CbNsi42 other = (CbNsi42) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi42[ idRecord=" + idRecord + " ]";
    }
    
}
