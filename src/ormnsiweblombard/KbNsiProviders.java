/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "kb_nsi_providers", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KbNsiProviders.findAll", query = "SELECT k FROM KbNsiProviders k"),
    @NamedQuery(name = "KbNsiProviders.findByTypeProvider", query = "SELECT k FROM KbNsiProviders k WHERE k.typeProvider = :typeProvider"),
    @NamedQuery(name = "KbNsiProviders.findByNameProvider", query = "SELECT k FROM KbNsiProviders k WHERE k.nameProvider = :nameProvider"),
    @NamedQuery(name = "KbNsiProviders.findByIdRecord", query = "SELECT k FROM KbNsiProviders k WHERE k.idRecord = :idRecord")})
public class KbNsiProviders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "type_provider", length = 2)
    private String typeProvider;
    @Column(name = "name_provider", length = 50)
    private String nameProvider;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public KbNsiProviders() {
    }

    public KbNsiProviders(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getTypeProvider() {
        return typeProvider;
    }

    public void setTypeProvider(String typeProvider) {
        this.typeProvider = typeProvider;
    }

    public String getNameProvider() {
        return nameProvider;
    }

    public void setNameProvider(String nameProvider) {
        this.nameProvider = nameProvider;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KbNsiProviders)) {
            return false;
        }
        KbNsiProviders other = (KbNsiProviders) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.KbNsiProviders[ idRecord=" + idRecord + " ]";
    }
    
}
