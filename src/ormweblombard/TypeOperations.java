/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_operations", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeOperations.findAll", query = "SELECT t FROM TypeOperations t"),
    @NamedQuery(name = "TypeOperations.findByIdRecord", query = "SELECT t FROM TypeOperations t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeOperations.findByIdTypeOperation", query = "SELECT t FROM TypeOperations t WHERE t.idTypeOperation = :idTypeOperation"),
    @NamedQuery(name = "TypeOperations.findByNameTypeOperation", query = "SELECT t FROM TypeOperations t WHERE t.nameTypeOperation = :nameTypeOperation"),
    @NamedQuery(name = "TypeOperations.findByIdTemplatePosting", query = "SELECT t FROM TypeOperations t WHERE t.idTemplatePosting = :idTemplatePosting"),
    @NamedQuery(name = "TypeOperations.findByStatusRow", query = "SELECT t FROM TypeOperations t WHERE t.statusRow = :statusRow")})
public class TypeOperations implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_type_operation")
    private Integer idTypeOperation;
    @Column(name = "name_type_operation", length = 30)
    private String nameTypeOperation;
    @Column(name = "id_template_posting")
    private Integer idTemplatePosting;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeOperations() {
    }

    public TypeOperations(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdTypeOperation() {
        return idTypeOperation;
    }

    public void setIdTypeOperation(Integer idTypeOperation) {
        this.idTypeOperation = idTypeOperation;
    }

    public String getNameTypeOperation() {
        return nameTypeOperation;
    }

    public void setNameTypeOperation(String nameTypeOperation) {
        this.nameTypeOperation = nameTypeOperation;
    }

    public Integer getIdTemplatePosting() {
        return idTemplatePosting;
    }

    public void setIdTemplatePosting(Integer idTemplatePosting) {
        this.idTemplatePosting = idTemplatePosting;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeOperations)) {
            return false;
        }
        TypeOperations other = (TypeOperations) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeOperations[ idRecord=" + idRecord + " ]";
    }
    
}
