/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_list", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsiList.findAll", query = "SELECT c FROM CbNsiList c"),
    @NamedQuery(name = "CbNsiList.findByNciIdO", query = "SELECT c FROM CbNsiList c WHERE c.nciIdO = :nciIdO"),
    @NamedQuery(name = "CbNsiList.findByNciId", query = "SELECT c FROM CbNsiList c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsiList.findByName", query = "SELECT c FROM CbNsiList c WHERE c.name = :name"),
    @NamedQuery(name = "CbNsiList.findByStype", query = "SELECT c FROM CbNsiList c WHERE c.stype = :stype"),
    @NamedQuery(name = "CbNsiList.findByDateActive", query = "SELECT c FROM CbNsiList c WHERE c.dateActive = :dateActive"),
    @NamedQuery(name = "CbNsiList.findByIdRecord", query = "SELECT c FROM CbNsiList c WHERE c.idRecord = :idRecord")})
public class CbNsiList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id_o", length = 10)
    private String nciIdO;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Column(name = "name", length = 80)
    private String name;
    @Column(name = "stype", length = 10)
    private String stype;
    @Column(name = "date_active", length = 8)
    private String dateActive;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsiList() {
    }

    public CbNsiList(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNciIdO() {
        return nciIdO;
    }

    public void setNciIdO(String nciIdO) {
        this.nciIdO = nciIdO;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStype() {
        return stype;
    }

    public void setStype(String stype) {
        this.stype = stype;
    }

    public String getDateActive() {
        return dateActive;
    }

    public void setDateActive(String dateActive) {
        this.dateActive = dateActive;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsiList)) {
            return false;
        }
        CbNsiList other = (CbNsiList) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsiList[ idRecord=" + idRecord + " ]";
    }
    
}
