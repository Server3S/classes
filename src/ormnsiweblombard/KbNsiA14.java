/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "kb_nsi_a14", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KbNsiA14.findAll", query = "SELECT k FROM KbNsiA14 k"),
    @NamedQuery(name = "KbNsiA14.findByCodeTypeLoancredit", query = "SELECT k FROM KbNsiA14 k WHERE k.codeTypeLoancredit = :codeTypeLoancredit"),
    @NamedQuery(name = "KbNsiA14.findByLoanCredit", query = "SELECT k FROM KbNsiA14 k WHERE k.loanCredit = :loanCredit"),
    @NamedQuery(name = "KbNsiA14.findByCode", query = "SELECT k FROM KbNsiA14 k WHERE k.code = :code"),
    @NamedQuery(name = "KbNsiA14.findByNameAtributLoan", query = "SELECT k FROM KbNsiA14 k WHERE k.nameAtributLoan = :nameAtributLoan"),
    @NamedQuery(name = "KbNsiA14.findByOrderAtribut", query = "SELECT k FROM KbNsiA14 k WHERE k.orderAtribut = :orderAtribut"),
    @NamedQuery(name = "KbNsiA14.findByTypeData", query = "SELECT k FROM KbNsiA14 k WHERE k.typeData = :typeData"),
    @NamedQuery(name = "KbNsiA14.findBySizeData", query = "SELECT k FROM KbNsiA14 k WHERE k.sizeData = :sizeData"),
    @NamedQuery(name = "KbNsiA14.findByNsiCb", query = "SELECT k FROM KbNsiA14 k WHERE k.nsiCb = :nsiCb"),
    @NamedQuery(name = "KbNsiA14.findByNsiAsoki", query = "SELECT k FROM KbNsiA14 k WHERE k.nsiAsoki = :nsiAsoki"),
    @NamedQuery(name = "KbNsiA14.findByMandatory", query = "SELECT k FROM KbNsiA14 k WHERE k.mandatory = :mandatory"),
    @NamedQuery(name = "KbNsiA14.findByEditable", query = "SELECT k FROM KbNsiA14 k WHERE k.editable = :editable"),
    @NamedQuery(name = "KbNsiA14.findByStatus", query = "SELECT k FROM KbNsiA14 k WHERE k.status = :status"),
    @NamedQuery(name = "KbNsiA14.findByIdRecord", query = "SELECT k FROM KbNsiA14 k WHERE k.idRecord = :idRecord")})
public class KbNsiA14 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "code_type_loancredit", length = 3)
    private String codeTypeLoancredit;
    @Column(name = "loan_credit", length = 2147483647)
    private String loanCredit;
    @Column(name = "code", length = 3)
    private String code;
    @Column(name = "name_atribut_loan", length = 200)
    private String nameAtributLoan;
    @Column(name = "order_atribut", length = 2)
    private String orderAtribut;
    @Column(name = "type_data", length = 20)
    private String typeData;
    @Column(name = "size_data", length = 10)
    private String sizeData;
    @Column(name = "nsi_cb", length = 10)
    private String nsiCb;
    @Column(name = "nsi_asoki", length = 10)
    private String nsiAsoki;
    @Column(name = "mandatory", length = 2)
    private String mandatory;
    @Column(name = "editable", length = 2)
    private String editable;
    @Column(name = "status", length = 1)
    private String status;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public KbNsiA14() {
    }

    public KbNsiA14(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getCodeTypeLoancredit() {
        return codeTypeLoancredit;
    }

    public void setCodeTypeLoancredit(String codeTypeLoancredit) {
        this.codeTypeLoancredit = codeTypeLoancredit;
    }

    public String getLoanCredit() {
        return loanCredit;
    }

    public void setLoanCredit(String loanCredit) {
        this.loanCredit = loanCredit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameAtributLoan() {
        return nameAtributLoan;
    }

    public void setNameAtributLoan(String nameAtributLoan) {
        this.nameAtributLoan = nameAtributLoan;
    }

    public String getOrderAtribut() {
        return orderAtribut;
    }

    public void setOrderAtribut(String orderAtribut) {
        this.orderAtribut = orderAtribut;
    }

    public String getTypeData() {
        return typeData;
    }

    public void setTypeData(String typeData) {
        this.typeData = typeData;
    }

    public String getSizeData() {
        return sizeData;
    }

    public void setSizeData(String sizeData) {
        this.sizeData = sizeData;
    }

    public String getNsiCb() {
        return nsiCb;
    }

    public void setNsiCb(String nsiCb) {
        this.nsiCb = nsiCb;
    }

    public String getNsiAsoki() {
        return nsiAsoki;
    }

    public void setNsiAsoki(String nsiAsoki) {
        this.nsiAsoki = nsiAsoki;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getEditable() {
        return editable;
    }

    public void setEditable(String editable) {
        this.editable = editable;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KbNsiA14)) {
            return false;
        }
        KbNsiA14 other = (KbNsiA14) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.KbNsiA14[ idRecord=" + idRecord + " ]";
    }
    
}
