/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_81", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi81.findAll", query = "SELECT c FROM CbNsi81 c"),
    @NamedQuery(name = "CbNsi81.findByNsiId", query = "SELECT c FROM CbNsi81 c WHERE c.nsiId = :nsiId"),
    @NamedQuery(name = "CbNsi81.findByIdRelatives", query = "SELECT c FROM CbNsi81 c WHERE c.idRelatives = :idRelatives"),
    @NamedQuery(name = "CbNsi81.findByCyrillicName", query = "SELECT c FROM CbNsi81 c WHERE c.cyrillicName = :cyrillicName"),
    @NamedQuery(name = "CbNsi81.findByLatinName", query = "SELECT c FROM CbNsi81 c WHERE c.latinName = :latinName"),
    @NamedQuery(name = "CbNsi81.findByRusName", query = "SELECT c FROM CbNsi81 c WHERE c.rusName = :rusName"),
    @NamedQuery(name = "CbNsi81.findByDateOpen", query = "SELECT c FROM CbNsi81 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi81.findByDateClose", query = "SELECT c FROM CbNsi81 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi81.findByActive", query = "SELECT c FROM CbNsi81 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi81.findByIdRecord", query = "SELECT c FROM CbNsi81 c WHERE c.idRecord = :idRecord")})
public class CbNsi81 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nsi_id", length = 3)
    private String nsiId;
    @Column(name = "id_relatives", length = 3)
    private String idRelatives;
    @Column(name = "cyrillic_name", length = 50)
    private String cyrillicName;
    @Column(name = "latin_name", length = 50)
    private String latinName;
    @Column(name = "rus_name", length = 50)
    private String rusName;
    @Column(name = "date_open", length = 8)
    private String dateOpen;
    @Column(name = "date_close", length = 8)
    private String dateClose;
    @Column(name = "active", length = 2)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi81() {
    }

    public CbNsi81(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNsiId() {
        return nsiId;
    }

    public void setNsiId(String nsiId) {
        this.nsiId = nsiId;
    }

    public String getIdRelatives() {
        return idRelatives;
    }

    public void setIdRelatives(String idRelatives) {
        this.idRelatives = idRelatives;
    }

    public String getCyrillicName() {
        return cyrillicName;
    }

    public void setCyrillicName(String cyrillicName) {
        this.cyrillicName = cyrillicName;
    }

    public String getLatinName() {
        return latinName;
    }

    public void setLatinName(String latinName) {
        this.latinName = latinName;
    }

    public String getRusName() {
        return rusName;
    }

    public void setRusName(String rusName) {
        this.rusName = rusName;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi81)) {
            return false;
        }
        CbNsi81 other = (CbNsi81) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi81[ idRecord=" + idRecord + " ]";
    }
    
}
