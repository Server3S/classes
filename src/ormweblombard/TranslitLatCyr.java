/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "translit_lat_cyr", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TranslitLatCyr.findAll", query = "SELECT t FROM TranslitLatCyr t"),
    @NamedQuery(name = "TranslitLatCyr.findByCyrillic", query = "SELECT t FROM TranslitLatCyr t WHERE t.cyrillic = :cyrillic"),
    @NamedQuery(name = "TranslitLatCyr.findByDecCyr", query = "SELECT t FROM TranslitLatCyr t WHERE t.decCyr = :decCyr"),
    @NamedQuery(name = "TranslitLatCyr.findByHexCyr", query = "SELECT t FROM TranslitLatCyr t WHERE t.hexCyr = :hexCyr"),
    @NamedQuery(name = "TranslitLatCyr.findByToLatin", query = "SELECT t FROM TranslitLatCyr t WHERE t.toLatin = :toLatin"),
    @NamedQuery(name = "TranslitLatCyr.findByAsciiHexLatin", query = "SELECT t FROM TranslitLatCyr t WHERE t.asciiHexLatin = :asciiHexLatin"),
    @NamedQuery(name = "TranslitLatCyr.findByForSearch", query = "SELECT t FROM TranslitLatCyr t WHERE t.forSearch = :forSearch"),
    @NamedQuery(name = "TranslitLatCyr.findByAsciiDecLatin", query = "SELECT t FROM TranslitLatCyr t WHERE t.asciiDecLatin = :asciiDecLatin"),
    @NamedQuery(name = "TranslitLatCyr.findByIdRecord", query = "SELECT t FROM TranslitLatCyr t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TranslitLatCyr.findByStatusRow", query = "SELECT t FROM TranslitLatCyr t WHERE t.statusRow = :statusRow")})
public class TranslitLatCyr implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "cyrillic", length = 1)
    private String cyrillic;
    @Column(name = "dec_cyr")
    private Integer decCyr;
    @Column(name = "hex_cyr", length = 5)
    private String hexCyr;
    @Lob
    @Column(name = "position_cyr")
    private Object positionCyr;
    @Lob
    @Column(name = "after_cyr")
    private Object afterCyr;
    @Lob
    @Column(name = "hard_cyr")
    private Object hardCyr;
    @Lob
    @Column(name = "voiced_cyr")
    private Object voicedCyr;
    @Lob
    @Column(name = "glas_cyr")
    private Object glasCyr;
    @Column(name = "to_latin", length = 3)
    private String toLatin;
    @Column(name = "ascii_hex_latin", length = 5)
    private String asciiHexLatin;
    @Column(name = "for_search", length = 15)
    private String forSearch;
    @Column(name = "ascii_dec_latin", length = 10)
    private String asciiDecLatin;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "status_row")
    private Short statusRow;

    public TranslitLatCyr() {
    }

    public TranslitLatCyr(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getCyrillic() {
        return cyrillic;
    }

    public void setCyrillic(String cyrillic) {
        this.cyrillic = cyrillic;
    }

    public Integer getDecCyr() {
        return decCyr;
    }

    public void setDecCyr(Integer decCyr) {
        this.decCyr = decCyr;
    }

    public String getHexCyr() {
        return hexCyr;
    }

    public void setHexCyr(String hexCyr) {
        this.hexCyr = hexCyr;
    }

    public Object getPositionCyr() {
        return positionCyr;
    }

    public void setPositionCyr(Object positionCyr) {
        this.positionCyr = positionCyr;
    }

    public Object getAfterCyr() {
        return afterCyr;
    }

    public void setAfterCyr(Object afterCyr) {
        this.afterCyr = afterCyr;
    }

    public Object getHardCyr() {
        return hardCyr;
    }

    public void setHardCyr(Object hardCyr) {
        this.hardCyr = hardCyr;
    }

    public Object getVoicedCyr() {
        return voicedCyr;
    }

    public void setVoicedCyr(Object voicedCyr) {
        this.voicedCyr = voicedCyr;
    }

    public Object getGlasCyr() {
        return glasCyr;
    }

    public void setGlasCyr(Object glasCyr) {
        this.glasCyr = glasCyr;
    }

    public String getToLatin() {
        return toLatin;
    }

    public void setToLatin(String toLatin) {
        this.toLatin = toLatin;
    }

    public String getAsciiHexLatin() {
        return asciiHexLatin;
    }

    public void setAsciiHexLatin(String asciiHexLatin) {
        this.asciiHexLatin = asciiHexLatin;
    }

    public String getForSearch() {
        return forSearch;
    }

    public void setForSearch(String forSearch) {
        this.forSearch = forSearch;
    }

    public String getAsciiDecLatin() {
        return asciiDecLatin;
    }

    public void setAsciiDecLatin(String asciiDecLatin) {
        this.asciiDecLatin = asciiDecLatin;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TranslitLatCyr)) {
            return false;
        }
        TranslitLatCyr other = (TranslitLatCyr) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TranslitLatCyr[ idRecord=" + idRecord + " ]";
    }
    
}
