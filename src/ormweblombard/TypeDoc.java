/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_doc", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeDoc.findAll", query = "SELECT t FROM TypeDoc t"),
    @NamedQuery(name = "TypeDoc.findByIdRecord", query = "SELECT t FROM TypeDoc t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeDoc.findByIdTypeDoc", query = "SELECT t FROM TypeDoc t WHERE t.idTypeDoc = :idTypeDoc"),
    @NamedQuery(name = "TypeDoc.findByNameTypeDoc", query = "SELECT t FROM TypeDoc t WHERE t.nameTypeDoc = :nameTypeDoc"),
    @NamedQuery(name = "TypeDoc.findByStatusRow", query = "SELECT t FROM TypeDoc t WHERE t.statusRow = :statusRow")})
public class TypeDoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_type_doc")
    private Integer idTypeDoc;
    @Column(name = "name_type_doc", length = 100)
    private String nameTypeDoc;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeDoc() {
    }

    public TypeDoc(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdTypeDoc() {
        return idTypeDoc;
    }

    public void setIdTypeDoc(Integer idTypeDoc) {
        this.idTypeDoc = idTypeDoc;
    }

    public String getNameTypeDoc() {
        return nameTypeDoc;
    }

    public void setNameTypeDoc(String nameTypeDoc) {
        this.nameTypeDoc = nameTypeDoc;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeDoc)) {
            return false;
        }
        TypeDoc other = (TypeDoc) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeDoc[ idRecord=" + idRecord + " ]";
    }
    
}
