/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "kb_logic_control", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KbLogicControl.findAll", query = "SELECT k FROM KbLogicControl k"),
    @NamedQuery(name = "KbLogicControl.findByIdRecord", query = "SELECT k FROM KbLogicControl k WHERE k.idRecord = :idRecord"),
    @NamedQuery(name = "KbLogicControl.findByGroupReport", query = "SELECT k FROM KbLogicControl k WHERE k.groupReport = :groupReport"),
    @NamedQuery(name = "KbLogicControl.findByNumberReport", query = "SELECT k FROM KbLogicControl k WHERE k.numberReport = :numberReport"),
    @NamedQuery(name = "KbLogicControl.findByIdRule", query = "SELECT k FROM KbLogicControl k WHERE k.idRule = :idRule"),
    @NamedQuery(name = "KbLogicControl.findByAtribControl", query = "SELECT k FROM KbLogicControl k WHERE k.atribControl = :atribControl"),
    @NamedQuery(name = "KbLogicControl.findByControlAction", query = "SELECT k FROM KbLogicControl k WHERE k.controlAction = :controlAction")})
public class KbLogicControl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "group_report", length = 3)
    private String groupReport;
    @Column(name = "number_report", length = 5)
    private String numberReport;
    @Column(name = "id_rule")
    private Integer idRule;
    @Column(name = "atrib_control", length = 200)
    private String atribControl;
    @Column(name = "control_action", length = 1000)
    private String controlAction;

    public KbLogicControl() {
    }

    public KbLogicControl(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getGroupReport() {
        return groupReport;
    }

    public void setGroupReport(String groupReport) {
        this.groupReport = groupReport;
    }

    public String getNumberReport() {
        return numberReport;
    }

    public void setNumberReport(String numberReport) {
        this.numberReport = numberReport;
    }

    public Integer getIdRule() {
        return idRule;
    }

    public void setIdRule(Integer idRule) {
        this.idRule = idRule;
    }

    public String getAtribControl() {
        return atribControl;
    }

    public void setAtribControl(String atribControl) {
        this.atribControl = atribControl;
    }

    public String getControlAction() {
        return controlAction;
    }

    public void setControlAction(String controlAction) {
        this.controlAction = controlAction;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KbLogicControl)) {
            return false;
        }
        KbLogicControl other = (KbLogicControl) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.KbLogicControl[ idRecord=" + idRecord + " ]";
    }
    
}
