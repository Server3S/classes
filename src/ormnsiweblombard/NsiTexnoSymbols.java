/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "nsi_texno_symbols", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NsiTexnoSymbols.findAll", query = "SELECT n FROM NsiTexnoSymbols n"),
    @NamedQuery(name = "NsiTexnoSymbols.findBySymbol", query = "SELECT n FROM NsiTexnoSymbols n WHERE n.symbol = :symbol"),
    @NamedQuery(name = "NsiTexnoSymbols.findByAsciiHex", query = "SELECT n FROM NsiTexnoSymbols n WHERE n.asciiHex = :asciiHex"),
    @NamedQuery(name = "NsiTexnoSymbols.findByAsciiDec", query = "SELECT n FROM NsiTexnoSymbols n WHERE n.asciiDec = :asciiDec"),
    @NamedQuery(name = "NsiTexnoSymbols.findByDescriptSymbol", query = "SELECT n FROM NsiTexnoSymbols n WHERE n.descriptSymbol = :descriptSymbol"),
    @NamedQuery(name = "NsiTexnoSymbols.findByIdRecord", query = "SELECT n FROM NsiTexnoSymbols n WHERE n.idRecord = :idRecord")})
public class NsiTexnoSymbols implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "symbol", length = 1)
    private String symbol;
    @Column(name = "ascii_hex", length = 2)
    private String asciiHex;
    @Column(name = "ascii_dec")
    private Integer asciiDec;
    @Column(name = "descript_symbol", length = 20)
    private String descriptSymbol;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NsiTexnoSymbols() {
    }

    public NsiTexnoSymbols(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getAsciiHex() {
        return asciiHex;
    }

    public void setAsciiHex(String asciiHex) {
        this.asciiHex = asciiHex;
    }

    public Integer getAsciiDec() {
        return asciiDec;
    }

    public void setAsciiDec(Integer asciiDec) {
        this.asciiDec = asciiDec;
    }

    public String getDescriptSymbol() {
        return descriptSymbol;
    }

    public void setDescriptSymbol(String descriptSymbol) {
        this.descriptSymbol = descriptSymbol;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NsiTexnoSymbols)) {
            return false;
        }
        NsiTexnoSymbols other = (NsiTexnoSymbols) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NsiTexnoSymbols[ idRecord=" + idRecord + " ]";
    }
    
}
