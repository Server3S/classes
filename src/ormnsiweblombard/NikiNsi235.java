/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_nsi_235", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiNsi235.findAll", query = "SELECT n FROM NikiNsi235 n"),
    @NamedQuery(name = "NikiNsi235.findByKod", query = "SELECT n FROM NikiNsi235 n WHERE n.kod = :kod"),
    @NamedQuery(name = "NikiNsi235.findByNameState", query = "SELECT n FROM NikiNsi235 n WHERE n.nameState = :nameState"),
    @NamedQuery(name = "NikiNsi235.findByIdRecord", query = "SELECT n FROM NikiNsi235 n WHERE n.idRecord = :idRecord")})
public class NikiNsi235 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "kod", length = 3)
    private String kod;
    @Column(name = "name_state", length = 25)
    private String nameState;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiNsi235() {
    }

    public NikiNsi235(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getNameState() {
        return nameState;
    }

    public void setNameState(String nameState) {
        this.nameState = nameState;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiNsi235)) {
            return false;
        }
        NikiNsi235 other = (NikiNsi235) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiNsi235[ idRecord=" + idRecord + " ]";
    }
    
}
