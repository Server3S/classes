/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "group_role", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GroupRole.findAll", query = "SELECT g FROM GroupRole g"),
    @NamedQuery(name = "GroupRole.findByIdRecord", query = "SELECT g FROM GroupRole g WHERE g.idRecord = :idRecord"),
    @NamedQuery(name = "GroupRole.findByIdGroupRole", query = "SELECT g FROM GroupRole g WHERE g.idGroupRole = :idGroupRole"),
    @NamedQuery(name = "GroupRole.findByNameGroupRole", query = "SELECT g FROM GroupRole g WHERE g.nameGroupRole = :nameGroupRole"),
    @NamedQuery(name = "GroupRole.findByDescriptGroupRole", query = "SELECT g FROM GroupRole g WHERE g.descriptGroupRole = :descriptGroupRole"),
    @NamedQuery(name = "GroupRole.findByUserCreateGroupRole", query = "SELECT g FROM GroupRole g WHERE g.userCreateGroupRole = :userCreateGroupRole"),
    @NamedQuery(name = "GroupRole.findByDatetimeCreateGroupRole", query = "SELECT g FROM GroupRole g WHERE g.datetimeCreateGroupRole = :datetimeCreateGroupRole"),
    @NamedQuery(name = "GroupRole.findByMaxNumberGroupeRole", query = "SELECT g FROM GroupRole g WHERE g.maxNumberGroupeRole = :maxNumberGroupeRole"),
    @NamedQuery(name = "GroupRole.findByTypeGroupRole", query = "SELECT g FROM GroupRole g WHERE g.typeGroupRole = :typeGroupRole"),
    @NamedQuery(name = "GroupRole.findByStatusRow", query = "SELECT g FROM GroupRole g WHERE g.statusRow = :statusRow")})
public class GroupRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_group_role")
    private Integer idGroupRole;
    @Column(name = "name_group_role", length = 30)
    private String nameGroupRole;
    @Column(name = "descript_group_role", length = 100)
    private String descriptGroupRole;
    @Column(name = "user_create_group_role", length = 30)
    private String userCreateGroupRole;
    @Column(name = "datetime_create_group_role", length = 20)
    private String datetimeCreateGroupRole;
    @Column(name = "max_number_groupe_role")
    private Integer maxNumberGroupeRole;
    @Column(name = "type_group_role", length = 2)
    private String typeGroupRole;
    @Column(name = "status_row")
    private Short statusRow;

    public GroupRole() {
    }

    public GroupRole(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdGroupRole() {
        return idGroupRole;
    }

    public void setIdGroupRole(Integer idGroupRole) {
        this.idGroupRole = idGroupRole;
    }

    public String getNameGroupRole() {
        return nameGroupRole;
    }

    public void setNameGroupRole(String nameGroupRole) {
        this.nameGroupRole = nameGroupRole;
    }

    public String getDescriptGroupRole() {
        return descriptGroupRole;
    }

    public void setDescriptGroupRole(String descriptGroupRole) {
        this.descriptGroupRole = descriptGroupRole;
    }

    public String getUserCreateGroupRole() {
        return userCreateGroupRole;
    }

    public void setUserCreateGroupRole(String userCreateGroupRole) {
        this.userCreateGroupRole = userCreateGroupRole;
    }

    public String getDatetimeCreateGroupRole() {
        return datetimeCreateGroupRole;
    }

    public void setDatetimeCreateGroupRole(String datetimeCreateGroupRole) {
        this.datetimeCreateGroupRole = datetimeCreateGroupRole;
    }

    public Integer getMaxNumberGroupeRole() {
        return maxNumberGroupeRole;
    }

    public void setMaxNumberGroupeRole(Integer maxNumberGroupeRole) {
        this.maxNumberGroupeRole = maxNumberGroupeRole;
    }

    public String getTypeGroupRole() {
        return typeGroupRole;
    }

    public void setTypeGroupRole(String typeGroupRole) {
        this.typeGroupRole = typeGroupRole;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupRole)) {
            return false;
        }
        GroupRole other = (GroupRole) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.GroupRole[ idRecord=" + idRecord + " ]";
    }
    
}
