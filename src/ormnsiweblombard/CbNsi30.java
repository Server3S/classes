/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_30", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi30.findAll", query = "SELECT c FROM CbNsi30 c"),
    @NamedQuery(name = "CbNsi30.findByNciId", query = "SELECT c FROM CbNsi30 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi30.findByKodPeriodkred", query = "SELECT c FROM CbNsi30 c WHERE c.kodPeriodkred = :kodPeriodkred"),
    @NamedQuery(name = "CbNsi30.findByNamePeriodkred", query = "SELECT c FROM CbNsi30 c WHERE c.namePeriodkred = :namePeriodkred"),
    @NamedQuery(name = "CbNsi30.findByDataOpen", query = "SELECT c FROM CbNsi30 c WHERE c.dataOpen = :dataOpen"),
    @NamedQuery(name = "CbNsi30.findByDataClose", query = "SELECT c FROM CbNsi30 c WHERE c.dataClose = :dataClose"),
    @NamedQuery(name = "CbNsi30.findByActive", query = "SELECT c FROM CbNsi30 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi30.findByIdRecord", query = "SELECT c FROM CbNsi30 c WHERE c.idRecord = :idRecord")})
public class CbNsi30 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Column(name = "kod_periodkred", length = 10)
    private String kodPeriodkred;
    @Column(name = "name_periodkred", length = 50)
    private String namePeriodkred;
    @Column(name = "data_open", length = 8)
    private String dataOpen;
    @Column(name = "data_close", length = 8)
    private String dataClose;
    @Column(name = "active", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi30() {
    }

    public CbNsi30(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getKodPeriodkred() {
        return kodPeriodkred;
    }

    public void setKodPeriodkred(String kodPeriodkred) {
        this.kodPeriodkred = kodPeriodkred;
    }

    public String getNamePeriodkred() {
        return namePeriodkred;
    }

    public void setNamePeriodkred(String namePeriodkred) {
        this.namePeriodkred = namePeriodkred;
    }

    public String getDataOpen() {
        return dataOpen;
    }

    public void setDataOpen(String dataOpen) {
        this.dataOpen = dataOpen;
    }

    public String getDataClose() {
        return dataClose;
    }

    public void setDataClose(String dataClose) {
        this.dataClose = dataClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi30)) {
            return false;
        }
        CbNsi30 other = (CbNsi30) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi30[ idRecord=" + idRecord + " ]";
    }
    
}
