/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_dimension", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeDimension.findAll", query = "SELECT t FROM TypeDimension t"),
    @NamedQuery(name = "TypeDimension.findByIdRecord", query = "SELECT t FROM TypeDimension t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeDimension.findByIdTypeDimension", query = "SELECT t FROM TypeDimension t WHERE t.idTypeDimension = :idTypeDimension"),
    @NamedQuery(name = "TypeDimension.findByNameTypeDimension", query = "SELECT t FROM TypeDimension t WHERE t.nameTypeDimension = :nameTypeDimension"),
    @NamedQuery(name = "TypeDimension.findByStatusRow", query = "SELECT t FROM TypeDimension t WHERE t.statusRow = :statusRow")})
public class TypeDimension implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_type_dimension")
    private Integer idTypeDimension;
    @Column(name = "name_type_dimension", length = 30)
    private String nameTypeDimension;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeDimension() {
    }

    public TypeDimension(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdTypeDimension() {
        return idTypeDimension;
    }

    public void setIdTypeDimension(Integer idTypeDimension) {
        this.idTypeDimension = idTypeDimension;
    }

    public String getNameTypeDimension() {
        return nameTypeDimension;
    }

    public void setNameTypeDimension(String nameTypeDimension) {
        this.nameTypeDimension = nameTypeDimension;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeDimension)) {
            return false;
        }
        TypeDimension other = (TypeDimension) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeDimension[ idRecord=" + idRecord + " ]";
    }
    
}
