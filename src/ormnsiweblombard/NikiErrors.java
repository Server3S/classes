/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_errors", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiErrors.findAll", query = "SELECT n FROM NikiErrors n"),
    @NamedQuery(name = "NikiErrors.findByCodeError", query = "SELECT n FROM NikiErrors n WHERE n.codeError = :codeError"),
    @NamedQuery(name = "NikiErrors.findByDescriptionError", query = "SELECT n FROM NikiErrors n WHERE n.descriptionError = :descriptionError"),
    @NamedQuery(name = "NikiErrors.findByIdRecord", query = "SELECT n FROM NikiErrors n WHERE n.idRecord = :idRecord")})
public class NikiErrors implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "code_error")
    private Integer codeError;
    @Column(name = "description_error", length = 250)
    private String descriptionError;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiErrors() {
    }

    public NikiErrors(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getCodeError() {
        return codeError;
    }

    public void setCodeError(Integer codeError) {
        this.codeError = codeError;
    }

    public String getDescriptionError() {
        return descriptionError;
    }

    public void setDescriptionError(String descriptionError) {
        this.descriptionError = descriptionError;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiErrors)) {
            return false;
        }
        NikiErrors other = (NikiErrors) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiErrors[ idRecord=" + idRecord + " ]";
    }
    
}
