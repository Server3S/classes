/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_nsi_231", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiNsi231.findAll", query = "SELECT n FROM NikiNsi231 n"),
    @NamedQuery(name = "NikiNsi231.findByKodTypesubiect", query = "SELECT n FROM NikiNsi231 n WHERE n.kodTypesubiect = :kodTypesubiect"),
    @NamedQuery(name = "NikiNsi231.findByNameTypesubiect", query = "SELECT n FROM NikiNsi231 n WHERE n.nameTypesubiect = :nameTypesubiect"),
    @NamedQuery(name = "NikiNsi231.findByKodTypecredit", query = "SELECT n FROM NikiNsi231 n WHERE n.kodTypecredit = :kodTypecredit"),
    @NamedQuery(name = "NikiNsi231.findByNameTypecredit", query = "SELECT n FROM NikiNsi231 n WHERE n.nameTypecredit = :nameTypecredit"),
    @NamedQuery(name = "NikiNsi231.findByIdRecord", query = "SELECT n FROM NikiNsi231 n WHERE n.idRecord = :idRecord")})
public class NikiNsi231 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "kod_typesubiect", length = 3)
    private String kodTypesubiect;
    @Column(name = "name_typesubiect", length = 50)
    private String nameTypesubiect;
    @Column(name = "kod_typecredit", length = 3)
    private String kodTypecredit;
    @Column(name = "name_typecredit", length = 100)
    private String nameTypecredit;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiNsi231() {
    }

    public NikiNsi231(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getKodTypesubiect() {
        return kodTypesubiect;
    }

    public void setKodTypesubiect(String kodTypesubiect) {
        this.kodTypesubiect = kodTypesubiect;
    }

    public String getNameTypesubiect() {
        return nameTypesubiect;
    }

    public void setNameTypesubiect(String nameTypesubiect) {
        this.nameTypesubiect = nameTypesubiect;
    }

    public String getKodTypecredit() {
        return kodTypecredit;
    }

    public void setKodTypecredit(String kodTypecredit) {
        this.kodTypecredit = kodTypecredit;
    }

    public String getNameTypecredit() {
        return nameTypecredit;
    }

    public void setNameTypecredit(String nameTypecredit) {
        this.nameTypecredit = nameTypecredit;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiNsi231)) {
            return false;
        }
        NikiNsi231 other = (NikiNsi231) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiNsi231[ idRecord=" + idRecord + " ]";
    }
    
}
