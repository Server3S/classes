/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_16", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi16.findAll", query = "SELECT c FROM CbNsi16 c"),
    @NamedQuery(name = "CbNsi16.findByNciId", query = "SELECT c FROM CbNsi16 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi16.findByRegionId", query = "SELECT c FROM CbNsi16 c WHERE c.regionId = :regionId"),
    @NamedQuery(name = "CbNsi16.findByRegionNam", query = "SELECT c FROM CbNsi16 c WHERE c.regionNam = :regionNam"),
    @NamedQuery(name = "CbNsi16.findByDopField", query = "SELECT c FROM CbNsi16 c WHERE c.dopField = :dopField"),
    @NamedQuery(name = "CbNsi16.findByDateOpen", query = "SELECT c FROM CbNsi16 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi16.findByDateClose", query = "SELECT c FROM CbNsi16 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi16.findByActive", query = "SELECT c FROM CbNsi16 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi16.findByIdRecord", query = "SELECT c FROM CbNsi16 c WHERE c.idRecord = :idRecord")})
public class CbNsi16 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Basic(optional = false)
    @Column(name = "region_id", nullable = false, length = 10)
    private String regionId;
    @Column(name = "region_nam", length = 50)
    private String regionNam;
    @Column(name = "dop_field")
    private Integer dopField;
    @Column(name = "date_open", length = 8)
    private String dateOpen;
    @Column(name = "date_close", length = 8)
    private String dateClose;
    @Column(name = "ACTIVE", length = 11)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi16() {
    }

    public CbNsi16(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public CbNsi16(Integer idRecord, String regionId) {
        this.idRecord = idRecord;
        this.regionId = regionId;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionNam() {
        return regionNam;
    }

    public void setRegionNam(String regionNam) {
        this.regionNam = regionNam;
    }

    public Integer getDopField() {
        return dopField;
    }

    public void setDopField(Integer dopField) {
        this.dopField = dopField;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi16)) {
            return false;
        }
        CbNsi16 other = (CbNsi16) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi16[ idRecord=" + idRecord + " ]";
    }
    
}
