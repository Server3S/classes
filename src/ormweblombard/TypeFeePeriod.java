/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_fee_period", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeFeePeriod.findAll", query = "SELECT t FROM TypeFeePeriod t"),
    @NamedQuery(name = "TypeFeePeriod.findByIdRecord", query = "SELECT t FROM TypeFeePeriod t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeFeePeriod.findByIdFeePeriod", query = "SELECT t FROM TypeFeePeriod t WHERE t.idFeePeriod = :idFeePeriod"),
    @NamedQuery(name = "TypeFeePeriod.findByNameFeePeriod", query = "SELECT t FROM TypeFeePeriod t WHERE t.nameFeePeriod = :nameFeePeriod"),
    @NamedQuery(name = "TypeFeePeriod.findByDescriptFeePeriod", query = "SELECT t FROM TypeFeePeriod t WHERE t.descriptFeePeriod = :descriptFeePeriod"),
    @NamedQuery(name = "TypeFeePeriod.findBySizeFeePeriod", query = "SELECT t FROM TypeFeePeriod t WHERE t.sizeFeePeriod = :sizeFeePeriod"),
    @NamedQuery(name = "TypeFeePeriod.findByIdTypedimensFeeperiod", query = "SELECT t FROM TypeFeePeriod t WHERE t.idTypedimensFeeperiod = :idTypedimensFeeperiod"),
    @NamedQuery(name = "TypeFeePeriod.findByStatusRow", query = "SELECT t FROM TypeFeePeriod t WHERE t.statusRow = :statusRow")})
public class TypeFeePeriod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_fee_period")
    private Integer idFeePeriod;
    @Column(name = "name_fee_period", length = 100)
    private String nameFeePeriod;
    @Column(name = "descript_fee_period", length = 300)
    private String descriptFeePeriod;
    @Column(name = "size_fee_period")
    private Integer sizeFeePeriod;
    @Column(name = "id_typedimens_feeperiod")
    private Integer idTypedimensFeeperiod;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeFeePeriod() {
    }

    public TypeFeePeriod(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdFeePeriod() {
        return idFeePeriod;
    }

    public void setIdFeePeriod(Integer idFeePeriod) {
        this.idFeePeriod = idFeePeriod;
    }

    public String getNameFeePeriod() {
        return nameFeePeriod;
    }

    public void setNameFeePeriod(String nameFeePeriod) {
        this.nameFeePeriod = nameFeePeriod;
    }

    public String getDescriptFeePeriod() {
        return descriptFeePeriod;
    }

    public void setDescriptFeePeriod(String descriptFeePeriod) {
        this.descriptFeePeriod = descriptFeePeriod;
    }

    public Integer getSizeFeePeriod() {
        return sizeFeePeriod;
    }

    public void setSizeFeePeriod(Integer sizeFeePeriod) {
        this.sizeFeePeriod = sizeFeePeriod;
    }

    public Integer getIdTypedimensFeeperiod() {
        return idTypedimensFeeperiod;
    }

    public void setIdTypedimensFeeperiod(Integer idTypedimensFeeperiod) {
        this.idTypedimensFeeperiod = idTypedimensFeeperiod;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeFeePeriod)) {
            return false;
        }
        TypeFeePeriod other = (TypeFeePeriod) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeFeePeriod[ idRecord=" + idRecord + " ]";
    }
    
}
