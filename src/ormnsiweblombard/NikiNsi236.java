/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_nsi_236", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiNsi236.findAll", query = "SELECT n FROM NikiNsi236 n"),
    @NamedQuery(name = "NikiNsi236.findByAccountNumber", query = "SELECT n FROM NikiNsi236 n WHERE n.accountNumber = :accountNumber"),
    @NamedQuery(name = "NikiNsi236.findByNameTypecontrol", query = "SELECT n FROM NikiNsi236 n WHERE n.nameTypecontrol = :nameTypecontrol"),
    @NamedQuery(name = "NikiNsi236.findByNameTypesubiect", query = "SELECT n FROM NikiNsi236 n WHERE n.nameTypesubiect = :nameTypesubiect"),
    @NamedQuery(name = "NikiNsi236.findByNameTypecredit", query = "SELECT n FROM NikiNsi236 n WHERE n.nameTypecredit = :nameTypecredit"),
    @NamedQuery(name = "NikiNsi236.findByNamePeriodcredit", query = "SELECT n FROM NikiNsi236 n WHERE n.namePeriodcredit = :namePeriodcredit"),
    @NamedQuery(name = "NikiNsi236.findByNameStatecredit", query = "SELECT n FROM NikiNsi236 n WHERE n.nameStatecredit = :nameStatecredit"),
    @NamedQuery(name = "NikiNsi236.findByDescription", query = "SELECT n FROM NikiNsi236 n WHERE n.description = :description"),
    @NamedQuery(name = "NikiNsi236.findByIdRecord", query = "SELECT n FROM NikiNsi236 n WHERE n.idRecord = :idRecord")})
public class NikiNsi236 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "account_number", length = 6)
    private String accountNumber;
    @Column(name = "name_typecontrol", length = 25)
    private String nameTypecontrol;
    @Column(name = "name_typesubiect", length = 100)
    private String nameTypesubiect;
    @Column(name = "name_typecredit", length = 100)
    private String nameTypecredit;
    @Column(name = "name_periodcredit", length = 25)
    private String namePeriodcredit;
    @Column(name = "name_statecredit", length = 30)
    private String nameStatecredit;
    @Column(name = "description", length = 100)
    private String description;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiNsi236() {
    }

    public NikiNsi236(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getNameTypecontrol() {
        return nameTypecontrol;
    }

    public void setNameTypecontrol(String nameTypecontrol) {
        this.nameTypecontrol = nameTypecontrol;
    }

    public String getNameTypesubiect() {
        return nameTypesubiect;
    }

    public void setNameTypesubiect(String nameTypesubiect) {
        this.nameTypesubiect = nameTypesubiect;
    }

    public String getNameTypecredit() {
        return nameTypecredit;
    }

    public void setNameTypecredit(String nameTypecredit) {
        this.nameTypecredit = nameTypecredit;
    }

    public String getNamePeriodcredit() {
        return namePeriodcredit;
    }

    public void setNamePeriodcredit(String namePeriodcredit) {
        this.namePeriodcredit = namePeriodcredit;
    }

    public String getNameStatecredit() {
        return nameStatecredit;
    }

    public void setNameStatecredit(String nameStatecredit) {
        this.nameStatecredit = nameStatecredit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiNsi236)) {
            return false;
        }
        NikiNsi236 other = (NikiNsi236) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiNsi236[ idRecord=" + idRecord + " ]";
    }
    
}
