/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_credit_schema", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeCreditSchema.findAll", query = "SELECT t FROM TypeCreditSchema t"),
    @NamedQuery(name = "TypeCreditSchema.findByIdRecord", query = "SELECT t FROM TypeCreditSchema t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeCreditSchema.findByIdSchema", query = "SELECT t FROM TypeCreditSchema t WHERE t.idSchema = :idSchema"),
    @NamedQuery(name = "TypeCreditSchema.findByNameSchema", query = "SELECT t FROM TypeCreditSchema t WHERE t.nameSchema = :nameSchema"),
    @NamedQuery(name = "TypeCreditSchema.findBySizePercent", query = "SELECT t FROM TypeCreditSchema t WHERE t.sizePercent = :sizePercent"),
    @NamedQuery(name = "TypeCreditSchema.findByIdTypedemensPercent", query = "SELECT t FROM TypeCreditSchema t WHERE t.idTypedemensPercent = :idTypedemensPercent"),
    @NamedQuery(name = "TypeCreditSchema.findBySizePeni", query = "SELECT t FROM TypeCreditSchema t WHERE t.sizePeni = :sizePeni"),
    @NamedQuery(name = "TypeCreditSchema.findByIdTypedemensPeni", query = "SELECT t FROM TypeCreditSchema t WHERE t.idTypedemensPeni = :idTypedemensPeni"),
    @NamedQuery(name = "TypeCreditSchema.findByStatusRow", query = "SELECT t FROM TypeCreditSchema t WHERE t.statusRow = :statusRow")})
public class TypeCreditSchema implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_schema")
    private Integer idSchema;
    @Column(name = "name_schema", length = 50)
    private String nameSchema;
    @Column(name = "size_percent")
    private Integer sizePercent;
    @Column(name = "id_typedemens_percent")
    private Integer idTypedemensPercent;
    @Column(name = "size_peni")
    private Integer sizePeni;
    @Column(name = "id_typedemens_peni")
    private Integer idTypedemensPeni;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeCreditSchema() {
    }

    public TypeCreditSchema(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdSchema() {
        return idSchema;
    }

    public void setIdSchema(Integer idSchema) {
        this.idSchema = idSchema;
    }

    public String getNameSchema() {
        return nameSchema;
    }

    public void setNameSchema(String nameSchema) {
        this.nameSchema = nameSchema;
    }

    public Integer getSizePercent() {
        return sizePercent;
    }

    public void setSizePercent(Integer sizePercent) {
        this.sizePercent = sizePercent;
    }

    public Integer getIdTypedemensPercent() {
        return idTypedemensPercent;
    }

    public void setIdTypedemensPercent(Integer idTypedemensPercent) {
        this.idTypedemensPercent = idTypedemensPercent;
    }

    public Integer getSizePeni() {
        return sizePeni;
    }

    public void setSizePeni(Integer sizePeni) {
        this.sizePeni = sizePeni;
    }

    public Integer getIdTypedemensPeni() {
        return idTypedemensPeni;
    }

    public void setIdTypedemensPeni(Integer idTypedemensPeni) {
        this.idTypedemensPeni = idTypedemensPeni;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeCreditSchema)) {
            return false;
        }
        TypeCreditSchema other = (TypeCreditSchema) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeCreditSchema[ idRecord=" + idRecord + " ]";
    }
    
}
