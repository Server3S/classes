/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "account_lombard", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountLombard.findAll", query = "SELECT a FROM AccountLombard a"),
    @NamedQuery(name = "AccountLombard.findByIdRecord", query = "SELECT a FROM AccountLombard a WHERE a.idRecord = :idRecord"),
    @NamedQuery(name = "AccountLombard.findByIdAccountLombard", query = "SELECT a FROM AccountLombard a WHERE a.idAccountLombard = :idAccountLombard"),
    @NamedQuery(name = "AccountLombard.findByAccountNumberLombard", query = "SELECT a FROM AccountLombard a WHERE a.accountNumberLombard = :accountNumberLombard"),
    @NamedQuery(name = "AccountLombard.findByAccountNameLombard", query = "SELECT a FROM AccountLombard a WHERE a.accountNameLombard = :accountNameLombard"),
    @NamedQuery(name = "AccountLombard.findByCurrentSaldoAcclombard", query = "SELECT a FROM AccountLombard a WHERE a.currentSaldoAcclombard = :currentSaldoAcclombard"),
    @NamedQuery(name = "AccountLombard.findByIdStateAcclombard", query = "SELECT a FROM AccountLombard a WHERE a.idStateAcclombard = :idStateAcclombard"),
    @NamedQuery(name = "AccountLombard.findByNameLastactionAcclombard", query = "SELECT a FROM AccountLombard a WHERE a.nameLastactionAcclombard = :nameLastactionAcclombard"),
    @NamedQuery(name = "AccountLombard.findByUserLastactionAcclombard", query = "SELECT a FROM AccountLombard a WHERE a.userLastactionAcclombard = :userLastactionAcclombard"),
    @NamedQuery(name = "AccountLombard.findByDatetimeLastactionAcclombard", query = "SELECT a FROM AccountLombard a WHERE a.datetimeLastactionAcclombard = :datetimeLastactionAcclombard"),
    @NamedQuery(name = "AccountLombard.findByCodeLastactionAcclombard", query = "SELECT a FROM AccountLombard a WHERE a.codeLastactionAcclombard = :codeLastactionAcclombard"),
    @NamedQuery(name = "AccountLombard.findByStatusRow", query = "SELECT a FROM AccountLombard a WHERE a.statusRow = :statusRow")})
public class AccountLombard implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_account_lombard")
    private Integer idAccountLombard;
    @Column(name = "account_number_lombard")
    private Integer accountNumberLombard;
    @Column(name = "account_name_lombard", length = 2147483647)
    private String accountNameLombard;
    @Column(name = "current_saldo_acclombard")
    private BigInteger currentSaldoAcclombard;
    @Column(name = "id_state_acclombard")
    private Integer idStateAcclombard;
    @Column(name = "name_lastaction_acclombard", length = 25)
    private String nameLastactionAcclombard;
    @Column(name = "user_lastaction_acclombard", length = 30)
    private String userLastactionAcclombard;
    @Column(name = "datetime_lastaction_acclombard", length = 20)
    private String datetimeLastactionAcclombard;
    @Column(name = "code_lastaction_acclombard")
    private Integer codeLastactionAcclombard;
    @Column(name = "status_row")
    private Short statusRow;

    public AccountLombard() {
    }

    public AccountLombard(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdAccountLombard() {
        return idAccountLombard;
    }

    public void setIdAccountLombard(Integer idAccountLombard) {
        this.idAccountLombard = idAccountLombard;
    }

    public Integer getAccountNumberLombard() {
        return accountNumberLombard;
    }

    public void setAccountNumberLombard(Integer accountNumberLombard) {
        this.accountNumberLombard = accountNumberLombard;
    }

    public String getAccountNameLombard() {
        return accountNameLombard;
    }

    public void setAccountNameLombard(String accountNameLombard) {
        this.accountNameLombard = accountNameLombard;
    }

    public BigInteger getCurrentSaldoAcclombard() {
        return currentSaldoAcclombard;
    }

    public void setCurrentSaldoAcclombard(BigInteger currentSaldoAcclombard) {
        this.currentSaldoAcclombard = currentSaldoAcclombard;
    }

    public Integer getIdStateAcclombard() {
        return idStateAcclombard;
    }

    public void setIdStateAcclombard(Integer idStateAcclombard) {
        this.idStateAcclombard = idStateAcclombard;
    }

    public String getNameLastactionAcclombard() {
        return nameLastactionAcclombard;
    }

    public void setNameLastactionAcclombard(String nameLastactionAcclombard) {
        this.nameLastactionAcclombard = nameLastactionAcclombard;
    }

    public String getUserLastactionAcclombard() {
        return userLastactionAcclombard;
    }

    public void setUserLastactionAcclombard(String userLastactionAcclombard) {
        this.userLastactionAcclombard = userLastactionAcclombard;
    }

    public String getDatetimeLastactionAcclombard() {
        return datetimeLastactionAcclombard;
    }

    public void setDatetimeLastactionAcclombard(String datetimeLastactionAcclombard) {
        this.datetimeLastactionAcclombard = datetimeLastactionAcclombard;
    }

    public Integer getCodeLastactionAcclombard() {
        return codeLastactionAcclombard;
    }

    public void setCodeLastactionAcclombard(Integer codeLastactionAcclombard) {
        this.codeLastactionAcclombard = codeLastactionAcclombard;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountLombard)) {
            return false;
        }
        AccountLombard other = (AccountLombard) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.AccountLombard[ idRecord=" + idRecord + " ]";
    }
    
}
