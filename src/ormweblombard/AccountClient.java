/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "account_client", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountClient.findAll", query = "SELECT a FROM AccountClient a"),
    @NamedQuery(name = "AccountClient.findByIdRecord", query = "SELECT a FROM AccountClient a WHERE a.idRecord = :idRecord"),
    @NamedQuery(name = "AccountClient.findByIdAccount", query = "SELECT a FROM AccountClient a WHERE a.idAccount = :idAccount"),
    @NamedQuery(name = "AccountClient.findByAccountNumber", query = "SELECT a FROM AccountClient a WHERE a.accountNumber = :accountNumber"),
    @NamedQuery(name = "AccountClient.findByAccountName", query = "SELECT a FROM AccountClient a WHERE a.accountName = :accountName"),
    @NamedQuery(name = "AccountClient.findByIdLocalClient", query = "SELECT a FROM AccountClient a WHERE a.idLocalClient = :idLocalClient"),
    @NamedQuery(name = "AccountClient.findByCurrentSaldoAccount", query = "SELECT a FROM AccountClient a WHERE a.currentSaldoAccount = :currentSaldoAccount"),
    @NamedQuery(name = "AccountClient.findByIdStateAccount", query = "SELECT a FROM AccountClient a WHERE a.idStateAccount = :idStateAccount"),
    @NamedQuery(name = "AccountClient.findByCodeActionAccount", query = "SELECT a FROM AccountClient a WHERE a.codeActionAccount = :codeActionAccount"),
    @NamedQuery(name = "AccountClient.findByDatetimeAction", query = "SELECT a FROM AccountClient a WHERE a.datetimeAction = :datetimeAction"),
    @NamedQuery(name = "AccountClient.findByUserActionAccount", query = "SELECT a FROM AccountClient a WHERE a.userActionAccount = :userActionAccount"),
    @NamedQuery(name = "AccountClient.findByNameActionAccount", query = "SELECT a FROM AccountClient a WHERE a.nameActionAccount = :nameActionAccount"),
    @NamedQuery(name = "AccountClient.findByStateAccount", query = "SELECT a FROM AccountClient a WHERE a.stateAccount = :stateAccount")})
public class AccountClient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_account")
    private Integer idAccount;
    @Column(name = "account_number")
    private Integer accountNumber;
    @Column(name = "account_name", length = 100)
    private String accountName;
    @Column(name = "id_local_client")
    private Integer idLocalClient;
    @Column(name = "current_saldo_account")
    private BigInteger currentSaldoAccount;
    @Column(name = "id_state_account")
    private Integer idStateAccount;
    @Column(name = "code_action_account")
    private Integer codeActionAccount;
    @Column(name = "datetime_action", length = 20)
    private String datetimeAction;
    @Column(name = "user_action_account", length = 30)
    private String userActionAccount;
    @Column(name = "name_action_account", length = 30)
    private String nameActionAccount;
    @Column(name = "state_account")
    private Short stateAccount;

    public AccountClient() {
    }

    public AccountClient(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    public Integer getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Integer accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getIdLocalClient() {
        return idLocalClient;
    }

    public void setIdLocalClient(Integer idLocalClient) {
        this.idLocalClient = idLocalClient;
    }

    public BigInteger getCurrentSaldoAccount() {
        return currentSaldoAccount;
    }

    public void setCurrentSaldoAccount(BigInteger currentSaldoAccount) {
        this.currentSaldoAccount = currentSaldoAccount;
    }

    public Integer getIdStateAccount() {
        return idStateAccount;
    }

    public void setIdStateAccount(Integer idStateAccount) {
        this.idStateAccount = idStateAccount;
    }

    public Integer getCodeActionAccount() {
        return codeActionAccount;
    }

    public void setCodeActionAccount(Integer codeActionAccount) {
        this.codeActionAccount = codeActionAccount;
    }

    public String getDatetimeAction() {
        return datetimeAction;
    }

    public void setDatetimeAction(String datetimeAction) {
        this.datetimeAction = datetimeAction;
    }

    public String getUserActionAccount() {
        return userActionAccount;
    }

    public void setUserActionAccount(String userActionAccount) {
        this.userActionAccount = userActionAccount;
    }

    public String getNameActionAccount() {
        return nameActionAccount;
    }

    public void setNameActionAccount(String nameActionAccount) {
        this.nameActionAccount = nameActionAccount;
    }

    public Short getStateAccount() {
        return stateAccount;
    }

    public void setStateAccount(Short stateAccount) {
        this.stateAccount = stateAccount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountClient)) {
            return false;
        }
        AccountClient other = (AccountClient) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.AccountClient[ idRecord=" + idRecord + " ]";
    }
    
}
