/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_nsi_210", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiNsi210.findAll", query = "SELECT n FROM NikiNsi210 n"),
    @NamedQuery(name = "NikiNsi210.findByKod", query = "SELECT n FROM NikiNsi210 n WHERE n.kod = :kod"),
    @NamedQuery(name = "NikiNsi210.findByNameTypepercent", query = "SELECT n FROM NikiNsi210 n WHERE n.nameTypepercent = :nameTypepercent"),
    @NamedQuery(name = "NikiNsi210.findByIdRecord", query = "SELECT n FROM NikiNsi210 n WHERE n.idRecord = :idRecord")})
public class NikiNsi210 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "kod", length = 3)
    private String kod;
    @Column(name = "name_typepercent", length = 30)
    private String nameTypepercent;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiNsi210() {
    }

    public NikiNsi210(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getNameTypepercent() {
        return nameTypepercent;
    }

    public void setNameTypepercent(String nameTypepercent) {
        this.nameTypepercent = nameTypepercent;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiNsi210)) {
            return false;
        }
        NikiNsi210 other = (NikiNsi210) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiNsi210[ idRecord=" + idRecord + " ]";
    }
    
}
