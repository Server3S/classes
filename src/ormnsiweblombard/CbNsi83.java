/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_83", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi83.findAll", query = "SELECT c FROM CbNsi83 c"),
    @NamedQuery(name = "CbNsi83.findByNsiId", query = "SELECT c FROM CbNsi83 c WHERE c.nsiId = :nsiId"),
    @NamedQuery(name = "CbNsi83.findByCodeBorrower", query = "SELECT c FROM CbNsi83 c WHERE c.codeBorrower = :codeBorrower"),
    @NamedQuery(name = "CbNsi83.findByCodeSubBorrower", query = "SELECT c FROM CbNsi83 c WHERE c.codeSubBorrower = :codeSubBorrower"),
    @NamedQuery(name = "CbNsi83.findByNameBorrower", query = "SELECT c FROM CbNsi83 c WHERE c.nameBorrower = :nameBorrower"),
    @NamedQuery(name = "CbNsi83.findByDateOpen", query = "SELECT c FROM CbNsi83 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi83.findByIdRecord", query = "SELECT c FROM CbNsi83 c WHERE c.idRecord = :idRecord")})
public class CbNsi83 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nsi_id", length = 3)
    private String nsiId;
    @Column(name = "code_borrower", length = 3)
    private String codeBorrower;
    @Column(name = "code_sub_borrower", length = 3)
    private String codeSubBorrower;
    @Column(name = "name_borrower", length = 200)
    private String nameBorrower;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi83() {
    }

    public CbNsi83(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNsiId() {
        return nsiId;
    }

    public void setNsiId(String nsiId) {
        this.nsiId = nsiId;
    }

    public String getCodeBorrower() {
        return codeBorrower;
    }

    public void setCodeBorrower(String codeBorrower) {
        this.codeBorrower = codeBorrower;
    }

    public String getCodeSubBorrower() {
        return codeSubBorrower;
    }

    public void setCodeSubBorrower(String codeSubBorrower) {
        this.codeSubBorrower = codeSubBorrower;
    }

    public String getNameBorrower() {
        return nameBorrower;
    }

    public void setNameBorrower(String nameBorrower) {
        this.nameBorrower = nameBorrower;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi83)) {
            return false;
        }
        CbNsi83 other = (CbNsi83) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi83[ idRecord=" + idRecord + " ]";
    }
    
}
