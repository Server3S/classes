/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_04", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi04.findAll", query = "SELECT c FROM CbNsi04 c"),
    @NamedQuery(name = "CbNsi04.findByNsiId", query = "SELECT c FROM CbNsi04 c WHERE c.nsiId = :nsiId"),
    @NamedQuery(name = "CbNsi04.findByKodVid", query = "SELECT c FROM CbNsi04 c WHERE c.kodVid = :kodVid"),
    @NamedQuery(name = "CbNsi04.findByNameVid", query = "SELECT c FROM CbNsi04 c WHERE c.nameVid = :nameVid"),
    @NamedQuery(name = "CbNsi04.findByDateOpen", query = "SELECT c FROM CbNsi04 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi04.findByDateClose", query = "SELECT c FROM CbNsi04 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi04.findByActive", query = "SELECT c FROM CbNsi04 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi04.findByIdRecord", query = "SELECT c FROM CbNsi04 c WHERE c.idRecord = :idRecord")})
public class CbNsi04 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nsi_id", length = 10)
    private String nsiId;
    @Column(name = "kod_vid", length = 10)
    private String kodVid;
    @Column(name = "name_vid", length = 40)
    private String nameVid;
    @Column(name = "date_open", length = 8)
    private String dateOpen;
    @Column(name = "date_close", length = 8)
    private String dateClose;
    @Column(name = "active", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi04() {
    }

    public CbNsi04(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNsiId() {
        return nsiId;
    }

    public void setNsiId(String nsiId) {
        this.nsiId = nsiId;
    }

    public String getKodVid() {
        return kodVid;
    }

    public void setKodVid(String kodVid) {
        this.kodVid = kodVid;
    }

    public String getNameVid() {
        return nameVid;
    }

    public void setNameVid(String nameVid) {
        this.nameVid = nameVid;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi04)) {
            return false;
        }
        CbNsi04 other = (CbNsi04) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi04[ idRecord=" + idRecord + " ]";
    }
    
}
