/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_status_lien", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeStatusLien.findAll", query = "SELECT t FROM TypeStatusLien t"),
    @NamedQuery(name = "TypeStatusLien.findByIdRecord", query = "SELECT t FROM TypeStatusLien t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeStatusLien.findByIdTypeStatusLien", query = "SELECT t FROM TypeStatusLien t WHERE t.idTypeStatusLien = :idTypeStatusLien"),
    @NamedQuery(name = "TypeStatusLien.findByNameTypeStatusLien", query = "SELECT t FROM TypeStatusLien t WHERE t.nameTypeStatusLien = :nameTypeStatusLien"),
    @NamedQuery(name = "TypeStatusLien.findByStatusRow", query = "SELECT t FROM TypeStatusLien t WHERE t.statusRow = :statusRow")})
public class TypeStatusLien implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_type_status_lien")
    private Integer idTypeStatusLien;
    @Column(name = "name_type_status_lien", length = 50)
    private String nameTypeStatusLien;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeStatusLien() {
    }

    public TypeStatusLien(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdTypeStatusLien() {
        return idTypeStatusLien;
    }

    public void setIdTypeStatusLien(Integer idTypeStatusLien) {
        this.idTypeStatusLien = idTypeStatusLien;
    }

    public String getNameTypeStatusLien() {
        return nameTypeStatusLien;
    }

    public void setNameTypeStatusLien(String nameTypeStatusLien) {
        this.nameTypeStatusLien = nameTypeStatusLien;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeStatusLien)) {
            return false;
        }
        TypeStatusLien other = (TypeStatusLien) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeStatusLien[ idRecord=" + idRecord + " ]";
    }
    
}
