/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_35", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi35.findAll", query = "SELECT c FROM CbNsi35 c"),
    @NamedQuery(name = "CbNsi35.findByNciId", query = "SELECT c FROM CbNsi35 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi35.findByKodO", query = "SELECT c FROM CbNsi35 c WHERE c.kodO = :kodO"),
    @NamedQuery(name = "CbNsi35.findByNameO", query = "SELECT c FROM CbNsi35 c WHERE c.nameO = :nameO"),
    @NamedQuery(name = "CbNsi35.findByDataOpen", query = "SELECT c FROM CbNsi35 c WHERE c.dataOpen = :dataOpen"),
    @NamedQuery(name = "CbNsi35.findByDataClose", query = "SELECT c FROM CbNsi35 c WHERE c.dataClose = :dataClose"),
    @NamedQuery(name = "CbNsi35.findByActive", query = "SELECT c FROM CbNsi35 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi35.findByIdRecord", query = "SELECT c FROM CbNsi35 c WHERE c.idRecord = :idRecord")})
public class CbNsi35 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Column(name = "kod_o", length = 10)
    private String kodO;
    @Column(name = "name_o", length = 50)
    private String nameO;
    @Column(name = "data_open", length = 8)
    private String dataOpen;
    @Column(name = "data_close", length = 8)
    private String dataClose;
    @Column(name = "active", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi35() {
    }

    public CbNsi35(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getKodO() {
        return kodO;
    }

    public void setKodO(String kodO) {
        this.kodO = kodO;
    }

    public String getNameO() {
        return nameO;
    }

    public void setNameO(String nameO) {
        this.nameO = nameO;
    }

    public String getDataOpen() {
        return dataOpen;
    }

    public void setDataOpen(String dataOpen) {
        this.dataOpen = dataOpen;
    }

    public String getDataClose() {
        return dataClose;
    }

    public void setDataClose(String dataClose) {
        this.dataClose = dataClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi35)) {
            return false;
        }
        CbNsi35 other = (CbNsi35) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi35[ idRecord=" + idRecord + " ]";
    }
    
}
