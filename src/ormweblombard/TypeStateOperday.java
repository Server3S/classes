/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_state_operday", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeStateOperday.findAll", query = "SELECT t FROM TypeStateOperday t"),
    @NamedQuery(name = "TypeStateOperday.findByIdRecord", query = "SELECT t FROM TypeStateOperday t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeStateOperday.findByIdTypestateOperday", query = "SELECT t FROM TypeStateOperday t WHERE t.idTypestateOperday = :idTypestateOperday"),
    @NamedQuery(name = "TypeStateOperday.findByNameTypestateOperday", query = "SELECT t FROM TypeStateOperday t WHERE t.nameTypestateOperday = :nameTypestateOperday"),
    @NamedQuery(name = "TypeStateOperday.findByDescriptTypestateOperday", query = "SELECT t FROM TypeStateOperday t WHERE t.descriptTypestateOperday = :descriptTypestateOperday"),
    @NamedQuery(name = "TypeStateOperday.findByStatusRow", query = "SELECT t FROM TypeStateOperday t WHERE t.statusRow = :statusRow")})
public class TypeStateOperday implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_typestate_operday")
    private Integer idTypestateOperday;
    @Column(name = "name_typestate_operday", length = 30)
    private String nameTypestateOperday;
    @Column(name = "descript_typestate_operday", length = 300)
    private String descriptTypestateOperday;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeStateOperday() {
    }

    public TypeStateOperday(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdTypestateOperday() {
        return idTypestateOperday;
    }

    public void setIdTypestateOperday(Integer idTypestateOperday) {
        this.idTypestateOperday = idTypestateOperday;
    }

    public String getNameTypestateOperday() {
        return nameTypestateOperday;
    }

    public void setNameTypestateOperday(String nameTypestateOperday) {
        this.nameTypestateOperday = nameTypestateOperday;
    }

    public String getDescriptTypestateOperday() {
        return descriptTypestateOperday;
    }

    public void setDescriptTypestateOperday(String descriptTypestateOperday) {
        this.descriptTypestateOperday = descriptTypestateOperday;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeStateOperday)) {
            return false;
        }
        TypeStateOperday other = (TypeStateOperday) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeStateOperday[ idRecord=" + idRecord + " ]";
    }
    
}
