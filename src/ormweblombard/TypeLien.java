/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_lien", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeLien.findAll", query = "SELECT t FROM TypeLien t"),
    @NamedQuery(name = "TypeLien.findByIdRecord", query = "SELECT t FROM TypeLien t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeLien.findByIdCategoryLien", query = "SELECT t FROM TypeLien t WHERE t.idCategoryLien = :idCategoryLien"),
    @NamedQuery(name = "TypeLien.findByIdTypeLien", query = "SELECT t FROM TypeLien t WHERE t.idTypeLien = :idTypeLien"),
    @NamedQuery(name = "TypeLien.findByNameTypeLien", query = "SELECT t FROM TypeLien t WHERE t.nameTypeLien = :nameTypeLien"),
    @NamedQuery(name = "TypeLien.findByStatusRow", query = "SELECT t FROM TypeLien t WHERE t.statusRow = :statusRow")})
public class TypeLien implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_category_lien")
    private Integer idCategoryLien;
    @Column(name = "id_type_lien")
    private Integer idTypeLien;
    @Column(name = "name_type_lien", length = 150)
    private String nameTypeLien;
    @Column(name = "status_row")
    private Short statusRow;

    public TypeLien() {
    }

    public TypeLien(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdCategoryLien() {
        return idCategoryLien;
    }

    public void setIdCategoryLien(Integer idCategoryLien) {
        this.idCategoryLien = idCategoryLien;
    }

    public Integer getIdTypeLien() {
        return idTypeLien;
    }

    public void setIdTypeLien(Integer idTypeLien) {
        this.idTypeLien = idTypeLien;
    }

    public String getNameTypeLien() {
        return nameTypeLien;
    }

    public void setNameTypeLien(String nameTypeLien) {
        this.nameTypeLien = nameTypeLien;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeLien)) {
            return false;
        }
        TypeLien other = (TypeLien) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeLien[ idRecord=" + idRecord + " ]";
    }
    
}
