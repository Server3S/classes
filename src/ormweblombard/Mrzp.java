/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "mrzp", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mrzp.findAll", query = "SELECT m FROM Mrzp m"),
    @NamedQuery(name = "Mrzp.findByDateActive", query = "SELECT m FROM Mrzp m WHERE m.dateActive = :dateActive"),
    @NamedQuery(name = "Mrzp.findByCurrency", query = "SELECT m FROM Mrzp m WHERE m.currency = :currency"),
    @NamedQuery(name = "Mrzp.findByMrzp", query = "SELECT m FROM Mrzp m WHERE m.mrzp = :mrzp"),
    @NamedQuery(name = "Mrzp.findByMinPension", query = "SELECT m FROM Mrzp m WHERE m.minPension = :minPension"),
    @NamedQuery(name = "Mrzp.findByMinChild", query = "SELECT m FROM Mrzp m WHERE m.minChild = :minChild"),
    @NamedQuery(name = "Mrzp.findByMinWorkless", query = "SELECT m FROM Mrzp m WHERE m.minWorkless = :minWorkless"),
    @NamedQuery(name = "Mrzp.findByNormativeAct", query = "SELECT m FROM Mrzp m WHERE m.normativeAct = :normativeAct"),
    @NamedQuery(name = "Mrzp.findByStatusRow", query = "SELECT m FROM Mrzp m WHERE m.statusRow = :statusRow"),
    @NamedQuery(name = "Mrzp.findByIdRecord", query = "SELECT m FROM Mrzp m WHERE m.idRecord = :idRecord")})
public class Mrzp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "date_active", length = 10)
    private String dateActive;
    @Column(name = "currency", length = 10)
    private String currency;
    @Column(name = "mrzp")
    private Integer mrzp;
    @Column(name = "min_pension")
    private Integer minPension;
    @Column(name = "min_child")
    private Integer minChild;
    @Column(name = "min_workless")
    private Integer minWorkless;
    @Column(name = "normative_act", length = 30)
    private String normativeAct;
    @Column(name = "status_row")
    private Short statusRow;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public Mrzp() {
    }

    public Mrzp(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getDateActive() {
        return dateActive;
    }

    public void setDateActive(String dateActive) {
        this.dateActive = dateActive;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getMrzp() {
        return mrzp;
    }

    public void setMrzp(Integer mrzp) {
        this.mrzp = mrzp;
    }

    public Integer getMinPension() {
        return minPension;
    }

    public void setMinPension(Integer minPension) {
        this.minPension = minPension;
    }

    public Integer getMinChild() {
        return minChild;
    }

    public void setMinChild(Integer minChild) {
        this.minChild = minChild;
    }

    public Integer getMinWorkless() {
        return minWorkless;
    }

    public void setMinWorkless(Integer minWorkless) {
        this.minWorkless = minWorkless;
    }

    public String getNormativeAct() {
        return normativeAct;
    }

    public void setNormativeAct(String normativeAct) {
        this.normativeAct = normativeAct;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mrzp)) {
            return false;
        }
        Mrzp other = (Mrzp) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.Mrzp[ idRecord=" + idRecord + " ]";
    }
    
}
