/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_nsi_list", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiNsiList.findAll", query = "SELECT n FROM NikiNsiList n"),
    @NamedQuery(name = "NikiNsiList.findByNsiId", query = "SELECT n FROM NikiNsiList n WHERE n.nsiId = :nsiId"),
    @NamedQuery(name = "NikiNsiList.findByNsiName", query = "SELECT n FROM NikiNsiList n WHERE n.nsiName = :nsiName"),
    @NamedQuery(name = "NikiNsiList.findByIdRecord", query = "SELECT n FROM NikiNsiList n WHERE n.idRecord = :idRecord")})
public class NikiNsiList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nsi_id", length = 5)
    private String nsiId;
    @Column(name = "nsi_name", length = 200)
    private String nsiName;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiNsiList() {
    }

    public NikiNsiList(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNsiId() {
        return nsiId;
    }

    public void setNsiId(String nsiId) {
        this.nsiId = nsiId;
    }

    public String getNsiName() {
        return nsiName;
    }

    public void setNsiName(String nsiName) {
        this.nsiName = nsiName;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiNsiList)) {
            return false;
        }
        NikiNsiList other = (NikiNsiList) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiNsiList[ idRecord=" + idRecord + " ]";
    }
    
}
