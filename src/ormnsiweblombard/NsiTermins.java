/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "nsi_termins", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NsiTermins.findAll", query = "SELECT n FROM NsiTermins n"),
    @NamedQuery(name = "NsiTermins.findByTermin", query = "SELECT n FROM NsiTermins n WHERE n.termin = :termin"),
    @NamedQuery(name = "NsiTermins.findByRelationTermin", query = "SELECT n FROM NsiTermins n WHERE n.relationTermin = :relationTermin"),
    @NamedQuery(name = "NsiTermins.findByDescriptTermin", query = "SELECT n FROM NsiTermins n WHERE n.descriptTermin = :descriptTermin"),
    @NamedQuery(name = "NsiTermins.findByIdRecord", query = "SELECT n FROM NsiTermins n WHERE n.idRecord = :idRecord")})
public class NsiTermins implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "termin", length = 200)
    private String termin;
    @Column(name = "relation_termin", length = 3)
    private String relationTermin;
    @Column(name = "descript_termin", length = 1000)
    private String descriptTermin;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NsiTermins() {
    }

    public NsiTermins(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getTermin() {
        return termin;
    }

    public void setTermin(String termin) {
        this.termin = termin;
    }

    public String getRelationTermin() {
        return relationTermin;
    }

    public void setRelationTermin(String relationTermin) {
        this.relationTermin = relationTermin;
    }

    public String getDescriptTermin() {
        return descriptTermin;
    }

    public void setDescriptTermin(String descriptTermin) {
        this.descriptTermin = descriptTermin;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NsiTermins)) {
            return false;
        }
        NsiTermins other = (NsiTermins) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NsiTermins[ idRecord=" + idRecord + " ]";
    }
    
}
