/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_nsi_234", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiNsi234.findAll", query = "SELECT n FROM NikiNsi234 n"),
    @NamedQuery(name = "NikiNsi234.findByNameRezident", query = "SELECT n FROM NikiNsi234 n WHERE n.nameRezident = :nameRezident"),
    @NamedQuery(name = "NikiNsi234.findByKodTypepersonaldocument", query = "SELECT n FROM NikiNsi234 n WHERE n.kodTypepersonaldocument = :kodTypepersonaldocument"),
    @NamedQuery(name = "NikiNsi234.findByNameTypepersonaldocument", query = "SELECT n FROM NikiNsi234 n WHERE n.nameTypepersonaldocument = :nameTypepersonaldocument"),
    @NamedQuery(name = "NikiNsi234.findBySerial", query = "SELECT n FROM NikiNsi234 n WHERE n.serial = :serial"),
    @NamedQuery(name = "NikiNsi234.findByNumber", query = "SELECT n FROM NikiNsi234 n WHERE n.number = :number"),
    @NamedQuery(name = "NikiNsi234.findByDateEnd", query = "SELECT n FROM NikiNsi234 n WHERE n.dateEnd = :dateEnd"),
    @NamedQuery(name = "NikiNsi234.findByIdRecord", query = "SELECT n FROM NikiNsi234 n WHERE n.idRecord = :idRecord")})
public class NikiNsi234 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "name_rezident", length = 25)
    private String nameRezident;
    @Column(name = "kod_typepersonaldocument", length = 100)
    private String kodTypepersonaldocument;
    @Column(name = "name_typepersonaldocument", length = 2147483647)
    private String nameTypepersonaldocument;
    @Column(name = "serial", length = 2)
    private String serial;
    @Column(name = "number", length = 7)
    private String number;
    @Column(name = "date_end", length = 8)
    private String dateEnd;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiNsi234() {
    }

    public NikiNsi234(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNameRezident() {
        return nameRezident;
    }

    public void setNameRezident(String nameRezident) {
        this.nameRezident = nameRezident;
    }

    public String getKodTypepersonaldocument() {
        return kodTypepersonaldocument;
    }

    public void setKodTypepersonaldocument(String kodTypepersonaldocument) {
        this.kodTypepersonaldocument = kodTypepersonaldocument;
    }

    public String getNameTypepersonaldocument() {
        return nameTypepersonaldocument;
    }

    public void setNameTypepersonaldocument(String nameTypepersonaldocument) {
        this.nameTypepersonaldocument = nameTypepersonaldocument;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiNsi234)) {
            return false;
        }
        NikiNsi234 other = (NikiNsi234) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiNsi234[ idRecord=" + idRecord + " ]";
    }
    
}
