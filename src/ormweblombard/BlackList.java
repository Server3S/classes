/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "black_list", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BlackList.findAll", query = "SELECT b FROM BlackList b"),
    @NamedQuery(name = "BlackList.findByIdRecord", query = "SELECT b FROM BlackList b WHERE b.idRecord = :idRecord"),
    @NamedQuery(name = "BlackList.findByIdLocalClient", query = "SELECT b FROM BlackList b WHERE b.idLocalClient = :idLocalClient"),
    @NamedQuery(name = "BlackList.findByDateInBl", query = "SELECT b FROM BlackList b WHERE b.dateInBl = :dateInBl"),
    @NamedQuery(name = "BlackList.findByDateOutBl", query = "SELECT b FROM BlackList b WHERE b.dateOutBl = :dateOutBl"),
    @NamedQuery(name = "BlackList.findByUserInBl", query = "SELECT b FROM BlackList b WHERE b.userInBl = :userInBl"),
    @NamedQuery(name = "BlackList.findByUserOutBl", query = "SELECT b FROM BlackList b WHERE b.userOutBl = :userOutBl"),
    @NamedQuery(name = "BlackList.findByCodeTypeBl", query = "SELECT b FROM BlackList b WHERE b.codeTypeBl = :codeTypeBl"),
    @NamedQuery(name = "BlackList.findByNameTypeBl", query = "SELECT b FROM BlackList b WHERE b.nameTypeBl = :nameTypeBl"),
    @NamedQuery(name = "BlackList.findByStatusRow", query = "SELECT b FROM BlackList b WHERE b.statusRow = :statusRow")})
public class BlackList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_local_client")
    private Integer idLocalClient;
    @Column(name = "date_in_bl", length = 10)
    private String dateInBl;
    @Column(name = "date_out_bl", length = 10)
    private String dateOutBl;
    @Column(name = "user_in_bl", length = 50)
    private String userInBl;
    @Column(name = "user_out_bl", length = 50)
    private String userOutBl;
    @Column(name = "code_type_bl", length = 2)
    private String codeTypeBl;
    @Column(name = "name_type_bl", length = 30)
    private String nameTypeBl;
    @Column(name = "status_row")
    private Short statusRow;

    public BlackList() {
    }

    public BlackList(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdLocalClient() {
        return idLocalClient;
    }

    public void setIdLocalClient(Integer idLocalClient) {
        this.idLocalClient = idLocalClient;
    }

    public String getDateInBl() {
        return dateInBl;
    }

    public void setDateInBl(String dateInBl) {
        this.dateInBl = dateInBl;
    }

    public String getDateOutBl() {
        return dateOutBl;
    }

    public void setDateOutBl(String dateOutBl) {
        this.dateOutBl = dateOutBl;
    }

    public String getUserInBl() {
        return userInBl;
    }

    public void setUserInBl(String userInBl) {
        this.userInBl = userInBl;
    }

    public String getUserOutBl() {
        return userOutBl;
    }

    public void setUserOutBl(String userOutBl) {
        this.userOutBl = userOutBl;
    }

    public String getCodeTypeBl() {
        return codeTypeBl;
    }

    public void setCodeTypeBl(String codeTypeBl) {
        this.codeTypeBl = codeTypeBl;
    }

    public String getNameTypeBl() {
        return nameTypeBl;
    }

    public void setNameTypeBl(String nameTypeBl) {
        this.nameTypeBl = nameTypeBl;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BlackList)) {
            return false;
        }
        BlackList other = (BlackList) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.BlackList[ idRecord=" + idRecord + " ]";
    }
    
}
