/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "kb_nsi_type_atribut", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KbNsiTypeAtribut.findAll", query = "SELECT k FROM KbNsiTypeAtribut k"),
    @NamedQuery(name = "KbNsiTypeAtribut.findByNameAtribut", query = "SELECT k FROM KbNsiTypeAtribut k WHERE k.nameAtribut = :nameAtribut"),
    @NamedQuery(name = "KbNsiTypeAtribut.findByTypeAtribut", query = "SELECT k FROM KbNsiTypeAtribut k WHERE k.typeAtribut = :typeAtribut"),
    @NamedQuery(name = "KbNsiTypeAtribut.findBySizeAtribut", query = "SELECT k FROM KbNsiTypeAtribut k WHERE k.sizeAtribut = :sizeAtribut"),
    @NamedQuery(name = "KbNsiTypeAtribut.findByFormatAtribut", query = "SELECT k FROM KbNsiTypeAtribut k WHERE k.formatAtribut = :formatAtribut"),
    @NamedQuery(name = "KbNsiTypeAtribut.findByDescriptAtribut", query = "SELECT k FROM KbNsiTypeAtribut k WHERE k.descriptAtribut = :descriptAtribut"),
    @NamedQuery(name = "KbNsiTypeAtribut.findBySystemName", query = "SELECT k FROM KbNsiTypeAtribut k WHERE k.systemName = :systemName"),
    @NamedQuery(name = "KbNsiTypeAtribut.findByIdRecord", query = "SELECT k FROM KbNsiTypeAtribut k WHERE k.idRecord = :idRecord")})
public class KbNsiTypeAtribut implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "name_atribut", length = 25)
    private String nameAtribut;
    @Column(name = "type_atribut", length = 25)
    private String typeAtribut;
    @Column(name = "size_atribut")
    private Integer sizeAtribut;
    @Column(name = "format_atribut", length = 100)
    private String formatAtribut;
    @Column(name = "descript_atribut", length = 500)
    private String descriptAtribut;
    @Column(name = "system_name", length = 30)
    private String systemName;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public KbNsiTypeAtribut() {
    }

    public KbNsiTypeAtribut(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNameAtribut() {
        return nameAtribut;
    }

    public void setNameAtribut(String nameAtribut) {
        this.nameAtribut = nameAtribut;
    }

    public String getTypeAtribut() {
        return typeAtribut;
    }

    public void setTypeAtribut(String typeAtribut) {
        this.typeAtribut = typeAtribut;
    }

    public Integer getSizeAtribut() {
        return sizeAtribut;
    }

    public void setSizeAtribut(Integer sizeAtribut) {
        this.sizeAtribut = sizeAtribut;
    }

    public String getFormatAtribut() {
        return formatAtribut;
    }

    public void setFormatAtribut(String formatAtribut) {
        this.formatAtribut = formatAtribut;
    }

    public String getDescriptAtribut() {
        return descriptAtribut;
    }

    public void setDescriptAtribut(String descriptAtribut) {
        this.descriptAtribut = descriptAtribut;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KbNsiTypeAtribut)) {
            return false;
        }
        KbNsiTypeAtribut other = (KbNsiTypeAtribut) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.KbNsiTypeAtribut[ idRecord=" + idRecord + " ]";
    }
    
}
