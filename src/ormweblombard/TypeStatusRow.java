/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "type_status_row", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeStatusRow.findAll", query = "SELECT t FROM TypeStatusRow t"),
    @NamedQuery(name = "TypeStatusRow.findByNameStatusRow", query = "SELECT t FROM TypeStatusRow t WHERE t.nameStatusRow = :nameStatusRow"),
    @NamedQuery(name = "TypeStatusRow.findByStatusRow", query = "SELECT t FROM TypeStatusRow t WHERE t.statusRow = :statusRow"),
    @NamedQuery(name = "TypeStatusRow.findByIdRecord", query = "SELECT t FROM TypeStatusRow t WHERE t.idRecord = :idRecord"),
    @NamedQuery(name = "TypeStatusRow.findByCodeStatusRow", query = "SELECT t FROM TypeStatusRow t WHERE t.codeStatusRow = :codeStatusRow")})
public class TypeStatusRow implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "name_status_row", length = 50)
    private String nameStatusRow;
    @Column(name = "status_row")
    private Short statusRow;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "code_status_row")
    private Short codeStatusRow;

    public TypeStatusRow() {
    }

    public TypeStatusRow(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNameStatusRow() {
        return nameStatusRow;
    }

    public void setNameStatusRow(String nameStatusRow) {
        this.nameStatusRow = nameStatusRow;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Short getCodeStatusRow() {
        return codeStatusRow;
    }

    public void setCodeStatusRow(Short codeStatusRow) {
        this.codeStatusRow = codeStatusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeStatusRow)) {
            return false;
        }
        TypeStatusRow other = (TypeStatusRow) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.TypeStatusRow[ idRecord=" + idRecord + " ]";
    }
    
}
