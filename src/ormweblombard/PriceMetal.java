/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "price_metal", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PriceMetal.findAll", query = "SELECT p FROM PriceMetal p"),
    @NamedQuery(name = "PriceMetal.findByIdPricePosition", query = "SELECT p FROM PriceMetal p WHERE p.idPricePosition = :idPricePosition"),
    @NamedQuery(name = "PriceMetal.findByMarkMetal", query = "SELECT p FROM PriceMetal p WHERE p.markMetal = :markMetal"),
    @NamedQuery(name = "PriceMetal.findByNameMetal", query = "SELECT p FROM PriceMetal p WHERE p.nameMetal = :nameMetal"),
    @NamedQuery(name = "PriceMetal.findByMinCostUnit", query = "SELECT p FROM PriceMetal p WHERE p.minCostUnit = :minCostUnit"),
    @NamedQuery(name = "PriceMetal.findByMaxCostUnit", query = "SELECT p FROM PriceMetal p WHERE p.maxCostUnit = :maxCostUnit"),
    @NamedQuery(name = "PriceMetal.findByStatusRow", query = "SELECT p FROM PriceMetal p WHERE p.statusRow = :statusRow"),
    @NamedQuery(name = "PriceMetal.findByIdRecord", query = "SELECT p FROM PriceMetal p WHERE p.idRecord = :idRecord")})
public class PriceMetal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "id_price_position")
    private Integer idPricePosition;
    @Column(name = "mark_metal")
    private Integer markMetal;
    @Column(name = "name_metal", length = 50)
    private String nameMetal;
    @Column(name = "min_cost_unit")
    private Integer minCostUnit;
    @Column(name = "max_cost_unit")
    private Integer maxCostUnit;
    @Column(name = "status_row")
    private Short statusRow;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public PriceMetal() {
    }

    public PriceMetal(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdPricePosition() {
        return idPricePosition;
    }

    public void setIdPricePosition(Integer idPricePosition) {
        this.idPricePosition = idPricePosition;
    }

    public Integer getMarkMetal() {
        return markMetal;
    }

    public void setMarkMetal(Integer markMetal) {
        this.markMetal = markMetal;
    }

    public String getNameMetal() {
        return nameMetal;
    }

    public void setNameMetal(String nameMetal) {
        this.nameMetal = nameMetal;
    }

    public Integer getMinCostUnit() {
        return minCostUnit;
    }

    public void setMinCostUnit(Integer minCostUnit) {
        this.minCostUnit = minCostUnit;
    }

    public Integer getMaxCostUnit() {
        return maxCostUnit;
    }

    public void setMaxCostUnit(Integer maxCostUnit) {
        this.maxCostUnit = maxCostUnit;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PriceMetal)) {
            return false;
        }
        PriceMetal other = (PriceMetal) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.PriceMetal[ idRecord=" + idRecord + " ]";
    }
    
}
