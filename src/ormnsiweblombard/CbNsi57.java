/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_57", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi57.findAll", query = "SELECT c FROM CbNsi57 c"),
    @NamedQuery(name = "CbNsi57.findByNsiId", query = "SELECT c FROM CbNsi57 c WHERE c.nsiId = :nsiId"),
    @NamedQuery(name = "CbNsi57.findByKodFormsobstv", query = "SELECT c FROM CbNsi57 c WHERE c.kodFormsobstv = :kodFormsobstv"),
    @NamedQuery(name = "CbNsi57.findByNameFormsobstv", query = "SELECT c FROM CbNsi57 c WHERE c.nameFormsobstv = :nameFormsobstv"),
    @NamedQuery(name = "CbNsi57.findByDateOpen", query = "SELECT c FROM CbNsi57 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi57.findByDateClose", query = "SELECT c FROM CbNsi57 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi57.findByActive", query = "SELECT c FROM CbNsi57 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi57.findByIdRecord", query = "SELECT c FROM CbNsi57 c WHERE c.idRecord = :idRecord")})
public class CbNsi57 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nsi_id", length = 10)
    private String nsiId;
    @Column(name = "kod_formsobstv", length = 10)
    private String kodFormsobstv;
    @Column(name = "name_formsobstv", length = 100)
    private String nameFormsobstv;
    @Column(name = "date_open", length = 8)
    private String dateOpen;
    @Column(name = "date_close", length = 8)
    private String dateClose;
    @Column(name = "active", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi57() {
    }

    public CbNsi57(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNsiId() {
        return nsiId;
    }

    public void setNsiId(String nsiId) {
        this.nsiId = nsiId;
    }

    public String getKodFormsobstv() {
        return kodFormsobstv;
    }

    public void setKodFormsobstv(String kodFormsobstv) {
        this.kodFormsobstv = kodFormsobstv;
    }

    public String getNameFormsobstv() {
        return nameFormsobstv;
    }

    public void setNameFormsobstv(String nameFormsobstv) {
        this.nameFormsobstv = nameFormsobstv;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi57)) {
            return false;
        }
        CbNsi57 other = (CbNsi57) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi57[ idRecord=" + idRecord + " ]";
    }
    
}
