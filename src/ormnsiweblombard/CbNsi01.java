/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_01", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi01.findAll", query = "SELECT c FROM CbNsi01 c"),
    @NamedQuery(name = "CbNsi01.findByNsiId", query = "SELECT c FROM CbNsi01 c WHERE c.nsiId = :nsiId"),
    @NamedQuery(name = "CbNsi01.findByTypeOrg", query = "SELECT c FROM CbNsi01 c WHERE c.typeOrg = :typeOrg"),
    @NamedQuery(name = "CbNsi01.findByCodeLombard", query = "SELECT c FROM CbNsi01 c WHERE c.codeLombard = :codeLombard"),
    @NamedQuery(name = "CbNsi01.findByNameLombard", query = "SELECT c FROM CbNsi01 c WHERE c.nameLombard = :nameLombard"),
    @NamedQuery(name = "CbNsi01.findByAdresLombarda", query = "SELECT c FROM CbNsi01 c WHERE c.adresLombarda = :adresLombarda"),
    @NamedQuery(name = "CbNsi01.findByDateLicense", query = "SELECT c FROM CbNsi01 c WHERE c.dateLicense = :dateLicense"),
    @NamedQuery(name = "CbNsi01.findByDateClose", query = "SELECT c FROM CbNsi01 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi01.findByRegion", query = "SELECT c FROM CbNsi01 c WHERE c.region = :region"),
    @NamedQuery(name = "CbNsi01.findByDistrict", query = "SELECT c FROM CbNsi01 c WHERE c.district = :district"),
    @NamedQuery(name = "CbNsi01.findByDateActive", query = "SELECT c FROM CbNsi01 c WHERE c.dateActive = :dateActive"),
    @NamedQuery(name = "CbNsi01.findByDateDeactive", query = "SELECT c FROM CbNsi01 c WHERE c.dateDeactive = :dateDeactive"),
    @NamedQuery(name = "CbNsi01.findByActive", query = "SELECT c FROM CbNsi01 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi01.findByInn", query = "SELECT c FROM CbNsi01 c WHERE c.inn = :inn"),
    @NamedQuery(name = "CbNsi01.findByIdRecord", query = "SELECT c FROM CbNsi01 c WHERE c.idRecord = :idRecord")})
public class CbNsi01 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nsi_id", length = 3)
    private String nsiId;
    @Column(name = "type_org", length = 1)
    private String typeOrg;
    @Column(name = "code_lombard", length = 5)
    private String codeLombard;
    @Column(name = "name_lombard", length = 200)
    private String nameLombard;
    @Column(name = "adres_lombarda", length = 200)
    private String adresLombarda;
    @Column(name = "date_license", length = 8)
    private String dateLicense;
    @Column(name = "date_close", length = 8)
    private String dateClose;
    @Column(name = "region", length = 3)
    private String region;
    @Column(name = "district", length = 3)
    private String district;
    @Column(name = "date_active", length = 8)
    private String dateActive;
    @Column(name = "date_deactive", length = 8)
    private String dateDeactive;
    @Column(name = "active", length = 1)
    private String active;
    @Column(name = "inn", length = 9)
    private String inn;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi01() {
    }

    public CbNsi01(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getNsiId() {
        return nsiId;
    }

    public void setNsiId(String nsiId) {
        this.nsiId = nsiId;
    }

    public String getTypeOrg() {
        return typeOrg;
    }

    public void setTypeOrg(String typeOrg) {
        this.typeOrg = typeOrg;
    }

    public String getCodeLombard() {
        return codeLombard;
    }

    public void setCodeLombard(String codeLombard) {
        this.codeLombard = codeLombard;
    }

    public String getNameLombard() {
        return nameLombard;
    }

    public void setNameLombard(String nameLombard) {
        this.nameLombard = nameLombard;
    }

    public String getAdresLombarda() {
        return adresLombarda;
    }

    public void setAdresLombarda(String adresLombarda) {
        this.adresLombarda = adresLombarda;
    }

    public String getDateLicense() {
        return dateLicense;
    }

    public void setDateLicense(String dateLicense) {
        this.dateLicense = dateLicense;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDateActive() {
        return dateActive;
    }

    public void setDateActive(String dateActive) {
        this.dateActive = dateActive;
    }

    public String getDateDeactive() {
        return dateDeactive;
    }

    public void setDateDeactive(String dateDeactive) {
        this.dateDeactive = dateDeactive;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi01)) {
            return false;
        }
        CbNsi01 other = (CbNsi01) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi01[ idRecord=" + idRecord + " ]";
    }
    
}
