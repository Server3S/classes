/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "role", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r"),
    @NamedQuery(name = "Role.findByIdRecord", query = "SELECT r FROM Role r WHERE r.idRecord = :idRecord"),
    @NamedQuery(name = "Role.findByIdGroupRole", query = "SELECT r FROM Role r WHERE r.idGroupRole = :idGroupRole"),
    @NamedQuery(name = "Role.findByIdRole", query = "SELECT r FROM Role r WHERE r.idRole = :idRole"),
    @NamedQuery(name = "Role.findByNameRole", query = "SELECT r FROM Role r WHERE r.nameRole = :nameRole"),
    @NamedQuery(name = "Role.findByDescriptRole", query = "SELECT r FROM Role r WHERE r.descriptRole = :descriptRole"),
    @NamedQuery(name = "Role.findByUserCreateRole", query = "SELECT r FROM Role r WHERE r.userCreateRole = :userCreateRole"),
    @NamedQuery(name = "Role.findByDatetimeCreateRole", query = "SELECT r FROM Role r WHERE r.datetimeCreateRole = :datetimeCreateRole"),
    @NamedQuery(name = "Role.findByMaxNumberRole", query = "SELECT r FROM Role r WHERE r.maxNumberRole = :maxNumberRole"),
    @NamedQuery(name = "Role.findByTypeRole", query = "SELECT r FROM Role r WHERE r.typeRole = :typeRole"),
    @NamedQuery(name = "Role.findByNameTypeRole", query = "SELECT r FROM Role r WHERE r.nameTypeRole = :nameTypeRole"),
    @NamedQuery(name = "Role.findByCodeFunctionRole", query = "SELECT r FROM Role r WHERE r.codeFunctionRole = :codeFunctionRole"),
    @NamedQuery(name = "Role.findByStatusRole", query = "SELECT r FROM Role r WHERE r.statusRole = :statusRole")})
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_group_role")
    private Integer idGroupRole;
    @Column(name = "id_role")
    private Integer idRole;
    @Column(name = "name_role", length = 50)
    private String nameRole;
    @Column(name = "descript_role", length = 100)
    private String descriptRole;
    @Column(name = "user_create_role", length = 30)
    private String userCreateRole;
    @Column(name = "datetime_create_role", length = 20)
    private String datetimeCreateRole;
    @Column(name = "max_number_role")
    private Integer maxNumberRole;
    @Column(name = "type_role", length = 2)
    private String typeRole;
    @Column(name = "name_type_role", length = 30)
    private String nameTypeRole;
    @Column(name = "code_function_role")
    private Integer codeFunctionRole;
    @Column(name = "status_role")
    private Short statusRole;

    public Role() {
    }

    public Role(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdGroupRole() {
        return idGroupRole;
    }

    public void setIdGroupRole(Integer idGroupRole) {
        this.idGroupRole = idGroupRole;
    }

    public Integer getIdRole() {
        return idRole;
    }

    public void setIdRole(Integer idRole) {
        this.idRole = idRole;
    }

    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }

    public String getDescriptRole() {
        return descriptRole;
    }

    public void setDescriptRole(String descriptRole) {
        this.descriptRole = descriptRole;
    }

    public String getUserCreateRole() {
        return userCreateRole;
    }

    public void setUserCreateRole(String userCreateRole) {
        this.userCreateRole = userCreateRole;
    }

    public String getDatetimeCreateRole() {
        return datetimeCreateRole;
    }

    public void setDatetimeCreateRole(String datetimeCreateRole) {
        this.datetimeCreateRole = datetimeCreateRole;
    }

    public Integer getMaxNumberRole() {
        return maxNumberRole;
    }

    public void setMaxNumberRole(Integer maxNumberRole) {
        this.maxNumberRole = maxNumberRole;
    }

    public String getTypeRole() {
        return typeRole;
    }

    public void setTypeRole(String typeRole) {
        this.typeRole = typeRole;
    }

    public String getNameTypeRole() {
        return nameTypeRole;
    }

    public void setNameTypeRole(String nameTypeRole) {
        this.nameTypeRole = nameTypeRole;
    }

    public Integer getCodeFunctionRole() {
        return codeFunctionRole;
    }

    public void setCodeFunctionRole(Integer codeFunctionRole) {
        this.codeFunctionRole = codeFunctionRole;
    }

    public Short getStatusRole() {
        return statusRole;
    }

    public void setStatusRole(Short statusRole) {
        this.statusRole = statusRole;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Role)) {
            return false;
        }
        Role other = (Role) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.Role[ idRecord=" + idRecord + " ]";
    }
    
}
