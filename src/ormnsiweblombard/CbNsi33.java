/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_33", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi33.findAll", query = "SELECT c FROM CbNsi33 c"),
    @NamedQuery(name = "CbNsi33.findByNciId", query = "SELECT c FROM CbNsi33 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi33.findByGuarId", query = "SELECT c FROM CbNsi33 c WHERE c.guarId = :guarId"),
    @NamedQuery(name = "CbNsi33.findByGuarName", query = "SELECT c FROM CbNsi33 c WHERE c.guarName = :guarName"),
    @NamedQuery(name = "CbNsi33.findByGuarGr", query = "SELECT c FROM CbNsi33 c WHERE c.guarGr = :guarGr"),
    @NamedQuery(name = "CbNsi33.findByDateOpen", query = "SELECT c FROM CbNsi33 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi33.findByDateClose", query = "SELECT c FROM CbNsi33 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi33.findByActive", query = "SELECT c FROM CbNsi33 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi33.findByIdRecord", query = "SELECT c FROM CbNsi33 c WHERE c.idRecord = :idRecord")})
public class CbNsi33 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Basic(optional = false)
    @Column(name = "guar_id", nullable = false, length = 10)
    private String guarId;
    @Column(name = "guar_name", length = 100)
    private String guarName;
    @Column(name = "guar_gr", length = 10)
    private String guarGr;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Column(name = "date_close", length = 10)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi33() {
    }

    public CbNsi33(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public CbNsi33(Integer idRecord, String guarId) {
        this.idRecord = idRecord;
        this.guarId = guarId;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getGuarId() {
        return guarId;
    }

    public void setGuarId(String guarId) {
        this.guarId = guarId;
    }

    public String getGuarName() {
        return guarName;
    }

    public void setGuarName(String guarName) {
        this.guarName = guarName;
    }

    public String getGuarGr() {
        return guarGr;
    }

    public void setGuarGr(String guarGr) {
        this.guarGr = guarGr;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi33)) {
            return false;
        }
        CbNsi33 other = (CbNsi33) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi33[ idRecord=" + idRecord + " ]";
    }
    
}
