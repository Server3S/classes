/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_21", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi21.findAll", query = "SELECT c FROM CbNsi21 c"),
    @NamedQuery(name = "CbNsi21.findByNciId", query = "SELECT c FROM CbNsi21 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi21.findByKodK", query = "SELECT c FROM CbNsi21 c WHERE c.kodK = :kodK"),
    @NamedQuery(name = "CbNsi21.findByNameK1", query = "SELECT c FROM CbNsi21 c WHERE c.nameK1 = :nameK1"),
    @NamedQuery(name = "CbNsi21.findByNameK2", query = "SELECT c FROM CbNsi21 c WHERE c.nameK2 = :nameK2"),
    @NamedQuery(name = "CbNsi21.findByDateOpen", query = "SELECT c FROM CbNsi21 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi21.findByDateClose", query = "SELECT c FROM CbNsi21 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi21.findByActive", query = "SELECT c FROM CbNsi21 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi21.findByIdRecord", query = "SELECT c FROM CbNsi21 c WHERE c.idRecord = :idRecord")})
public class CbNsi21 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Basic(optional = false)
    @Column(name = "kod_k", nullable = false, length = 2)
    private String kodK;
    @Column(name = "name_k1", length = 10)
    private String nameK1;
    @Column(name = "name_k2", length = 40)
    private String nameK2;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Column(name = "date_close", length = 10)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi21() {
    }

    public CbNsi21(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public CbNsi21(Integer idRecord, String kodK) {
        this.idRecord = idRecord;
        this.kodK = kodK;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getKodK() {
        return kodK;
    }

    public void setKodK(String kodK) {
        this.kodK = kodK;
    }

    public String getNameK1() {
        return nameK1;
    }

    public void setNameK1(String nameK1) {
        this.nameK1 = nameK1;
    }

    public String getNameK2() {
        return nameK2;
    }

    public void setNameK2(String nameK2) {
        this.nameK2 = nameK2;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi21)) {
            return false;
        }
        CbNsi21 other = (CbNsi21) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi21[ idRecord=" + idRecord + " ]";
    }
    
}
