/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "cb_nsi_17", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbNsi17.findAll", query = "SELECT c FROM CbNsi17 c"),
    @NamedQuery(name = "CbNsi17.findByNciId", query = "SELECT c FROM CbNsi17 c WHERE c.nciId = :nciId"),
    @NamedQuery(name = "CbNsi17.findByKod", query = "SELECT c FROM CbNsi17 c WHERE c.kod = :kod"),
    @NamedQuery(name = "CbNsi17.findByKodB", query = "SELECT c FROM CbNsi17 c WHERE c.kodB = :kodB"),
    @NamedQuery(name = "CbNsi17.findByNamev", query = "SELECT c FROM CbNsi17 c WHERE c.namev = :namev"),
    @NamedQuery(name = "CbNsi17.findByScale", query = "SELECT c FROM CbNsi17 c WHERE c.scale = :scale"),
    @NamedQuery(name = "CbNsi17.findByScaleName", query = "SELECT c FROM CbNsi17 c WHERE c.scaleName = :scaleName"),
    @NamedQuery(name = "CbNsi17.findByHard", query = "SELECT c FROM CbNsi17 c WHERE c.hard = :hard"),
    @NamedQuery(name = "CbNsi17.findByAllow", query = "SELECT c FROM CbNsi17 c WHERE c.allow = :allow"),
    @NamedQuery(name = "CbNsi17.findByDateOpen", query = "SELECT c FROM CbNsi17 c WHERE c.dateOpen = :dateOpen"),
    @NamedQuery(name = "CbNsi17.findByDateClose", query = "SELECT c FROM CbNsi17 c WHERE c.dateClose = :dateClose"),
    @NamedQuery(name = "CbNsi17.findByActive", query = "SELECT c FROM CbNsi17 c WHERE c.active = :active"),
    @NamedQuery(name = "CbNsi17.findByIdRecord", query = "SELECT c FROM CbNsi17 c WHERE c.idRecord = :idRecord")})
public class CbNsi17 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "nci_id", length = 10)
    private String nciId;
    @Basic(optional = false)
    @Column(name = "kod", nullable = false, length = 10)
    private String kod;
    @Column(name = "kod_b", length = 10)
    private String kodB;
    @Column(name = "namev", length = 100)
    private String namev;
    @Column(name = "scale", length = 10)
    private String scale;
    @Column(name = "scale_name", length = 10)
    private String scaleName;
    @Column(name = "hard", length = 1)
    private String hard;
    @Column(name = "allow", length = 1)
    private String allow;
    @Column(name = "date_open", length = 10)
    private String dateOpen;
    @Column(name = "date_close", length = 10)
    private String dateClose;
    @Column(name = "ACTIVE", length = 1)
    private String active;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public CbNsi17() {
    }

    public CbNsi17(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public CbNsi17(Integer idRecord, String kod) {
        this.idRecord = idRecord;
        this.kod = kod;
    }

    public String getNciId() {
        return nciId;
    }

    public void setNciId(String nciId) {
        this.nciId = nciId;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getKodB() {
        return kodB;
    }

    public void setKodB(String kodB) {
        this.kodB = kodB;
    }

    public String getNamev() {
        return namev;
    }

    public void setNamev(String namev) {
        this.namev = namev;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public String getScaleName() {
        return scaleName;
    }

    public void setScaleName(String scaleName) {
        this.scaleName = scaleName;
    }

    public String getHard() {
        return hard;
    }

    public void setHard(String hard) {
        this.hard = hard;
    }

    public String getAllow() {
        return allow;
    }

    public void setAllow(String allow) {
        this.allow = allow;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbNsi17)) {
            return false;
        }
        CbNsi17 other = (CbNsi17) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.CbNsi17[ idRecord=" + idRecord + " ]";
    }
    
}
