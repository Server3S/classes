/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "user", catalog = "WebLombard", schema = "weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findByIdRecord", query = "SELECT u FROM User u WHERE u.idRecord = :idRecord"),
    @NamedQuery(name = "User.findByIdUser", query = "SELECT u FROM User u WHERE u.idUser = :idUser"),
    @NamedQuery(name = "User.findByPositionUser", query = "SELECT u FROM User u WHERE u.positionUser = :positionUser"),
    @NamedQuery(name = "User.findByInnUser", query = "SELECT u FROM User u WHERE u.innUser = :innUser"),
    @NamedQuery(name = "User.findBySerialPassUser", query = "SELECT u FROM User u WHERE u.serialPassUser = :serialPassUser"),
    @NamedQuery(name = "User.findByNumberPassUser", query = "SELECT u FROM User u WHERE u.numberPassUser = :numberPassUser"),
    @NamedQuery(name = "User.findByDatePassUser", query = "SELECT u FROM User u WHERE u.datePassUser = :datePassUser"),
    @NamedQuery(name = "User.findByPlaceGivePass", query = "SELECT u FROM User u WHERE u.placeGivePass = :placeGivePass"),
    @NamedQuery(name = "User.findByFioUserLat", query = "SELECT u FROM User u WHERE u.fioUserLat = :fioUserLat"),
    @NamedQuery(name = "User.findByFioUserCyr", query = "SELECT u FROM User u WHERE u.fioUserCyr = :fioUserCyr"),
    @NamedQuery(name = "User.findByUserContact", query = "SELECT u FROM User u WHERE u.userContact = :userContact"),
    @NamedQuery(name = "User.findByUserAdresLive", query = "SELECT u FROM User u WHERE u.userAdresLive = :userAdresLive"),
    @NamedQuery(name = "User.findByUserBirthday", query = "SELECT u FROM User u WHERE u.userBirthday = :userBirthday"),
    @NamedQuery(name = "User.findByUserGender", query = "SELECT u FROM User u WHERE u.userGender = :userGender"),
    @NamedQuery(name = "User.findByUserLogin", query = "SELECT u FROM User u WHERE u.userLogin = :userLogin"),
    @NamedQuery(name = "User.findByIdUserRole", query = "SELECT u FROM User u WHERE u.idUserRole = :idUserRole"),
    @NamedQuery(name = "User.findByDateCreateUser", query = "SELECT u FROM User u WHERE u.dateCreateUser = :dateCreateUser"),
    @NamedQuery(name = "User.findByCreatorUser", query = "SELECT u FROM User u WHERE u.creatorUser = :creatorUser"),
    @NamedQuery(name = "User.findByCommentDisableUser", query = "SELECT u FROM User u WHERE u.commentDisableUser = :commentDisableUser"),
    @NamedQuery(name = "User.findByDatetimeDisableUser", query = "SELECT u FROM User u WHERE u.datetimeDisableUser = :datetimeDisableUser"),
    @NamedQuery(name = "User.findByCommentEnableUser", query = "SELECT u FROM User u WHERE u.commentEnableUser = :commentEnableUser"),
    @NamedQuery(name = "User.findByDatetimeEnableUser", query = "SELECT u FROM User u WHERE u.datetimeEnableUser = :datetimeEnableUser"),
    @NamedQuery(name = "User.findByStatusRow", query = "SELECT u FROM User u WHERE u.statusRow = :statusRow")})
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;
    @Column(name = "id_user")
    private Integer idUser;
    @Column(name = "position_user", length = 50)
    private String positionUser;
    @Column(name = "inn_user")
    private Integer innUser;
    @Column(name = "serial_pass_user", length = 3)
    private String serialPassUser;
    @Column(name = "number_pass_user")
    private Integer numberPassUser;
    @Column(name = "date_pass_user", length = 10)
    private String datePassUser;
    @Column(name = "place_give_pass", length = 200)
    private String placeGivePass;
    @Column(name = "fio_user_lat", length = 150)
    private String fioUserLat;
    @Column(name = "fio_user_cyr", length = 150)
    private String fioUserCyr;
    @Column(name = "user_contact", length = 300)
    private String userContact;
    @Column(name = "user_adres_live", length = 300)
    private String userAdresLive;
    @Column(name = "user_birthday", length = 10)
    private String userBirthday;
    @Column(name = "user_gender", length = 10)
    private String userGender;
    @Column(name = "user_login", length = 25)
    private String userLogin;
    @Column(name = "id_user_role")
    private Integer idUserRole;
    @Column(name = "date_create_user", length = 10)
    private String dateCreateUser;
    @Column(name = "creator_user", length = 25)
    private String creatorUser;
    @Column(name = "comment_disable_user", length = 100)
    private String commentDisableUser;
    @Column(name = "datetime_disable_user", length = 20)
    private String datetimeDisableUser;
    @Column(name = "comment_enable_user", length = 100)
    private String commentEnableUser;
    @Column(name = "datetime_enable_user", length = 20)
    private String datetimeEnableUser;
    @Column(name = "status_row")
    private Short statusRow;

    public User() {
    }

    public User(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getPositionUser() {
        return positionUser;
    }

    public void setPositionUser(String positionUser) {
        this.positionUser = positionUser;
    }

    public Integer getInnUser() {
        return innUser;
    }

    public void setInnUser(Integer innUser) {
        this.innUser = innUser;
    }

    public String getSerialPassUser() {
        return serialPassUser;
    }

    public void setSerialPassUser(String serialPassUser) {
        this.serialPassUser = serialPassUser;
    }

    public Integer getNumberPassUser() {
        return numberPassUser;
    }

    public void setNumberPassUser(Integer numberPassUser) {
        this.numberPassUser = numberPassUser;
    }

    public String getDatePassUser() {
        return datePassUser;
    }

    public void setDatePassUser(String datePassUser) {
        this.datePassUser = datePassUser;
    }

    public String getPlaceGivePass() {
        return placeGivePass;
    }

    public void setPlaceGivePass(String placeGivePass) {
        this.placeGivePass = placeGivePass;
    }

    public String getFioUserLat() {
        return fioUserLat;
    }

    public void setFioUserLat(String fioUserLat) {
        this.fioUserLat = fioUserLat;
    }

    public String getFioUserCyr() {
        return fioUserCyr;
    }

    public void setFioUserCyr(String fioUserCyr) {
        this.fioUserCyr = fioUserCyr;
    }

    public String getUserContact() {
        return userContact;
    }

    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    public String getUserAdresLive() {
        return userAdresLive;
    }

    public void setUserAdresLive(String userAdresLive) {
        this.userAdresLive = userAdresLive;
    }

    public String getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(String userBirthday) {
        this.userBirthday = userBirthday;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Integer getIdUserRole() {
        return idUserRole;
    }

    public void setIdUserRole(Integer idUserRole) {
        this.idUserRole = idUserRole;
    }

    public String getDateCreateUser() {
        return dateCreateUser;
    }

    public void setDateCreateUser(String dateCreateUser) {
        this.dateCreateUser = dateCreateUser;
    }

    public String getCreatorUser() {
        return creatorUser;
    }

    public void setCreatorUser(String creatorUser) {
        this.creatorUser = creatorUser;
    }

    public String getCommentDisableUser() {
        return commentDisableUser;
    }

    public void setCommentDisableUser(String commentDisableUser) {
        this.commentDisableUser = commentDisableUser;
    }

    public String getDatetimeDisableUser() {
        return datetimeDisableUser;
    }

    public void setDatetimeDisableUser(String datetimeDisableUser) {
        this.datetimeDisableUser = datetimeDisableUser;
    }

    public String getCommentEnableUser() {
        return commentEnableUser;
    }

    public void setCommentEnableUser(String commentEnableUser) {
        this.commentEnableUser = commentEnableUser;
    }

    public String getDatetimeEnableUser() {
        return datetimeEnableUser;
    }

    public void setDatetimeEnableUser(String datetimeEnableUser) {
        this.datetimeEnableUser = datetimeEnableUser;
    }

    public Short getStatusRow() {
        return statusRow;
    }

    public void setStatusRow(Short statusRow) {
        this.statusRow = statusRow;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormweblombard.User[ idRecord=" + idRecord + " ]";
    }
    
}
