/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "kb_nsi_a10", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KbNsiA10.findAll", query = "SELECT k FROM KbNsiA10 k"),
    @NamedQuery(name = "KbNsiA10.findByKod", query = "SELECT k FROM KbNsiA10 k WHERE k.kod = :kod"),
    @NamedQuery(name = "KbNsiA10.findByKodTrebovaniya", query = "SELECT k FROM KbNsiA10 k WHERE k.kodTrebovaniya = :kodTrebovaniya"),
    @NamedQuery(name = "KbNsiA10.findByNaim", query = "SELECT k FROM KbNsiA10 k WHERE k.naim = :naim"),
    @NamedQuery(name = "KbNsiA10.findByStat", query = "SELECT k FROM KbNsiA10 k WHERE k.stat = :stat"),
    @NamedQuery(name = "KbNsiA10.findByIdRecord", query = "SELECT k FROM KbNsiA10 k WHERE k.idRecord = :idRecord")})
public class KbNsiA10 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "kod")
    private Integer kod;
    @Column(name = "kod_trebovaniya")
    private Integer kodTrebovaniya;
    @Column(name = "naim", length = 50)
    private String naim;
    @Column(name = "stat")
    private Short stat;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public KbNsiA10() {
    }

    public KbNsiA10(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public Integer getKod() {
        return kod;
    }

    public void setKod(Integer kod) {
        this.kod = kod;
    }

    public Integer getKodTrebovaniya() {
        return kodTrebovaniya;
    }

    public void setKodTrebovaniya(Integer kodTrebovaniya) {
        this.kodTrebovaniya = kodTrebovaniya;
    }

    public String getNaim() {
        return naim;
    }

    public void setNaim(String naim) {
        this.naim = naim;
    }

    public Short getStat() {
        return stat;
    }

    public void setStat(Short stat) {
        this.stat = stat;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KbNsiA10)) {
            return false;
        }
        KbNsiA10 other = (KbNsiA10) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.KbNsiA10[ idRecord=" + idRecord + " ]";
    }
    
}
