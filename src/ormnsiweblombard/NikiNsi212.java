/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormnsiweblombard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Baka
 */
@Entity
@Table(name = "niki_nsi_212", catalog = "WebLombard", schema = "nsi_weblombard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NikiNsi212.findAll", query = "SELECT n FROM NikiNsi212 n"),
    @NamedQuery(name = "NikiNsi212.findByKod", query = "SELECT n FROM NikiNsi212 n WHERE n.kod = :kod"),
    @NamedQuery(name = "NikiNsi212.findByNameTypeoperation", query = "SELECT n FROM NikiNsi212 n WHERE n.nameTypeoperation = :nameTypeoperation"),
    @NamedQuery(name = "NikiNsi212.findByIdRecord", query = "SELECT n FROM NikiNsi212 n WHERE n.idRecord = :idRecord")})
public class NikiNsi212 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "kod", length = 3)
    private String kod;
    @Column(name = "name_typeoperation", length = 50)
    private String nameTypeoperation;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_record", nullable = false)
    private Integer idRecord;

    public NikiNsi212() {
    }

    public NikiNsi212(Integer idRecord) {
        this.idRecord = idRecord;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getNameTypeoperation() {
        return nameTypeoperation;
    }

    public void setNameTypeoperation(String nameTypeoperation) {
        this.nameTypeoperation = nameTypeoperation;
    }

    public Integer getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(Integer idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRecord != null ? idRecord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NikiNsi212)) {
            return false;
        }
        NikiNsi212 other = (NikiNsi212) object;
        if ((this.idRecord == null && other.idRecord != null) || (this.idRecord != null && !this.idRecord.equals(other.idRecord))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ormnsiweblombard.NikiNsi212[ idRecord=" + idRecord + " ]";
    }
    
}
